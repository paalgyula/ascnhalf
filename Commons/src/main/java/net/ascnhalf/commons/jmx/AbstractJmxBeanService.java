package net.ascnhalf.commons.jmx;

import net.ascnhalf.commons.service.Service;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractJmxBeanService implements Service {

    @Autowired
    private JMXService jmxService;

    @Override
    public void start() {
        jmxService.start(this, getBeanName());
    }

    @Override
    public void stop() {
        jmxService.stop(getBeanName());
    }

    protected abstract String getBeanName();
}
