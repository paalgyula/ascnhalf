package net.ascnhalf.commons.network.jmx;

import net.ascnhalf.commons.jmx.AbstractJmxBeanService;
import net.ascnhalf.commons.network.netty.service.NetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
@Qualifier("JMXNetworkService")
public class JmxNetworkService extends AbstractJmxBeanService implements
        JmxNetworkServiceMBean {
    private static final String BEAN_NAME = "commons.network:name=JmxNetworkService";

    @Autowired(required = false)
    private NetworkService networkService;

    @Override
    public String printClientChannels() {
        return networkService.getChannelsInfo();
    }

    @Override
    protected String getBeanName() {
        return BEAN_NAME;
    }

}
