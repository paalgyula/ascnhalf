/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.commons.network.netty.receiver;

import net.ascnhalf.commons.network.handlers.PacketHandlerFactory;
import net.ascnhalf.commons.network.model.NetworkChannel;
import net.ascnhalf.commons.network.model.ReceivablePacket;
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;

/**
 * The Class NettyPacketReceiver.
 */
public class NettyPacketReceiver {

    /**
     * The Constant log.
     */
    private static final Logger log = Logger
            .getLogger(NettyPacketReceiver.class);

    /**
     * Receive packet.
     *
     * @param packetHandler  the packet handler
     * @param buffer         the buffer
     * @param networkChannel the network channel
     */
    public void receivePacket(PacketHandlerFactory packetHandler,
                              ChannelBuffer buffer, NetworkChannel networkChannel) {
        int packetId = buffer.readUnsignedByte();
        log.debug(String.format("[RESIVE PACKET] :  0x%02X", packetId));
        readAndRunPacket(packetHandler, buffer, packetId, networkChannel);
    }

    /**
     * Read and run packet.
     *
     * @param packetHandler  the packet handler
     * @param buffer         the buffer
     * @param packetId       the packet id
     * @param networkChannel the network channel
     */
    private void readAndRunPacket(PacketHandlerFactory packetHandler,
                                  ChannelBuffer buffer, int packetId, NetworkChannel networkChannel) {
        ReceivablePacket cp = packetHandler.handleClientPacket(packetId,
                networkChannel);
        if (cp != null) {
            cp.setByteBuffer(buffer);
            cp.setClient(networkChannel);
            if (cp.getAvaliableBytes() < cp.getMinimumLength()) {
                log.warn("BUFFER_UNDER_FLOW" + networkChannel + cp);
            } else if (cp.read()) {

            }
        } else {
            System.out.println("Handle of ReceivablePacket is null.");
        }
    }
}
