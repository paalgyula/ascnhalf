/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.commons.network.handlers;

import gnu.trove.procedure.TIntObjectProcedure;
import net.ascnhalf.commons.dataholder.XmlDataLoader;
import net.ascnhalf.commons.network.model.NetworkChannel;
import net.ascnhalf.commons.network.model.ReceivablePacket;
import net.ascnhalf.commons.network.model.SendablePacket;
import net.ascnhalf.commons.network.model.State;
import net.ascnhalf.commons.network.netty.model.PacketData;
import net.ascnhalf.commons.network.netty.model.PacketList;
import net.ascnhalf.commons.network.netty.model.PacketTemplate;
import net.ascnhalf.commons.service.ServiceContent;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * A factory for creating AbstractPacketHandler objects.
 *
 * @author minimajack
 */
public abstract class AbstractPacketHandlerFactory extends XmlDataLoader
        implements PacketHandlerFactory {
    private static final Logger logger = Logger.getLogger(AbstractPacketHandlerFactory.class);
    /**
     * The cHandler.
     */
    ClientPacketHandler cHandler = new ClientPacketHandler();

    /**
     * The sHandler.
     */
    ServerPacketHandler sHandler = new ServerPacketHandler();

    @Autowired
    @Qualifier("packetXSD")
    protected String packetXSDLocation;

    @Autowired
    @Qualifier("toClient")
    protected String clientPacketPath;

    /**
     * Instantiates a new abstract packet handler factory.
     */
    public AbstractPacketHandlerFactory() {
        super();
    }

    static abstract class AddPackets implements
            TIntObjectProcedure<PacketTemplate> {
        protected String upstreamPackageName;
        protected ClassLoader classLoader;
        protected AbstractPacketHandlerFactory packetHandlerFactory;

        public AddPackets(AbstractPacketHandlerFactory phf, String pkdgName) {
            super();
            packetHandlerFactory = phf;
            upstreamPackageName = pkdgName;
            classLoader = AbstractPacketHandlerFactory.class.getClassLoader();
        }
    }

    static final class AddUpstreamPackets extends AddPackets {
        private static final Logger logger = Logger
                .getLogger(AddUpstreamPackets.class);

        public AddUpstreamPackets(AbstractPacketHandlerFactory phf,
                                  String pkdgName) {
            super(phf, pkdgName);
        }

        @Override
        public boolean execute(int number, PacketTemplate template) {
            String fullPath = upstreamPackageName + template.getName();
            try {
                packetHandlerFactory.addPacket(classLoader.loadClass(fullPath)
                        .asSubclass(SendablePacket.class), template
                        .getTemplateId());
            } catch (ClassNotFoundException e) {
                logger.warn("Class " + fullPath + " not found", e);
            } catch (ClassCastException e) {
                logger.warn("Class " + fullPath
                        + " can't cast to SendablePacket.class", e);
            }
            return true;
        }
    }

    static final class AddDownstreamPackets extends AddPackets {
        private static final Logger logger = Logger
                .getLogger(AddDownstreamPackets.class);

        public AddDownstreamPackets(AbstractPacketHandlerFactory phf,
                                    String pkdgName) {
            super(phf, pkdgName);
        }

        @Override
        public boolean execute(int number, PacketTemplate template) {
            String fullPath = upstreamPackageName + template.getName();
            try {
                // Get Spring bean
                ReceivablePacket packet = ServiceContent.getContext().getBean(classLoader.loadClass(fullPath).asSubclass(ReceivablePacket.class));
                packet.setOpCode(template.getTemplateId());
                packetHandlerFactory.addPacket(packet, template.getState());
            } catch (ClassCastException e) {
                logger.warn("Class " + fullPath + " can't cast to ReceivablePacket.class", e);
            } catch (ClassNotFoundException e) {
                logger.warn("Class " + fullPath + " not found", e);
            }
            return true;
        }
    }

    /**
     * Adds the list.
     *
     * @param pl the pl
     */
    public void addList(PacketData pl) {
        for (PacketList plist : pl.templates) {
            switch (plist.getDirection()) {
                case DOWNSTREAM:
                    plist.data.forEachEntry(new AddDownstreamPackets(this, plist.getPackageName()));
                    break;
                case UPSTREAM:
                    plist.data.forEachEntry(new AddUpstreamPackets(this, plist.getPackageName()));
                    break;
                default:
                    logger.warn("Unknown packets derection in configuration files.");
                    break;
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ascnhalf.commons.network.handlers.PacketHandlerFactory#addPacket(org
     * .jmangos.commons.network.model.ReceivablePacket,
     * net.ascnhalf.commons.network.model.State[])
     */
    public void addPacket(ReceivablePacket packetPrototype, State... states) {
        cHandler.addPacketOpcode(packetPrototype, states);

    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.handlers.PacketHandlerFactory#
     * getServerPacketopCode(net.ascnhalf.commons.network.model.SendablePacket)
     */
    public int getServerPacketopCode(SendablePacket packetClass) {
        return sHandler.getOpCode(packetClass);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ascnhalf.commons.network.handlers.PacketHandlerFactory#handleClientPacket
     * (int, net.ascnhalf.commons.network.model.NetworkChannel)
     */
    public ReceivablePacket handleClientPacket(int id, NetworkChannel ch) {
        return cHandler.getPacket(id, ch);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ascnhalf.commons.network.handlers.PacketHandlerFactory#addPacket(java
     * .lang.Class, int)
     */
    public void addPacket(Class<? extends SendablePacket> packetPrototype,
                          int opcode) {
        sHandler.addPacketOpcode(packetPrototype, opcode);

    }
}
