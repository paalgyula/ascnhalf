package net.ascnhalf.commons.service;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

/**
 * The Class ServiceContent.
 */
public final class ServiceContent {

    /**
     * The injector.
     */
    private static ApplicationContext applicationContext;

    /**
     * Sets the Spring's ApplicationContext
     *
     * @param ctx The application context
     * @return instance
     */
    public static final void setContext(ApplicationContext ctx) {
        ServiceContent.applicationContext = ctx;
    }

    /**
     * Gets the injector.
     *
     * @return the injector
     */
    public static final ApplicationContext getContext() {
        return applicationContext;
    }

    public static final <T> T createBean(Class<T> beanClass) {
        return (T) applicationContext.getAutowireCapableBeanFactory().createBean(beanClass, AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
    }

    public static final AutowireCapableBeanFactory getBeanFactory() {
        return applicationContext.getAutowireCapableBeanFactory();
    }

    public static final <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    public static final <T> T getBean(String qualifier, Class<T> clazz) {
        return applicationContext.getBean(qualifier, clazz);
    }
}
