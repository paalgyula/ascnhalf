package net.ascnhalf.tools;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.09.02.
 * Time: 11:14
 * To change this template use File | Settings | File Templates.
 */
public class ValuesPanel extends JFrame {
    JPanel panel;

    public ValuesPanel() {
        setTitle("UpdateValues");
        panel = new JPanel(new GridLayout(0, 3));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setContentPane(panel);
    }

    public void addField(String label, String value, int type) {
        panel.add(new JLabel(label));
        panel.add(new JTextField(value));
        panel.add(new JComboBox<String>(new String[]{"uint32", "short", "byte"}));
    }

    public void display() {
        pack();
        setVisible(true);
    }
}
