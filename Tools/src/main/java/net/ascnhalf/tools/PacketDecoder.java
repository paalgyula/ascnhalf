package net.ascnhalf.tools;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.BitSet;
import java.util.zip.InflaterOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.09.02.
 * Time: 10:13
 * To change this template use File | Settings | File Templates.
 */
public class PacketDecoder {
    private JPanel panel1;
    private JButton decodeButton;
    private JTextArea inputArea;
    private JTextArea outputArea;
    private JTextArea logArea;
    private JCheckBox cbCompressed;
    private JComboBox packetCombo;

    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Swing".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (UnsupportedLookAndFeelException e) {
            // handle exception
        } catch (ClassNotFoundException e) {
            // handle exception
        } catch (InstantiationException e) {
            // handle exception
        } catch (IllegalAccessException e) {
            // handle exception
        }

        JFrame frame = new JFrame("PacketDecoder");
        frame.setContentPane(new PacketDecoder().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public PacketDecoder() {
        decodeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String bytes = inputArea.getText();
                String[] str = bytes.split("( )|\n");

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                for (String b : str) {
                    baos.write(((byte) Integer.parseInt(b, 16)) & 0xFF);
                }

                switch (packetCombo.getSelectedItem().toString().toUpperCase()) {
                    case "SMSG_UPDATE_OBJECT":
                        decodeUpdateObject(baos);
                        break;
                    case "CMSG_JOIN_CHANNEL":
                        decodeChannelJoin(baos);
                        break;
                }
            }
        });
    }

    public void decodeChannelJoin(ByteArrayOutputStream baos) {
        //01 00 47 65 6E 65 72 61 6C 20 2D 20 45 6C 77 79 6E 6E 20 46 6F 72 65 73 74 00 00
        System.out.println(new String(baos.toByteArray()));
    }

    public void decodeUpdateObject(ByteArrayOutputStream baos) {
        byte[] outBytes = baos.toByteArray();

        if (cbCompressed.isSelected()) {
            ByteArrayOutputStream deflaterStream = new ByteArrayOutputStream();
            InflaterOutputStream deflater = new InflaterOutputStream(deflaterStream);
            try {
                deflater.write(baos.toByteArray());
            } catch (IOException e2) {
                logArea.append("\n" + e2.getMessage());
            }
            outBytes = deflaterStream.toByteArray();
        }

        outputArea.setText("");
        for (int i = 0; i < outBytes.length; i++) {
            if (i % 8 == 0)
                outputArea.append("\n");

            outputArea.append(String.format("(byte)0x%02x, ", outBytes[i]));
        }

        ByteArrayInputStream bais = new ByteArrayInputStream(outBytes);
        byte[] readable = new byte[4];
        try {
            bais.read(readable);
            int blockCount = ByteBuffer.wrap(readable).order(ByteOrder.LITTLE_ENDIAN).getInt();
            System.out.println("Block count: " + blockCount);

            ValuesPanel vp = new ValuesPanel();
            vp.addField("BlockCount", Integer.toString(blockCount), 0);
            vp.addField("UpdateType", Integer.toString(bais.read()), 0);
            vp.addField("WoWGuid", Long.toString(readPackedGUID(bais)), 0);
            vp.display();
        } catch (IOException e3) {
            e3.printStackTrace();
        }

        bais.read();
    }

    private long readPackedGUID(ByteArrayInputStream stream) throws IOException {
        BitSet bs = BitSet.valueOf(new byte[]{(byte) stream.read()});

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        for (int i = 0; i < bs.size(); i++) {
            if (bs.get(i))
                bout.write(stream.read());
            else
                bout.write(0x00);
        }

        if (bout.size() > 4)
            return ByteBuffer.wrap(bout.toByteArray()).order(ByteOrder.LITTLE_ENDIAN).getLong();
        if (bout.size() > 2)
            return ByteBuffer.wrap(bout.toByteArray()).order(ByteOrder.LITTLE_ENDIAN).getInt();
        return ByteBuffer.wrap(bout.toByteArray()).order(ByteOrder.LITTLE_ENDIAN).getShort();
    }
}
