package net.ascnhalf.auth;

import net.ascnhalf.auth.console.ConsoleManager;
import net.ascnhalf.auth.dao.AccountDAO;
import net.ascnhalf.auth.service.BanIpService;
import net.ascnhalf.auth.service.RealmListService;
import net.ascnhalf.auth.utils.ShutdownHook;
import net.ascnhalf.commons.network.netty.service.NetworkService;
import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.commons.threadpool.ThreadPoolManager;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

/**
 * The AuthServer main class.
 */
public class AuthServer {

    public static Locale locale;

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("classpath:authConfig.xml");

        AuthServer.locale = Locale.forLanguageTag(appContext.getBean("defaultLocale", String.class));

        Logger log = Logger.getLogger(AuthServer.class.getName());

        ResourceBundleMessageSource messageSource = appContext.getBean(ResourceBundleMessageSource.class);
        log.info(messageSource.getMessage("auth.authserver.messageSetToLocale", new String[]{AuthServer.locale.getCountry()}, AuthServer.locale));

        ServiceContent.setContext(appContext);

        appContext.getBean(RealmListService.class).start();
        appContext.getBean(BanIpService.class).start();
        appContext.getBean(ThreadPoolManager.class).start();
        //appContext.getBean( JmxRealmList.class ).start();
        //appContext.getBean( JmxNetworkService.class ).start();

        Runtime.getRuntime().addShutdownHook(appContext.getBean(ShutdownHook.class));
        System.gc();

        appContext.getBean(NetworkService.class).start();

        new ConsoleManager(appContext.getBean(AccountDAO.class));
    }
}