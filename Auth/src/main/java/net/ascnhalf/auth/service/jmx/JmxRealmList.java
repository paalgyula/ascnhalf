package net.ascnhalf.auth.service.jmx;

import net.ascnhalf.auth.service.RealmListService;
import net.ascnhalf.commons.jmx.AbstractJmxBeanService;
import net.ascnhalf.commons.model.Realm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Map.Entry;

@Service
@Scope("singleton")
public class JmxRealmList extends AbstractJmxBeanService implements
        JmxRealmListMBean {

    @Autowired
    private RealmListService realmListService;

    private static final String BEAN_NAME = "auth.network.realms:name=JMXRealmList";

    @Override
    protected String getBeanName() {
        return BEAN_NAME;
    }

    @Override
    public String getRealmCount() {
        return String.valueOf(realmListService.getRealmCount());
    }

    @Override
    public String printRealmInfo() {
        StringBuffer sb = new StringBuffer();
        for (Entry<Integer, Realm> it : realmListService.getRealms().entrySet()) {
            sb.append("Realm info for: " + it.getKey() + " realm");
            sb.append("\n\tName: " + it.getValue().getName());
            sb.append("\n\tPopulation: " + it.getValue().getPopulation());
            sb.append("\n");
        }
        return sb.toString();
    }

}
