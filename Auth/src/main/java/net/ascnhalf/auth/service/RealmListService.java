/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.service;

import javolution.util.FastMap;
import net.ascnhalf.auth.AuthServer;
import net.ascnhalf.auth.dao.RealmDAO;
import net.ascnhalf.commons.model.Realm;
import net.ascnhalf.commons.service.Service;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ResourceBundleMessageSource;

@org.springframework.stereotype.Service
@Scope("singleton")
public class RealmListService implements Service {
    /**
     * Logger for this class.
     */
    private final Logger log = Logger.getLogger(getClass());

    /**
     * Map with realms
     */
    private FastMap<Integer, Realm> realms = new FastMap<Integer, Realm>()
            .shared();

    /**
     * The byte size.
     */
    private int byteSize;

    /**
     * The realm dao.
     */
    @Autowired
    private RealmDAO realmDAO;

    @Autowired
    private ResourceBundleMessageSource messageSource;

    private long nextUpdateTime = 0;

    /**
     * Gets the worlds.
     *
     * @return the worlds
     */
    @SuppressWarnings("unused")
    public FastMap<Integer, Realm> getWorlds() {
        return realms;
    }

    @SuppressWarnings("unused")
    public void addFromConnected(Realm newRealm) {
        if (realms.containsKey(newRealm.getId())) {
            log.debug(messageSource.getMessage("auth.realmlistservice.serverAlreadyConnected", null, AuthServer.locale));
            realms.remove(newRealm.getId());
            realms.put(newRealm.getId(), newRealm);
        } else {
            realms.put(newRealm.getId(), newRealm);
            byteSize = calculateWorldsSize();
        }
    }

    /**
     * Loads list of banned ip.
     */
    public void start() {
        update();
        log.debug("WorldList loaded " + realms.size() + " realms.");

    }

    /**
     * Update if need
     */
    synchronized public void update() {
        if (nextUpdateTime > System.currentTimeMillis()) {
            return;
        }
        long UPDATE_INTERVAL = 2000;
        nextUpdateTime = System.currentTimeMillis() + UPDATE_INTERVAL;
        FastMap<Integer, Realm> trealms = realmDAO.getAllRealms();
        for (Realm realm : trealms.values()) {
            if (realms.containsKey(realm.getId())) {
                realms.get(realm.getId()).setPopulation(realm.getPopulation());
            } else {
                realms.put(realm.getId(), realm);
            }
        }
        // update byte size all realms
        byteSize = calculateWorldsSize();
    }

    /**
     * Gets the byte size.
     *
     * @return the byteSize
     */
    @SuppressWarnings("unused")
    public int getByteSize() {
        return byteSize;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    @SuppressWarnings("unused")
    public int getRealmCount() {
        return realms.size();
    }

    /**
     * Calculate worlds size.
     *
     * @return the int
     */
    public int calculateWorldsSize() {
        int value = 8;
        for (Realm realm : realms.values()) {
            value += realm.getSize();
        }
        return value;
    }

    /**
     * Gets the amount characters.
     *
     * @param id the id
     * @return the amount characters
     */
    @SuppressWarnings("unused")
    public FastMap<Integer, Integer> getAmountCharacters(Integer id) {
        return getWorldDAO().getAmountCharacters(id);
    }

    private RealmDAO getWorldDAO() {
        return realmDAO;
    }

    /**
     * @return the realms
     */
    public final FastMap<Integer, Realm> getRealms() {
        return realms;
    }

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        realms.clear();
    }

}
