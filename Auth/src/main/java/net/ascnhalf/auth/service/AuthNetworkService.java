/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.service;

import net.ascnhalf.auth.config.Config;
import net.ascnhalf.commons.network.handlers.PacketHandlerFactory;
import net.ascnhalf.commons.network.netty.service.AbstractNetworkService;
import org.apache.commons.lang.NotImplementedException;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class LoginNetworkService.
 */
@Service
@Scope("singleton")
public class AuthNetworkService extends AbstractNetworkService {

    @Autowired
    private Config config;

    /**
     * The auth to client pipeline factory.
     */
    @Autowired
    @Qualifier("AuthToClient")
    private ChannelPipelineFactory authToClientPipelineFactory;

    /**
     * The packet service.
     */
    @Autowired
    @Qualifier("AuthToClient")
    private PacketHandlerFactory packetService;

    /**
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {
        packetService.loadPacket();
        createServerChannel(config.CLIENT_ADDRESS, authToClientPipelineFactory);
    }

    /**
     * @see net.ascnhalf.commons.network.netty.service.NetworkService#status()
     */
    @Override
    public void status() {
        throw new NotImplementedException();
    }

    /**
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        throw new NotImplementedException();
    }

}
