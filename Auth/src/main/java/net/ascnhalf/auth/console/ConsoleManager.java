package net.ascnhalf.auth.console;

import jline.console.ConsoleReader;
import net.ascnhalf.auth.dao.AccountDAO;
import net.ascnhalf.commons.model.Account;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: Felhasználó
 * Date: 2013.06.04.
 * Time: 0:09
 * To change this template use File | Settings | File Templates.
 */
public class ConsoleManager {

    private final Logger log = Logger.getLogger(ConsoleManager.class);

    public ConsoleManager(AccountDAO accountDAO) {
        try {
            ConsoleReader console = new ConsoleReader();
            console.setPrompt("[ AuthServer ]> ");
            String line = null;

            while ((line = console.readLine()) != null) {
                String[] command = line.split(" ");
                switch (command[0].toLowerCase()) {
                    case "exit":
                    case "quit":
                        System.exit(0);
                        break;
                    case "gc":
                        System.gc();
                        break;
                    case "register":
                        if (command.length != 3) {
                            console.println("usage: register [username] [password]");
                            break;
                        }

                        Account account = new Account();
                        account.setName(command[1].toUpperCase());
                        String userPass = command[1].toUpperCase() + ":" + command[2].toUpperCase();

                        try {
                            MessageDigest sha = MessageDigest.getInstance("SHA1");
                            String passwordHash = new BigInteger(sha.digest(userPass.getBytes())).toString(16);
                            account.setPasswordHash(passwordHash);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }


                        try {
                            accountDAO.persist(account);
                            console.println("Account [" + account.getName() + "] persisted. (" + account.getPasswordHash() + ")");
                        } catch (Exception e) {
                            log.error("Account persist failed!", e);
                            console.println("Account persist failed!");
                        }
                        break;
                    default:
                        console.println("Invalid command! If you need command list type help");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
