package net.ascnhalf.auth.config;

import net.ascnhalf.commons.config.Compatiple;
import net.ascnhalf.commons.configuration.AbstractConfig;
import net.ascnhalf.commons.configuration.Property;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.net.InetSocketAddress;

/**
 * The Class Config.
 *
 * @author MinimaJack
 */
@Service
@Scope("singleton")
public class Config extends AbstractConfig {

    /**
     * The Constant CONFIG_FILE.
     */
    private static final String CONFIG_FILE = "conf/network/auth.network.properties";

    /**
     * Login Server address to client.
     */
    @Property(key = "network.client.address", defaultValue = "*:3724")
    public InetSocketAddress CLIENT_ADDRESS;

    /**
     * The UPDAT e_ interval.
     */
    @Property(key = "network.service.updateRealmlistInterval", defaultValue = "60")
    public Integer UPDATE_INTERVAL;

    /**
     * The COMPATIBLE.
     */
    @Property(key = "network.compatible", defaultValue = "NONE")
    public Compatiple COMPATIBLE;

    /**
     * Load configuration.
     */
    public Config() {
        super(CONFIG_FILE);
    }
}
