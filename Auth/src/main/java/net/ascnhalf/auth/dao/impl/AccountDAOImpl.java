/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.dao.impl;

import net.ascnhalf.auth.dao.AccountDAO;
import net.ascnhalf.commons.model.Account;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO that manages accounts.
 */
@Repository("accountDAO")
public class AccountDAOImpl implements AccountDAO {
    Logger logger = Logger.getLogger(AccountDAOImpl.class);

    @Autowired
    @Qualifier("accountSessionFactory")
    SessionFactory accountSessionFactory;

    /**
     * Returns account by name or null.
     *
     * @param name account name
     * @return account object or null
     */
    @Transactional
    public Account getAccount(String name) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :name").setString("name", name);
        return (Account) query.uniqueResult();
    }

    /**
     * Retuns account id or -1 in case of error.
     *
     * @param name name of account
     * @return id or -1 in case of error
     */
    @Transactional
    public int getAccountId(String name) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :name").setString("name", name);
        Account account = (Account) query.uniqueResult();
        if (account == null)
            return -1;
        return account.getId();
    }

    /**
     * Reruns account count If error occured - returns -1.
     *
     * @return account count
     */
    @Transactional
    public int getAccountCount() {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a");
        int accCount = query.list().size();
        return accCount;
    }

    /**
     * Update security key.
     *
     * @param account the account
     * @return true, if successful
     */
    @Transactional
    public boolean updateSecurityKey(Account account) {
        Session session = accountSessionFactory.getCurrentSession();
        session.saveOrUpdate(account);
        return true;
    }

    /**
     * Updates lastServer field of account.
     *
     * @param accountId  account id
     * @param lastServer last accessed server
     * @return was updated successful or not
     */
    @Transactional
    public boolean updateLastServer(int accountId, byte lastServer) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :id").setInteger("id", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            logger.warn("Account with id (" + accountId + ") not found!");
            return false;
        }

        account.setLastServer(lastServer);
        session.saveOrUpdate(account);
        return true;
    }

    /**
     * Updates last ip that was used to access an account.
     *
     * @param accountId account id
     * @param ip        ip address
     * @return was update successful or not
     */
    @Transactional
    public boolean updateLastIp(int accountId, String ip) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :id").setInteger("id", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            logger.warn("Account with id (" + accountId + ") not found!");
            return false;
        }

        account.setLastIp(ip);
        session.saveOrUpdate(account);
        logger.info(String.format("Account (#%d) updated. New IP: %s.", accountId, ip));
        return true;
    }

    /**
     * Get last ip that was used to access an account.
     *
     * @param accountId account id
     * @return ip address
     */
    @Transactional
    public String getLastIp(int accountId) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :accountid").setInteger("accountid", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            logger.warn("Account with id (" + accountId + ") not found!");
            return "";
        }

        String lastIp = account.getLastIp();
        return lastIp;
    }

    /**
     * Update session key.
     *
     * @param username the username
     * @param key      the key
     * @return true, if successful
     */
    @Transactional
    public boolean updateSessionKey(String username, String key) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :username").setString("username", username);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            logger.warn("Account with id (" + username + ") not found!");
            return false;
        }

        account.setSessionKey(key);
        session.saveOrUpdate(account);
        return true;
    }

    /**
     * Gets the session key.
     *
     * @param username the username
     * @return the session key
     */
    @Transactional
    public String getSessionKey(String username) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :username").setString("username", username);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            logger.warn("Account with id (" + username + ") not found!");
            return "";
        }

        return account.getMsessionKey();
    }

    @Transactional
    @Override
    public void persist(Account account) {
        accountSessionFactory.getCurrentSession().persist(account);
    }
}
