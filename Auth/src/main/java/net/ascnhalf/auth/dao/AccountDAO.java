/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.dao;

import net.ascnhalf.commons.model.Account;
import org.apache.log4j.Logger;

/**
 * DAO that manages accounts.
 *
 * @author MinimaJack
 */
public interface AccountDAO {
    Logger logger = Logger.getLogger(AccountDAO.class);

    /**
     * Returns account by name or null.
     *
     * @param name account name
     * @return account object or null
     */
    public Account getAccount(String name);

    /**
     * Retuns account id or -1 in case of error.
     *
     * @param name name of account
     * @return id or -1 in case of error
     */
    public int getAccountId(String name);

    /**
     * Reruns account count If error occured - returns -1.
     *
     * @return account count
     */
    public int getAccountCount();

    /**
     * Update security key.
     *
     * @param account the account
     * @return true, if successful
     */
    public boolean updateSecurityKey(Account account);

    /**
     * Updates lastServer field of account.
     *
     * @param accountId  account id
     * @param lastServer last accessed server
     * @return was updated successful or not
     */
    public boolean updateLastServer(int accountId, byte lastServer);

    /**
     * Updates last ip that was used to access an account.
     *
     * @param accountId account id
     * @param ip        ip address
     * @return was update successful or not
     */
    public boolean updateLastIp(int accountId, String ip);

    /**
     * Get last ip that was used to access an account.
     *
     * @param accountId account id
     * @return ip address
     */
    public String getLastIp(int accountId);

    /**
     * Update session key.
     *
     * @param username the username
     * @param key      the key
     * @return true, if successful
     */
    public boolean updateSessionKey(String username, String key);

    /**
     * Gets the session key.
     *
     * @param username the username
     * @return the session key
     */
    public String getSessionKey(String username);

    /**
     * Persists account to the database
     *
     * @param account
     */
    public void persist(Account account);
}
