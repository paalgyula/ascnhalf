/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.dao;

import javolution.util.FastMap;
import net.ascnhalf.commons.model.Realm;

/**
 * The Class WorldDAO.
 *
 * @author MinimaJack
 */
public interface RealmDAO {

    /**
     * Gets the all worlds.
     *
     * @return the all worlds
     */
    public FastMap<Integer, Realm> getAllRealms();

    /**
     * Gets the amount characters.
     *
     * @param id the realm id
     * @return the amount characters
     */
    public FastMap<Integer, Integer> getAmountCharacters(final Integer id);

}
