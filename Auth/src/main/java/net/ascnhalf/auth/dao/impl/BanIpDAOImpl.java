package net.ascnhalf.auth.dao.impl;

import net.ascnhalf.auth.dao.BanIpDAO;
import net.ascnhalf.commons.model.BanIp;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * The Class BanIpDAO.
 */
@Repository("banIpDAO")
public class BanIpDAOImpl implements BanIpDAO {
    private final Logger log = Logger.getLogger(getClass());

    @Autowired
    SessionFactory accountSessionFactory;

    /**
     * Inserts ip mask to database, returns BannedIP object that represents
     * inserted mask or null if error.<br>
     * Expire time is null so ban never expires.<br>
     *
     * @param mask ip mask to ban
     * @return BannedIP object represetns mask or null if error happened
     */
    public BanIp insert(String mask) {
        return insert(mask, null);
    }

    /**
     * Inserts ip mask to database with given expireTime.<br>
     * Null is allowed for expire time in case of infinite ban.<br>
     * Returns object that represents ip mask or null in case of error.<br>
     *
     * @param mask       ip mask to ban
     * @param expireTime expiration time of ban
     * @return object that represetns added ban or null in case of error
     */
    public BanIp insert(String mask, Timestamp expireTime) {
        BanIp banIp = new BanIp(mask, expireTime);
        return insert(banIp);
    }

    /**
     * Inserts BannedIP object to database.<br>
     * ID of object must be NULL.<br>
     * If insert was successfull - sets the assigned id to BannedIP object and
     * returns true.<br>
     * In case of error returns false without modification of bannedIP object.<br>
     *
     * @param bannedIp record to add to db
     * @return true in case of success or false
     */
    @Transactional
    public BanIp insert(BanIp bannedIp) {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        hibernateSession.saveOrUpdate(bannedIp);
        return bannedIp;
    }

    /**
     * Updates BannedIP object.<br>
     * ID of object must NOT be null.<br>
     * In case of success returns true.<br>
     * In case of error returns false.<br>
     *
     * @param bannedIp record to update
     * @return true in case of success or false in other case
     */
    @Transactional
    public boolean update(BanIp bannedIp) {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        hibernateSession.saveOrUpdate(bannedIp);
        return true;
    }

    /**
     * Removes ban by mask.<br>
     * Returns true in case of success, false othervise.<br>
     *
     * @param ip the ip
     * @return true in case of success, false in other case
     */
    @Transactional
    public boolean remove(String ip) {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        hibernateSession.delete(BanIp.class.getSimpleName(), ip);
        return true;
    }

    /**
     * Removes BannedIP record by ID. Id must not be null.<br>
     * Returns true in case of success, false in case of error
     *
     * @param bannedIp record to unban
     * @return true if removeas wass successfull, false in case of error
     */
    @Transactional
    public boolean remove(BanIp bannedIp) {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        hibernateSession.delete(bannedIp);
        return true;
    }

    /**
     * Returns all bans from database.
     *
     * @return all bans from database.
     */
    @Transactional
    public List<BanIp> getAllBans() {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        Criteria crit = hibernateSession.createCriteria(BanIp.class);
        return (List<BanIp>) crit.list();
    }

    /**
     * Gets the ban.
     *
     * @param ip the ip
     * @return the ban
     */
    @Transactional
    public BanIp getBan(String ip) {
        Session hibernateSession = accountSessionFactory.getCurrentSession();
        return (BanIp) hibernateSession.get(BanIp.class, ip);
    }

}
