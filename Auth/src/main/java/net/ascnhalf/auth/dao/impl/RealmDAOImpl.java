/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.dao.impl;

import javolution.util.FastMap;
import net.ascnhalf.auth.dao.RealmDAO;
import net.ascnhalf.commons.model.Realm;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The implementation of WorldDAO.
 */
@Repository("realmDAO")
public class RealmDAOImpl implements RealmDAO {

    @Autowired
    SessionFactory accountSessionFactory;

    /**
     * Gets the all worlds.
     *
     * @return the all worlds
     */
    @Transactional
    public FastMap<Integer, Realm> getAllRealms() {
        Criteria criteria = accountSessionFactory.getCurrentSession().createCriteria(Realm.class);
        FastMap<Integer, Realm> realmFastMap = new FastMap<Integer, Realm>();
        @SuppressWarnings("unchecked")
        List<Realm> realmList = (List<Realm>) criteria.list();

        for (Realm realm : realmList)
            realmFastMap.put(realm.getId(), realm);

        return realmFastMap;
    }

    /**
     * Gets the amount characters.
     *
     * @param id the realm id
     * @return the amount characters
     */
    @Transactional
    public FastMap<Integer, Integer> getAmountCharacters(final Integer id) {
        // TODO:implement
        return new FastMap<Integer, Integer>();
    }

}
