/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 *
 */
package net.ascnhalf.auth.network.netty.factory;

import net.ascnhalf.auth.network.netty.handler.AuthToClientChannelHandler;
import net.ascnhalf.auth.network.netty.handler.EventLogHandler;
import net.ascnhalf.commons.network.handlers.PacketHandlerFactory;
import net.ascnhalf.commons.network.model.ConnectHandler;
import net.ascnhalf.commons.network.netty.factory.BasicPipelineFactory;
import net.ascnhalf.commons.network.netty.receiver.NettyPacketReceiver;
import org.jboss.netty.channel.ChannelPipeline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import static org.jboss.netty.channel.Channels.pipeline;

/**
 * A factory for creating Auth-Client Pipeline objects.
 *
 * @author MinimaJack
 */
@Component
@Qualifier("AuthToClient")
public class AuthToClientPipelineFactory extends BasicPipelineFactory {

    /**
     * The connection handler.
     */
    @Autowired
    @Qualifier("AuthToClient")
    private ConnectHandler connectionHandler;

    /**
     * The packet service.
     */
    @Autowired
    @Qualifier("AuthToClient")
    private PacketHandlerFactory packetService;

    /**
     * (non-Javadoc)
     *
     * @see org.jboss.netty.channel.ChannelPipelineFactory#getPipeline()
     */
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = pipeline();

        pipeline.addLast("executor", getExecutorHandler());

        pipeline.addLast("eventlog", new EventLogHandler());

        pipeline.addLast("handler", new AuthToClientChannelHandler(
                packetService, connectionHandler, new NettyPacketReceiver()));

        return pipeline;
    }
}
