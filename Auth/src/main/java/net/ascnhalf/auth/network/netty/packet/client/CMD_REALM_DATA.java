/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.netty.packet.client;

import net.ascnhalf.auth.network.netty.packet.AbstractWoWClientPacket;
import net.ascnhalf.auth.service.RealmListService;
import net.ascnhalf.commons.model.Realm;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class <tt>CMD_REALM_DATA</tt>.
 */
@Service
@Scope("prototype")
@Qualifier("CMD_REALM_DATA")
public class CMD_REALM_DATA extends AbstractWoWClientPacket {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(CMD_REALM_DATA.class);

    @Autowired
    private RealmListService realmListService;

    public CMD_REALM_DATA() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws RuntimeException {
        logger.debug("Receive realm info from realm account: "
                + getAccount().getName());
        Realm realm = new Realm();
        realm.setId(getAccount().getObjectId());
        realm.setName(readS());
        realm.setAddress(readS());
        realm.setPort(readD());
        realm.setIcon(readC());
        realm.setRealmflags(readC());
        realm.setTimezone(readC());
        realm.setAllowedSecurityLevel(readC());
        realm.setPopulation(readF());
        realm.setRealmbuilds(readS());
        realmListService.addFromConnected(realm);
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {

    }
}
