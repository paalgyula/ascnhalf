/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.netty.packet.client;

import net.ascnhalf.auth.network.netty.packet.AbstractWoWClientPacket;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.nio.BufferUnderflowException;

/**
 * The Class <tt>CMD_TEST_CRYPT</tt>.
 */
@Service
@Scope("prototype")
@Qualifier("CMD_TEST_CRYPT")
public class CMD_TEST_CRYPT extends AbstractWoWClientPacket {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(CMD_TEST_CRYPT.class);
    /**
     * The sender.
     */
    @Autowired
    private AbstractPacketSender sender;

    public CMD_TEST_CRYPT() {
        super();
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        logger.debug((char) readC());
        logger.debug((char) readC());
        logger.debug((char) readC());
        logger.debug((char) readC());
        logger.debug((char) readC());
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {

    }
}
