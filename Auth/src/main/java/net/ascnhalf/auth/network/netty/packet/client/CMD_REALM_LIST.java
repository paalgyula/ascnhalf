/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.netty.packet.client;

import net.ascnhalf.auth.config.Config;
import net.ascnhalf.auth.network.netty.packet.AbstractWoWClientPacket;
import net.ascnhalf.auth.network.netty.packet.server.TCMD_REALM_LIST;
import net.ascnhalf.auth.service.RealmListService;
import net.ascnhalf.commons.config.Compatiple;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class <tt>CMD_REALM_LIST</tt>.
 */
@Service
@Scope("prototype")
@Qualifier("CMD_REALM_LIST")
public class CMD_REALM_LIST extends AbstractWoWClientPacket {

    /**
     * The sender.
     */
    @Autowired
    private AbstractPacketSender sender;

    /**
     * The worldlist.
     */
    @Autowired
    private RealmListService worldlist;

    /**
     * The worldlist.
     */
    @Autowired
    private Config config;

    /*
     * (non-Javadoc)
     *
     * @see
     * net.ascnhalf.commons.network.model.ReceivablePacket#getMinimumLength()
     */
    @Override
    public int getMinimumLength() {
        return 4;
    }

    /**
     * Instantiates a new CMD_REALM_LIST.
     */
    public CMD_REALM_LIST() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void readImpl() {
        readB(getAvaliableBytes());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void runImpl() {
        // must be reloaded from db
        if (config.COMPATIBLE.equals(Compatiple.MANGOS)) {
            worldlist.update();
        }
        sender.send(getClient(), new TCMD_REALM_LIST(worldlist));
    }
}
