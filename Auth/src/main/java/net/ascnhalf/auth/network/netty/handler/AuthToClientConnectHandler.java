/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.netty.handler;

import net.ascnhalf.commons.network.model.ConnectHandler;
import net.ascnhalf.commons.network.model.NettyNetworkChannel;
import net.ascnhalf.commons.network.model.State;
import net.ascnhalf.commons.network.netty.service.NetworkService;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.ChannelHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * The Class AuthToClientConnectHandler.
 */
@Component
@Qualifier("AuthToClient")
public class AuthToClientConnectHandler implements ConnectHandler {

    /**
     * The Constant log.
     */
    private static final Logger log = Logger
            .getLogger(AuthToClientConnectHandler.class);

    @Autowired
    private NetworkService networkService;

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onConnect(net.ascnhalf.commons.network.model.NettyNetworkChannel, org.jboss.netty.channel.ChannelHandler)
     */
    @Override
    public void onConnect(NettyNetworkChannel networkChannel,
                          ChannelHandler handler) {
        networkChannel.setChannelState(State.CONNECTED);
        networkService.registerClientChannel(networkChannel);
        log.info("Accepting connection from: "
                + networkChannel.getAddress().getHostName());

    }

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onDisconnect(net.ascnhalf.commons.network.model.NettyNetworkChannel)
     */
    @Override
    public void onDisconnect(NettyNetworkChannel networkChannel) {
        log.info("Disconnection : " + networkChannel.getAddress().getHostName());

    }

}
