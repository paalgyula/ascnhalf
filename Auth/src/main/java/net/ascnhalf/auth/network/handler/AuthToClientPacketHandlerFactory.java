/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.handler;

import net.ascnhalf.commons.network.handlers.AbstractPacketHandlerFactory;
import net.ascnhalf.commons.network.netty.model.PacketData;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author MinimaJack
 */
@Component
@Qualifier("AuthToClient")
public class AuthToClientPacketHandlerFactory extends
        AbstractPacketHandlerFactory {
    /*
     * (non-Javadoc)
     *
     * @see
     * net.ascnhalf.commons.network.handlers.PacketHandlerFactory#loadPacket()
     */
    @Override
    public void loadPacket() {
        addList(loadStaticData(PacketData.class,
                packetXSDLocation,
                clientPacketPath));

    }
}
