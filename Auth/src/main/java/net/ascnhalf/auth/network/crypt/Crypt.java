/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.crypt;

import net.ascnhalf.commons.network.crypt.SARC4;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * The Class Crypt.
 */
public class Crypt {

    /**
     * The Server encryption key.
     */
    byte[] clientDecryptionKey = {(byte) 0xCC, (byte) 0x98, (byte) 0xAE, 0x04,
            (byte) 0xE8, (byte) 0x97, (byte) 0xEA, (byte) 0xCA, 0x12,
            (byte) 0xDD, (byte) 0xC0, (byte) 0x93, 0x42, (byte) 0x91, 0x53,
            0x57};

    /**
     * The Server decryption key.
     */
    byte[] serverEncryptionKey = {(byte) 0xC2, (byte) 0xB3, 0x72, 0x3C,
            (byte) 0xC6, (byte) 0xAE, (byte) 0xD9, (byte) 0xB5, 0x34, 0x3C,
            0x53, (byte) 0xEE, 0x2F, 0x43, 0x67, (byte) 0xCE};

    /**
     * The is enabled.
     */
    private boolean isEnabled = false;

    /**
     * The client decrypt.
     */
    private SARC4 clientDecryptSARC4 = new SARC4();

    /**
     * The server encrypt.
     */
    private SARC4 serverEncryptSARC4 = new SARC4();

    /**
     * Instantiates a new crypt.
     */
    public Crypt() {
    }

    /**
     * Decrypt.
     *
     * @param data the data
     * @return the byte[]
     */
    public byte[] decrypt(byte[] data) {
        if (!isEnabled)
            return data;
        return clientDecryptSARC4.update(data);
    }

    /**
     * Encrypt.
     *
     * @param data the data
     * @return the byte[]
     */
    public byte[] encrypt(byte[] data) {
        if (!isEnabled)
            return data;
        return serverEncryptSARC4.update(data);

    }

    /**
     * Inits the.
     *
     * @param key the key
     */
    public void init(byte[] key) {
        byte[] decryptHash = getKey(clientDecryptionKey, key);

        serverEncryptSARC4.init(decryptHash);
        byte[] encryptHash = getKey(serverEncryptionKey, key);

        clientDecryptSARC4.init(encryptHash);
        byte[] tar = new byte[1024];
        for (int i = 0; i < tar.length; i++) {
            tar[i] = 0;
        }
        serverEncryptSARC4.update(tar);
        for (int i = 0; i < tar.length; i++) {
            tar[i] = 0;
        }
        clientDecryptSARC4.update(tar);
        this.isEnabled = true;
    }

    /**
     * Gets the key.
     *
     * @param EncryptionKey the encryption key
     * @param key           the key
     * @return the key
     */
    private byte[] getKey(byte[] EncryptionKey, byte[] key) {
        SecretKeySpec ds = new SecretKeySpec(EncryptionKey, "HmacSHA1");
        Mac m;
        try {
            m = Mac.getInstance("HmacSHA1");
            m.init(ds);
            return m.doFinal(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
