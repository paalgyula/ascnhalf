/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.network.netty.packet.server;

import net.ascnhalf.auth.network.netty.packet.AbstractWoWServerPacket;
import net.ascnhalf.auth.utils.AccountUtils;
import net.ascnhalf.commons.model.Account;
import net.ascnhalf.commons.model.WoWAuthResponse;
import net.ascnhalf.commons.utils.BigNumber;

/**
 * The Class <tt>TCMD_AUTH_LOGON_CHALLENGE</tt>.
 */
public class TCMD_AUTH_LOGON_CHALLENGE extends AbstractWoWServerPacket {

    /**
     * The response.
     */
    private WoWAuthResponse response;

    /**
     * Constructs new instance of <tt>TCMD_AUTH_LOGON_CHALLENGE</tt> packet.
     *
     * @param response auth response
     */
    public TCMD_AUTH_LOGON_CHALLENGE(WoWAuthResponse response) {
        this.response = response;
    }

    /**
     *
     */
    public TCMD_AUTH_LOGON_CHALLENGE() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeImpl() {
        writeC(0x00);
        writeC(response.getMessageId());
        if (response == WoWAuthResponse.WOW_SUCCESS) {
            BigNumber unk3 = new BigNumber();
            unk3.setRand(16);
            writeB(((Account) (getChannel().getChanneledObject())).getcryptoB()
                    .asByteArray(32));
            writeC(1);
            writeB(AccountUtils.g.asByteArray(1));
            writeC(32);
            writeB(AccountUtils.N.asByteArray(32));
            writeB(((Account) (getChannel().getChanneledObject()))
                    .getS_crypto().asByteArray(32));
            writeB(unk3.asByteArray(16));
            writeC(0);
        }
    }
}
