/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.auth.module;

/**
 * The Class HandlerDM.
 */
public class HandlerDM {

	/**
	 * @see com.google.inject.AbstractModule#configure()
	 */
    /*
	@Override
	protected void configure() {
        	install(new CommonModule());

          bind(String.class).annotatedWith(Names.named("toClient")).toInstance(
                  "./conf/packetData/lc-packets.xml");

          bind(NetworkService.class).to(AuthNetworkService.class).in(
                  Scopes.SINGLETON);
          bind(ThreadPoolManager.class).to(CommonThreadPoolManager.class).in(
                  Scopes.SINGLETON);

          bind(PacketHandlerFactory.class)
                  .annotatedWith(Names.named("AuthToClient"))
                  .to(AuthToClientPacketHandlerFactory.class)
                  .in(Scopes.SINGLETON);
          bind(ChannelPipelineFactory.class)
                  .annotatedWith(Names.named("AuthToClient"))
                  .to(AuthToClientPipelineFactory.class).in(Scopes.SINGLETON);

          bind(ConnectHandler.class).annotatedWith(Names.named("AuthToClient"))
                  .to(AuthToClientConnectHandler.class).in(Scopes.SINGLETON);
          bind(AbstractPacketSender.class).to(NettyPacketSender.class).in(
                  Scopes.SINGLETON);

          bind(RealmDAO.class).in(Scopes.SINGLETON);
          bind(BanIpDAO.class).in(Scopes.SINGLETON);
          bind(AccountDAO.class).in(Scopes.SINGLETON);

          bind(RealmListService.class).in(Scopes.SINGLETON);
          bind(BanIpService.class).in(Scopes.SINGLETON);

          bind(DatabaseFactory.class).in(Scopes.SINGLETON);
          bind(AccountService.class).in(Scopes.SINGLETON);
          bind(ShutdownHook.class).in(Scopes.SINGLETON);

          bind(JmxRealmList.class).in(Scopes.SINGLETON);

      }


      @Provides
      @Singleton
      public Config provideConfig() {
          return new Config();
      }

      @Provides
      @Singleton
      public DatabaseConfig provideDatabaseConfig() {
          return new DatabaseConfig();
      } */
}
