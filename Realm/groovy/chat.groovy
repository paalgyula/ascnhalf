import net.ascnhalf.realm.model.ChatType
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.model.player.chat.ChatColor
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_MESSAGE_CHAT

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 12:16
 */

player = (Player) player;
sender.send(player.getChannel(), new SMSG_MESSAGE_CHAT("Hali!", ChatType.CHAT_MSG_YELL));
player.sendAreaTriggerMessage(String.format("[%sAscNHalf|r] Deelopment Server", ChatColor.MSG_COLOR_LIGHTBLUE.getValue()));