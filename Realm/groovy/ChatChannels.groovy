import net.ascnhalf.commons.service.ServiceContent
import net.ascnhalf.realm.service.ChatService

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 21:15
 */
ChatService chatService = ServiceContent.getInjector().getInstance(ChatService.class);
Map<String, ChatService.ChatChannel> chanMap = chatService.channels;

chanMap.keySet().each {
    println it;
    println chanMap.get(it).playerList
}