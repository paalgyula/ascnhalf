import net.ascnhalf.realm.model.base.update.PlayerFields
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_LEARN_SPELL

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 21:11
 */
player = (Player) player;

sender.send(player.getChannel(), new SMSG_LEARN_SPELL(668)); // Common language
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(669)); // Orcish
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(670)); // Taurahe
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(671)); // Darnassian
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(672)); // Dwarfen
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(813)); // THALASSIAN
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(814)); // Dragonic
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(815)); // Demon tongue
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(816)); // Titan
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(815)); // Old tongue
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(7430)); // Gnomish
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(7341)); // Troll
sender.send(player.getChannel(), new SMSG_LEARN_SPELL(17737)); // Glutterspeak

for (int i = PlayerFields.PLAYER_SKILL_INFO_1_1.getValue(); i < PlayerFields.PLAYER_CHARACTER_POINTS1.getValue(); i += 3) {
    player.SetUInt32Value(i, 98);
    player.SetUInt32Value(++i, 450 << 16 | 450);
    player.SetUInt32Value(++i, 0);
}

player.update();
player.clearBits();