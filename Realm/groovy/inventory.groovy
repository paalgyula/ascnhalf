import net.ascnhalf.realm.model.UpdateType
import net.ascnhalf.realm.model.base.update.PlayerFields
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_UPDATE_OBJECT

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.08.30.
 * Time: 21:14
 * To change this template use File | Settings | File Templates.
 */

player = (Player) player;
player.clearBits();

for (int i = 1; i < 20; i++) {
    player.SetUInt32Value(PlayerFields."PLAYER_VISIBLE_ITEM_${i}_ENTRYID", 36);

    player.SetUInt32Value(PlayerFields.PLAYER_FIELD_INV_SLOT_HEAD.value + i * 2, 1);
    player.SetUInt32Value(PlayerFields.PLAYER_FIELD_INV_SLOT_HEAD.value + i * 2 + 1, 0x47000000);
}


sender.send(player.getChannel(), new SMSG_UPDATE_OBJECT(player, UpdateType.VALUES));