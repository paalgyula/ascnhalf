import net.ascnhalf.realm.model.UpdateType
import net.ascnhalf.realm.model.base.guid.TypeId
import net.ascnhalf.realm.model.base.update.UpdateFieldUtils
import net.ascnhalf.realm.model.base.update.UpdateFlags
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.model.unit.MovementFlags
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket
import org.apache.log4j.Logger

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.08.20.
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
public class UpdateObject extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(getClass());

    private Player player;
    private BigInteger guid = null;
    private UpdateType updateType = UpdateType.VALUES;

    public UpdateObject() {};

    public UpdateObject(Player player, UpdateType updateType) {
        this.player = player;
        this.updateType = updateType;
        setOpCode(0xa9);
    }

    public void setGuid(BigInteger guid) {
        this.guid = guid;
    }

    @Override
    protected void writeImpl() {
        BitSet bits = player.getBitSet();

        byte[] bytes = bits.toByteArray();
        log.info("Bytes size: " + bytes.length);

        writeD(1); // Commands

        if (updateType == UpdateType.CREATE_SELF) {
            // Dynamic object, corpse, player
            if (true /* it's a player*/)
                updateType = UpdateType.CREATE_SELF;

            writeC(updateType.getValue()); // Update type
            writeB(player.getObjectGuid().pack()); // Packet wow guid (uint64)
            writeC(0x04); // ObjectType

            int updateFlags = player.m_updateFlag;
            player.m_updateFlag |= UpdateFlags.SELF.getValue();

            //buildMovementUpdate( updateFlags );
            writeB([
                    (byte) 0x71, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00,
                    (byte) 0x56, (byte) 0xFC, (byte) 0x00, (byte) 0x00,
                    (byte) 0x66, (byte) 0x06, (byte) 0x35, (byte) 0xC5,
                    (byte) 0x17, (byte) 0xB9, (byte) 0x6F, (byte) 0xC3,
                    (byte) 0x44, (byte) 0x7A, (byte) 0x55, (byte) 0x42,
                    (byte) 0x9A, (byte) 0xB1, (byte) 0x82, (byte) 0x40,
                    (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0xE0, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0x90, (byte) 0x40,
                    (byte) 0x71, (byte) 0x1C, (byte) 0x97, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0x20, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0xE0, (byte) 0x40, (byte) 0x00, (byte) 0x00, (byte) 0x90, (byte) 0x40, (byte) 0xE0, (byte) 0x0F, (byte) 0x49, (byte) 0x40,
                    (byte) 0xC3, (byte) 0xF5, (byte) 0x48, (byte) 0x40, (byte) 0x2F, (byte) 0x00, (byte) 0x00, (byte) 0x00
            ] as byte[]);
        } else {
            writeC(updateType.getValue()); // Update type
            writePackedGuid(player.getCharacterData().getGuid()); // Packet wow guid (uint64)
        }

        writeC(bytes.length); // UpdateValues pakcet size
        writeB(bytes); // UpdateValues bitmask

        for (int i = 0; i < bits.length(); i++) {
            if (!bits.get(i))
                continue;

            log.info("Updating object: " + UpdateFieldUtils.getField(i, player.getObjectGuid()));
            writeD(player.GetUInt32Value(i));
        }

        player.setCreateBits();
        log.info("Update complete. packets cleared.");
    }

    void buildMovementUpdate(int updateFlags) {
        writeH(updateFlags); // movement flags

        if ((updateFlags & UpdateFlags.LIVING.getValue()) > 0) {
            if (player.getObjectGuid().GetTypeId() == TypeId.TYPEID_PLAYER) {
                if (player.getTransport() != null)
                    player.m_movementInfo.flags |= MovementFlags.MOVEFLAG_ONTRANSPORT.getValue();
                else
                //TODO:imlement remove flag
                    player.m_movementInfo.flags |= MovementFlags.MOVEFLAG_ONTRANSPORT.getValue();
            }

            // Updating movement time
            player.m_movementInfo.updateTime();
            //data << flags << unk_230 << getMSTime();
            //data << x << y << z << orientation;
            writeD(player.m_movementInfo.flags);
            writeH(player.m_movementInfo.unk_230);
            writeD(player.m_movementInfo.time);

            writeF(player.m_movementInfo.x);
            writeF(player.m_movementInfo.y);
            writeF(player.m_movementInfo.z);
            writeF(player.m_movementInfo.orientation);

            // TODO: implement extended movement data

            // Unit speeds
            writeF(0.0f /*unit->GetSpeed(MOVE_WALK)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_RUN)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_RUN_BACK)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_SWIM)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_SWIM_BACK)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_FLIGHT)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_FLIGHT_BACK)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_TURN_RATE)*/);
            writeF(0.0f /*unit->GetSpeed(MOVE_PITCH_RATE)*/);

            // 0x08000000
            /*if (unit->m_movementInfo.GetMovementFlags() & MOVEFLAG_SPLINE_ENABLED)
          Movement::PacketBuilder::WriteCreate(*unit->movespline, *data); */
        } else { /* NOT LIVING */
            if ((updateFlags & UpdateFlags.HAS_POSITION.getValue()) > 0) {
                if ((updateFlags & 0x02) > 0) { // Transport
                    writeF(0.0f);
                    writeF(0.0f);
                    writeF(0.0f);
                    writeF(0.0f);
                } else {
                    writeF(player.getCharacterData().getPositionX());
                    writeF(player.getCharacterData().getPositionY());
                    writeF(player.getCharacterData().getPositionZ());
                    writeF(player.getCharacterData().getOrientation());
                }
            }
        }

        if ((updateFlags & UpdateFlags.HIGHGUID.getValue()) > 0) {
            switch (player.getObjectGuid().GetTypeId()) {
                case TYPEID_OBJECT:
                case TYPEID_ITEM:
                case TYPEID_CONTAINER:
                case TYPEID_GAMEOBJECT:
                case TYPEID_DYNAMICOBJECT:
                case TYPEID_CORPSE:
                    writeD(player.getObjectGuid().getHigh().getValue()); // GetGUIDHigh()
                    break;
                case TYPEID_UNIT:
                    writeD(0x0000000B);                // unk, can be 0xB or 0xC
                    break;
                case TYPEID_PLAYER:
                    if ((updateFlags & UpdateFlags.SELF.getValue()) > 0)
                        writeD(0x0000002F);            // unk, can be 0x15 or 0x22
                    else
                        writeD(0x00000008);            // unk, can be 0x7 or 0x8
                    break;
                default:
                    writeD(0x00000000);                // unk
                    break;
            }
        }
    }
}

UpdateObject updateObject = new UpdateObject(player, UpdateType.CREATE_SELF);

sender.send(player.getChannel(), updateObject)