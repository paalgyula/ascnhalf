import net.ascnhalf.realm.model.UpdateType
import net.ascnhalf.realm.model.base.item.Item
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_STANDSTATE_UPDATE
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_UPDATE_OBJECT

/**
 * Recruit's Pants
 */


player = (Player) player;
Item item = new Item(1, 39, player);

sender.send(player.getChannel(), new SMSG_UPDATE_OBJECT(item, UpdateType.CREATE_OBJECT));
sender.send(player.getChannel(), new SMSG_STANDSTATE_UPDATE(1));