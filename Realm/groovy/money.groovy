import net.ascnhalf.realm.model.UpdateType
import net.ascnhalf.realm.model.base.guid.TypeId
import net.ascnhalf.realm.model.base.item.Item
import net.ascnhalf.realm.model.base.update.ObjectFields
import net.ascnhalf.realm.model.base.update.PlayerFields
import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_UPDATE_OBJECT

player = (Player) player;

Item item = new Item(1, 51018, player);
sender.send(player.getChannel(), new SMSG_UPDATE_OBJECT(item, UpdateType.CREATE_OBJECT))

player.clearBits()
println item.getObjectGuid().getRawValue();

player.SetUInt32Value(ObjectFields.OBJECT_FIELD_TYPE, TypeId.TYPEID_PLAYER.getValue());
player.SetUInt32Value(PlayerFields.PLAYER_VISIBLE_ITEM_5_ENTRYID, 51018) // chest
player.SetUInt32Value(PlayerFields.PLAYER_VISIBLE_ITEM_18_ENTRYID, 0x00)

sender.send(player.getChannel(), new SMSG_UPDATE_OBJECT(player, UpdateType.VALUES))