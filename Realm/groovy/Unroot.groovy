import net.ascnhalf.realm.model.player.Player
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket
import org.apache.log4j.Logger

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.08.20.
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
public class UnrootPacket extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(getClass());

    Player player;

    public UnrootPacket(Player player) {
        this.player = player;
        setOpCode(0xea);
    }

    @Override
    protected void writeImpl() {
        writeB(player.getObjectGuid().pack());
        writeD(0x00);
    }
}

sender.send(player.getChannel(), new UnrootPacket(player));