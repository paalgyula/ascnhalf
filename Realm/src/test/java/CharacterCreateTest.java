import net.ascnhalf.realm.model.Classes;
import net.ascnhalf.realm.model.InventoryItem;
import net.ascnhalf.realm.model.Races;
import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.character.CharacterSkills;
import net.ascnhalf.realm.model.player.PlayerHomeBindData;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;


/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.21.
 * Time: 13:18
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/testApplicationContext.xml")
public class CharacterCreateTest {

    @Autowired
    @Qualifier("characterSessionFactory")
    SessionFactory characterSessionFactory;

    @Test
    @Before
    @Transactional
    public void setUp() {
        characterSessionFactory.getCurrentSession();
        characterSessionFactory.getStatistics();
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test1Create() {
        Session session = characterSessionFactory.getCurrentSession();


        CharacterData characterData = new CharacterData();
        characterData.setAccount(1);
        characterData.setGender(1);
        characterData.setRace(Races.BLOODELF);
        characterData.setClazz(Classes.CLASS_PRIEST);
        characterData.setName("JUnit");

        PlayerHomeBindData phbd = new PlayerHomeBindData();
        phbd.setHomeBindAreaId(42);
        phbd.setHomeBindMapId(1);
        phbd.setHomeBindPositionX(512);
        phbd.setHomeBindPositionY(451);
        phbd.setHomeBindPositionZ(463);
        phbd.setCharacterData(characterData);
        characterData.setHomeBindData(phbd);

        session.save(characterData);

        // Adding inventory
        InventoryItem ii = new InventoryItem();
        ii.setBag_guid(0);
        ii.setItem_id(64);
        ii.setSlot(0);
        ii.setCharacterData(characterData);

        session.save(ii);

        CharacterSkills skill = new CharacterSkills();
        skill.setSkill(89);
        skill.setMax(350);
        skill.setValue(300);
        skill.setCharacterData(characterData);
        session.save(skill);


        System.out.println("Character guid: " + characterData.getGuid());
        System.out.println("InventoryItem owner: " + ii.getCharacterData().getName());
        System.out.println("PlayerHomeBindData owner: " + phbd.getGuid());
    }

    @Test
    @Transactional
    public void test2Find() {
        Session session = characterSessionFactory.getCurrentSession();
        Query query = session.createQuery("from CharacterData cd where cd.name = :name").setString("name", "JUnit");
        CharacterData characterData = (CharacterData) query.uniqueResult();

        // Character data
        assertNotNull(characterData);
    }

    @Test
    @Transactional
    @Rollback(false)
    public void test3Delete() {
        Session session = characterSessionFactory.getCurrentSession();
        Query query = session.createQuery("select cd from CharacterData cd where name = :name").setString("name", "JUnit");
        CharacterData cd = (CharacterData) query.uniqueResult();
        assertNotNull(cd);

        session.delete(cd);
    }

}
