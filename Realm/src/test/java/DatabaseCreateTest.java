import org.hibernate.dialect.HSQLDialect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with IntelliJ IDEA.
 * User: Felhasználó
 * Date: 2013.06.03.
 * Time: 18:20
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/testApplicationContext.xml")
public class DatabaseCreateTest {

    @Autowired
    @Qualifier("worldSessionFactory")
    LocalSessionFactoryBean worldSessionFactory;

    @Test
    public void update() throws Exception {
        String[] s = worldSessionFactory.getConfiguration().generateSchemaCreationScript(new HSQLDialect());

        for (String sql : s)
            System.err.println(sql);
    }
}
