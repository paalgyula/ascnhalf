/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import net.ascnhalf.commons.model.Account;
import net.ascnhalf.commons.network.model.NettyNetworkChannel;
import net.ascnhalf.realm.dao.AccountDAO;
import net.ascnhalf.realm.model.base.account.AccountData;
import net.ascnhalf.realm.model.base.character.CharacterData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class is responsible for controlling all account actions.
 *
 * @author MinimaJack
 */
@Service
@Scope("singleton")
public class AccountService {

    /**
     * The account dao.
     */
    @Autowired
    private AccountDAO accountDAO;

    /**
     * The item storages.
     */
    @Autowired
    private ItemStorages itemStorages;

    /**
     * Creates the and attach account.
     *
     * @param name           the name
     * @param channelHandler the channel handler
     * @return the account
     */
    public Account createAndAttachAccount(String name, NettyNetworkChannel channelHandler) {
        Account account = getAccountFromDB(name);
        channelHandler.setChanneledObject(account);
        return account;
    }

    /**
     * Loads account from DB and returns it, or returns null if account was not
     * loaded.
     *
     * @param accountName Account name
     * @return loaded account or null
     */
    public Account getAccountFromDB(String accountName) {
        return accountDAO.getAccount(accountName);
    }

    /**
     * Save session key.
     *
     * @param accountName the account name
     * @param Key         the key
     */
    public void saveSessionKey(String accountName, String Key) {
        accountDAO.saveSessionKey(accountName, Key);
    }

    /**
     * Gets the account data.
     *
     * @param id the id
     * @return the account data
     */
    public List<AccountData> getAccountData(int id) {
        return accountDAO.getAccountData(id);
    }

    /**
     * Gets the session key from db.
     *
     * @param username the username
     * @return the session key from db
     *         Session Key from DB
     */
    public String getSessionKeyFromDB(String username) {
        return accountDAO.getSessionKey(username);
    }

    /**
     * Load tutorials data from db.
     *
     * @param guid - Account
     * @return the int[]
     *         all tutorial Data saved in DB
     */
    public int[] loadTutorialsDataFromDB(int guid) {
        return accountDAO.getTutorialsData(guid);
    }

    /**
     * Search account's characters
     *
     * @param accountId account to query
     */
    public List<CharacterData> getCharacters(int accountId) {
        return accountDAO.getCharacters(accountId);
    }
}
