/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.procedure.TObjectProcedure;
import net.ascnhalf.commons.network.model.SendablePacket;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.commons.service.Service;
import net.ascnhalf.realm.model.base.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;

/**
 * The Class MapService.
 */
@org.springframework.stereotype.Service
@Scope("singleton")
public class MapService implements Service {

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    /**
     * The maps.
     */
    TIntObjectHashMap<Map> maps = new TIntObjectHashMap<Map>();

    /**
     * The map updater.
     */
    private MapUpdater mapUpdater = new MapUpdater();

    private Map createMap(int mapId) {
        if (maps.containsKey(mapId))
            return maps.get(mapId);

        Map map = new Map(mapId);
        maps.put(mapId, map);
        return map;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {

    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        maps.clear();

    }

    /**
     * Gets the map.
     *
     * @param guid the guid
     * @return the map
     */
    public Map getMap(int guid) {
        return createMap(guid);
    }

    /**
     * Update.
     */
    public void update() {
        maps.forEachValue(mapUpdater);
    }

    /**
     * The Class MapUpdater.
     */
    class MapUpdater implements TObjectProcedure<Map> {

        /* (non-Javadoc)
         * @see gnu.trove.TObjectProcedure#execute(java.lang.Object)
         */
        @Override
        public boolean execute(Map map) {
            return map.update();
        }
    }

    public void sendToAll(SendablePacket packet) {
        for (Map map : maps.valueCollection())
            map.sendToSet(sender, packet);
    }
}
