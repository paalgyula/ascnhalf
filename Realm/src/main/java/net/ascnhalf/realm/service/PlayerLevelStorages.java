/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import net.ascnhalf.commons.dataholder.DataLoadService;
import net.ascnhalf.realm.dao.SimpleDataDAO;
import net.ascnhalf.realm.model.Classes;
import net.ascnhalf.realm.model.Races;
import net.ascnhalf.realm.model.base.PlayerLevelInfo;
import net.ascnhalf.realm.model.base.PlayerLevelInfoPK;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * The Class PlayerLevelStorages.
 */
@Service
@Scope("singleton")
public class PlayerLevelStorages implements
        DataLoadService<HashMap<PlayerLevelInfoPK, PlayerLevelInfo>> {

    /**
     * The Constant log.
     */
    private static final Logger log = Logger
            .getLogger(PlayerLevelStorages.class);

    /**
     * The simple data dao.
     */
    @Autowired
    SimpleDataDAO simpleDataDAO;

    /**
     * The Player Race Class Level info.
     */
    private HashMap<PlayerLevelInfoPK, PlayerLevelInfo> playerRCLI = new HashMap<PlayerLevelInfoPK, PlayerLevelInfo>();

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {
        load();
        log.info("Loaded " + playerRCLI.size() + " PlayerLevelInfos");
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.dataholder.DataLoadService#load()
     */
    @Override
    public HashMap<PlayerLevelInfoPK, PlayerLevelInfo> load() {
        // TODO Auto-generated method stub
        return playerRCLI = simpleDataDAO.getRaceClassLevelInfos();
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.dataholder.DataLoadService#reload()
     */
    @Override
    public void reload() {
        HashMap<PlayerLevelInfoPK, PlayerLevelInfo> playerRCLITemp = simpleDataDAO.getRaceClassLevelInfos();
        playerRCLI = playerRCLITemp;
        playerRCLITemp = null;
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.dataholder.DataLoadService#save()
     */
    @Override
    public void save() {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.dataholder.DataLoadService#get()
     */
    @Override
    public HashMap<PlayerLevelInfoPK, PlayerLevelInfo> get() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer getSize() {
        return playerRCLI.size();
    }

    /**
     * Gets the.
     *
     * @param race  the race
     * @param clazz the clazz
     * @param level the level
     * @return the player level info
     */
    public PlayerLevelInfo get(int race, int clazz, int level) {
        PlayerLevelInfoPK cl = new PlayerLevelInfoPK(race, clazz, level);
        if (playerRCLI.containsKey(cl)) {
            return playerRCLI.get(cl);
        } else {
            log.warn("Can't find proper PlayerLevelInfo PLI size: " + playerRCLI.size());
            return null;
        }
    }

    /**
     * Gets the.
     *
     * @param race  the race
     * @param clazz the clazz
     * @param level the level
     * @return the player level info
     */
    public PlayerLevelInfo get(Races race, Classes clazz, int level) {
        return get(race.getValue(), clazz.getValue(), level);
    }


    @Override
    public String toString() {
        return "PlayerLevelStorages [ " + getSize() + " items ]";
    }
}
