package net.ascnhalf.realm.service;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.commons.service.Service;
import net.ascnhalf.realm.model.player.chat.ChatChannel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 19:45
 */
@org.springframework.stereotype.Service
@Scope("singleton")
public class ChatService implements Service {
    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    private final Logger log = Logger.getLogger(getClass());

    private Map<String, ChatChannel> channelList = new HashMap<String, ChatChannel>();

    public ChatChannel getChannel(String channelName) {
        ChatChannel cc = channelList.get(channelName);

        if (cc == null)
            cc = createChannel(channelName);

        return cc;
    }

    private ChatChannel createChannel(String name) {
        ChatChannel chan = new ChatChannel(sender, name);
        channelList.put(name, chan);
        return chan;
    }

    public Map getChannels() {
        return channelList;
    }

    @Override
    public void stop() {

    }

    @Override
    public void start() {
        log.info("ChatService started!");
    }
}
