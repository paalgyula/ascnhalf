/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.commons.dataholder.DataLoadService;
import net.ascnhalf.realm.dao.SimpleDataDAO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class SimpleStorages.
 */
@Service
@Scope("singleton")
public class SimpleStorages implements DataLoadService<TIntObjectHashMap<Integer>> {

    public TIntObjectHashMap<Integer> playerXpForLevelMap;

    @Autowired
    SimpleDataDAO simpleDataDAO;

    /**
     * Gets the xp for level.
     *
     * @param level the level
     * @return the int
     */
    public int GetXPForLevel(byte level) {
        return playerXpForLevelMap.get(level);
    }


    @Override
    public TIntObjectHashMap<Integer> load() {
        playerXpForLevelMap = simpleDataDAO.getPlayerXPForLevel();
        return playerXpForLevelMap;
    }

    @Override
    public void reload() {
        playerXpForLevelMap.clear();
        load();
    }

    @Override
    public void save() {
        // Readonly!
        return;
    }

    @Override
    public TIntObjectHashMap<Integer> get() {
        return playerXpForLevelMap;
    }

    @Override
    public Integer getSize() {
        return playerXpForLevelMap.size();
    }

    @Override
    public void start() {
        load();
        Logger.getLogger(getClass()).info(String.format("Loaded %d Level-XP entry.", playerXpForLevelMap.size()));
    }

    @Override
    public void stop() {
        save();
    }
}
