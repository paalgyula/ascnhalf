/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.commons.dataholder.DataLoadService;
import net.ascnhalf.realm.dao.QuestDAO;
import net.ascnhalf.realm.model.base.QuestPrototype;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class QuestStorages.
 */
@Service
@Scope("singleton")
public class QuestStorages implements DataLoadService<TIntObjectHashMap<QuestPrototype>> {

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger.getLogger(QuestStorages.class);

    /**
     * The player class level infos.
     */
    private TIntObjectHashMap<QuestPrototype> questMap = new TIntObjectHashMap<QuestPrototype>();

    /**
     * The quest dao.
     */
    @Autowired
    private QuestDAO questDAO;

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {
        load();
        logger.info("Loaded " + questMap.size() + " questPrototypes");
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        questMap.clear();
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#load()
     */
    @Override
    public TIntObjectHashMap<QuestPrototype> load() {
        questMap = questDAO.loadQuestPrototypes();
        return questMap;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#reload()
     */
    public void reload() {
        // Don't replace directly becouse the players can't query quest while it's loading!
        logger.info("Loading quest templates to temoary store.");
        TIntObjectHashMap<QuestPrototype> tempQuestMap = questDAO.loadQuestPrototypes();
        logger.info("Loaded " + tempQuestMap.size() + " quests. Replacing new old Quests with newer");
        questMap = tempQuestMap;
        tempQuestMap = null;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#save()
     */
    @Override
    public void save() {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#get()
     */
    @Override
    public TIntObjectHashMap<QuestPrototype> get() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer getSize() {
        return questMap.size();
    }


    // Specific getters
    public QuestPrototype getQuest(int questId) {
        return questMap.get(questId);
    }

    @Override
    public String toString() {
        return "QuestStorages [ " + getSize() + " items ]";
    }
}
