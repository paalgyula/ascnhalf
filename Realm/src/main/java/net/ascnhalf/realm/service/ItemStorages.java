/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.commons.dataholder.DataLoadService;
import net.ascnhalf.realm.dao.ItemDAO;
import net.ascnhalf.realm.model.base.item.ItemPrototype;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class ItemStorages.
 */
@Service
@Scope("singleton")
public class ItemStorages
        implements
        DataLoadService<TIntObjectHashMap<ItemPrototype>> {

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger.getLogger(ItemStorages.class);

    /**
     * The item dao.
     */
    @Autowired
    private ItemDAO itemDAO;

    /**
     * The item prototypes.
     */
    private TIntObjectHashMap<ItemPrototype> itemPrototypes = new TIntObjectHashMap<ItemPrototype>();

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.dataholder.DataLoadService#get()
     */
    @Override
    public TIntObjectHashMap<ItemPrototype> get() {
        return itemPrototypes;
    }

    @Override
    public Integer getSize() {
        return itemPrototypes.size();
    }

    /**
     * Gets the.
     *
     * @param guid the guid
     * @return the item prototype
     */
    public ItemPrototype get(int guid) {
        if (itemPrototypes.containsKey(guid)) {
            logger.debug(String.format("The specified id: %d hit in the storage! DisplayInfoID: %d", guid, itemPrototypes.get(guid).getDisplayInfoID()));
            return itemPrototypes.get(guid);
        } else {
            logger.debug(String.format("The specified id: %d not in the store. Trying to load directly from the database!", guid));
            ItemPrototype ip = itemDAO.loadItemPrototype(guid);
            if (ip != null) {
                itemPrototypes.put(ip.getObjectId(), ip);
                return ip;
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#load()
     */
    @Override
    public TIntObjectHashMap<ItemPrototype> load() {
        itemPrototypes = itemDAO.loadItemPrototypes();
        return itemPrototypes;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#reload()
     */
    @Override
    public void reload() {
        TIntObjectHashMap<ItemPrototype> itemProrotypesTemp = itemDAO.loadItemPrototypes();
        itemPrototypes = itemProrotypesTemp;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.dataholder.DataLoadService#save()
     */
    @Override
    public void save() {
        // TODO Save in xml?

    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {
        //load();
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        itemPrototypes.clear();
    }

}
