/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import net.ascnhalf.commons.config.Compatiple;
import net.ascnhalf.commons.network.handlers.PacketHandlerFactory;
import net.ascnhalf.commons.network.netty.service.AbstractNetworkService;
import net.ascnhalf.realm.config.Config;
import org.apache.commons.lang.NotImplementedException;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * The Class RealmNetworkService.
 */
@Component("realmNetworkService")
public class RealmNetworkService extends AbstractNetworkService {

    @Autowired
    private Config config;

    /**
     * The RealmToClient pipeline factory.
     */
    @Autowired
    @Qualifier("RealmToClient")
    private ChannelPipelineFactory realmToClientPipelineFactory;

    /**
     * The packet service.
     */
    @Autowired
    @Qualifier("AuthToClient")
    private PacketHandlerFactory clientPacketService;
    /**
     * The packet service.
     */

    @Autowired
    @Qualifier("RealmToAuth")
    private PacketHandlerFactory authPacketService;

    @Autowired
    @Qualifier("RealmToAuth")
    private ChannelPipelineFactory realmToAuthPipelineFactory;

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.service.Service#start()
     */
    @Override
    public void start() {
        clientPacketService.loadPacket();
        createServerChannel(config.CLIENT_ADDRESS, realmToClientPipelineFactory);
        // Only run if auth server not from mangos team
        if (!config.COMPATIBLE.equals(Compatiple.MANGOS)) {
            authPacketService.loadPacket();
            createClientChannel(config.AUTH_ADDRESS, realmToAuthPipelineFactory);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.netty.service.NetworkService#status()
     */
    @Override
    public void status() {
        throw new NotImplementedException();
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.service.Service#stop()
     */
    @Override
    public void stop() {
        throw new NotImplementedException();
    }
}
