/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.service;

import gnu.trove.map.hash.TLongObjectHashMap;
import groovy.ui.Console;
import net.ascnhalf.commons.network.model.NettyNetworkChannel;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.model.InventoryItem;
import net.ascnhalf.realm.model.UpdateType;
import net.ascnhalf.realm.model.base.PlayerClassLevelInfo;
import net.ascnhalf.realm.model.base.PlayerLevelInfo;
import net.ascnhalf.realm.model.base.WorldObject;
import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.character.CharacterRaces;
import net.ascnhalf.realm.model.base.character.CharacterSkills;
import net.ascnhalf.realm.model.base.item.ItemPrototype;
import net.ascnhalf.realm.model.base.update.ObjectFields;
import net.ascnhalf.realm.model.base.update.PlayerFields;
import net.ascnhalf.realm.model.base.update.UnitField;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.model.unit.Powers;
import net.ascnhalf.realm.model.unit.SpellSchools;
import net.ascnhalf.realm.model.unit.Stats;
import net.ascnhalf.realm.network.netty.packetClient.server.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The Class PlayerService.
 */
@Service
@Scope("singleton")
public class PlayerService {

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger.getLogger(PlayerService.class);

    /**
     * The Constant PLAYER_EXPLORED_ZONES_SIZE.
     */
    private static final int PLAYER_EXPLORED_ZONES_SIZE = 128;

    /**
     * The Constant KNOWN_TITLES_SIZE.
     */
    private static final int KNOWN_TITLES_SIZE = 3;

    /**
     * The Constant CONFIG_UINT32_MAX_PLAYER_LEVEL.
     */
    private static final int CONFIG_UINT32_MAX_PLAYER_LEVEL = 80;

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /**
     * The player dao.
     */
    @Autowired
    private PlayerDAO playerDAO;

    /**
     * The player class level info storages.
     */
    @Autowired
    private PlayerClassLevelInfoStorages playerClassLevelInfoStorages;

    /**
     * The player level storages.
     */
    @Autowired
    private PlayerLevelStorages playerLevelStorages;

    /**
     * The simple storages.
     */
    @Autowired
    private SimpleStorages simpleStorages;

    /**
     * The item storages.
     */
    @Autowired
    private ItemStorages itemStorages;

    /**
     * The map service
     */
    @Autowired
    private MapService mapService;

    /**
     * The playerlist.
     */
    private static TLongObjectHashMap<Player> playerlist = new TLongObjectHashMap<Player>();

    /**
     * Prepare player.
     *
     * @param chanel the chanel
     * @param guid   the guid
     * @return the player
     */
    public Player preparePlayer(NettyNetworkChannel chanel, long guid) {
        Player player = new Player(playerDAO.getCharacter(guid));
        player.setSender(sender);
        player.setChannel(chanel);
        chanel.setActiveObject(player);
        return player;
    }

    /**
     * Send inicial packets.
     *
     * @param player the player
     * @return the world object
     */
    public WorldObject sendInitialPackets(Player player) {
        sender.send(player.getChannel(), new SMSG_POWER_UPDATE(player, Powers.POWER_ENERGY, 100));

        sender.send(player.getChannel(), new MSG_SET_DUNGEON_DIFFICULTY());
        sender.send(player.getChannel(), new SMSG_INSTANCE_DIFFICULTY());

        sender.send(player.getChannel(), new SMSG_LOGIN_VERIFY_WORLD(player));
        sender.send(player.getChannel(), new SMSG_FEATURE_SYSTEM_STATUS());
        sender.send(player.getChannel(), new SMSG_LEARNED_DANCE_MOVES());
        sender.send(player.getChannel(), new SMSG_BINDPOINTUPDATE(player));
        sender.send(player.getChannel(), new SMSG_TALENTS_INFO());
        sender.send(player.getChannel(), new SMSG_INITIAL_SPELLS());
        sender.send(player.getChannel(), new SMSG_SEND_UNLEARN_SPELLS());
        sender.send(player.getChannel(), new SMSG_ACTION_BUTTONS());
        sender.send(player.getChannel(), new SMSG_INITIALIZE_FACTIONS());
        sender.send(player.getChannel(), new SMSG_ACCOUNT_DATA_TIMES(SMSG_ACCOUNT_DATA_TIMES.PER_CHARACTER_CACHE_MASK, player.getCharacterData().getAccountDataList()));

        sender.send(player.getChannel(), new SMSG_EQUIPMENT_SET_LIST());
        sender.send(player.getChannel(), new SMSG_ALL_ACHIEVEMENT_DATA());
        sender.send(player.getChannel(), new SMSG_LOGIN_SETTIMESPEED());
        sender.send(player.getChannel(), new SMSG_INIT_WORLD_STATES(player));
        sender.send(player.getChannel(), new SMSG_PLAYED_TIME());

        player.SetUInt32Value(PlayerFields.PLAYER_FIELD_MAX_LEVEL, 80); // PLAYER_FIELD_MAX_LEVEL

        // Battle Stance
        sender.send(player.getChannel(), new SMSG_AURA_UPDATE(0, 0x999, 0x1b, 1, 1));

        // Revenge trigger
        sender.send(player.getChannel(), new SMSG_AURA_UPDATE(1, 0x14b5, 0x19, 1, 1));

        // Heroic presence
        sender.send(player.getChannel(), new SMSG_AURA_UPDATE(1, 0x19a2, 0x1b, 1, 1));


        sender.send(player.getChannel(), new SMSG_AURA_UPDATE_ALL());

        player.SetUInt32Value(ObjectFields.OBJECT_FIELD_TYPE, player.getTypeMask()); // OBJECT_FIELD_TYPE
        player.SetFloatValue(ObjectFields.OBJECT_FIELD_SCALE_X, 1.0f); // OBJECT_FIELD_SCALE_X
        player.SetUInt32Value(0x0037, 0x00000006); // UNIT_FIELD_FACTIONTEMPLATE
        player.SetUInt32Value(0x003B, 0x00000008); // UNIT_FIELD_FLAGS
        player.SetUInt32Value(0x003C, 0x00000800); // UNIT_FIELD_FLAGS_2
        player.SetUInt32Value(0x0046, 0x41590751); // UNIT_FIELD_MINDAMAGE
        player.SetUInt32Value(0x0047, 0x41790751); // UNIT_FIELD_MAXDAMAGE
        player.SetUInt32Value(0x0054, 0x0000001f); // UNIT_FIELD_STAT0
        player.SetUInt32Value(0x0055, 0x00000011); // UNIT_FIELD_STAT1
        player.SetUInt32Value(0x0056, 0x0000001a); // UNIT_FIELD_STAT2
        player.SetUInt32Value(0x0057, 0x0000000f); // UNIT_FIELD_STAT3
        player.SetUInt32Value(0x0058, 0x00000017); // UNIT_FIELD_STAT4
        player.SetUInt32Value(0x0063, 0x000000ba); // UNIT_FIELD_RESISTANCES
        player.SetUInt32Value(0x007A, 0x11000000); // UNIT_FIELD_BYTES_2
        player.SetUInt32Value(0x007B, 0x00000033); // UNIT_FIELD_ATTACK_POWER
        player.SetUInt32Value(0x007E, 0x0000000a); // UNIT_FIELD_RANGED_ATTACK_POWER
        player.SetUInt32Value(0x0081, 0x401b6db7); // UNIT_FIELD_MINRANGEDDAMAGE
        player.SetUInt32Value(0x0082, 0x405b6db7); // UNIT_FIELD_MAXRANGEDDAMAGE

        for (int i = 0; i < 19; ++i) {
            int offset = i * 2;

            InventoryItem ii = player.getCharacterData().getItemFromSlot(i);
            if (ii != null) {
                player.SetUInt32Value(PlayerFields.PLAYER_VISIBLE_ITEM_1_ENTRYID.getValue() + offset, ii.getItem_guid());
                player.SetUInt32Value(PlayerFields.PLAYER_VISIBLE_ITEM_1_ENCHANTMENT.getValue() + offset, 0x00);
            }
        }

        // Common language
        //player.SetUInt32Value( 0x029E, 98 ); // PLAYER_SKILL_INFO_1_1
        //player.SetUInt32Value( 0x02A0, 450 << 16 | 450 ); // PLAYER_SKILL_INFO_1_1
        //player.SetUInt32Value( 0x02A1, 0x00 ); // PLAYER_SKILL_INFO_1_1

        player.SetUInt32Value(0x03FD, 0x00000002); // PLAYER_CHARACTER_POINTS2
        player.SetUInt32Value(0x0400, 0x409ae148); // PLAYER_BLOCK_PERCENTAGE
        player.SetUInt32Value(0x0401, 0x409a5b96); // PLAYER_DODGE_PERCENTAGE
        player.SetUInt32Value(0x0405, 0x40dd5f6f); // PLAYER_CRIT_PERCENTAGE
        player.SetUInt32Value(0x0406, 0x40ce0346); // PLAYER_RANGED_CRIT_PERCENTAGE
        player.SetUInt32Value(0x0407, 0x40ce0346); // PLAYER_OFFHAND_CRIT_PERCENTAGE
        player.SetUInt32Value(0x040F, 0x00000005); // PLAYER_SHIELD_BLOCK
        player.SetUInt32Value(0x0491, 0x000002d7); // PLAYER_REST_STATE_EXPERIENCE
        player.SetUInt32Value(0x04A1, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A2, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A3, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A4, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A5, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A6, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT
        player.SetUInt32Value(0x04A7, 0x3f800000); // PLAYER_FIELD_MOD_DAMAGE_DONE_PCT

        player.SetUInt32Value(0x0520, 0x00000015); // PLAYER_FIELD_GLYPH_SLOTS_1
        player.SetUInt32Value(0x0521, 0x00000016); // PLAYER_FIELD_GLYPH_SLOTS_1
        player.SetUInt32Value(0x0522, 0x00000017); // PLAYER_FIELD_GLYPH_SLOTS_1
        player.SetUInt32Value(0x0523, 0x00000018); // PLAYER_FIELD_GLYPH_SLOTS_1
        player.SetUInt32Value(0x0524, 0x00000019); // PLAYER_FIELD_GLYPH_SLOTS_1
        player.SetUInt32Value(0x0525, 0x0000001a); // PLAYER_FIELD_GLYPH_SLOTS_1

        sender.send(player.getChannel(), new SMSG_UPDATE_OBJECT(player, UpdateType.CREATE_SELF));

        sender.send(player.getChannel(), new SMSG_MOTD("AscNHalf Development Server".split("@")));
        sender.send(player.getChannel(), new SMSG_SPELL_GO());
        sender.send(player.getChannel(), new SMSG_TIME_SYNC_REQ());

        mapService.getMap(player.getCharacterData().getMap()).AddObject(player);
        PlayerService.playerlist.put(player.getObjectId(), player);

        /**
         * Groovy console pass parameters
         */
        Console console = new Console();
        console.setVariable("sender", sender);
        console.setVariable("player", player);
        //console.run();

        return player;
    }

    public void savePlayer(Player player) {
        playerDAO.saveCharacterData(player.collectCharacterData());
    }

    /**
     * Load home bind.
     *
     * @param player the player
     * @return true, if successful
     */
    public boolean LoadHomeBind(Player player) {
        return player.setHomeBind(playerDAO.loadHomeBind(player.getObjectId()));
    }

    /**
     * Load from db.
     *
     * @param player the player
     * @return true, if successful
     */
    public boolean LoadFromDB(Player player) {
        player.initfields();
        player.SetUInt64Value(ObjectFields.OBJECT_FIELD_GUID, player.getObjectGuid().getRawValue());

        player.SetFloatValue(ObjectFields.OBJECT_FIELD_SCALE_X, 1.0f);

        CharacterData ch = player.getCharacterData();
        int bytes0 = 0;
        bytes0 |= ch.getRace().getValue();
        bytes0 |= ch.getClazz().getValue() << 8;
        bytes0 |= ch.getGender() << 16;

        player.SetUInt32Value(UnitField.UNIT_FIELD_BYTES_0, bytes0);
        player.SetUInt32Value(UnitField.UNIT_FIELD_LEVEL, ch.getLevel());
        player.SetUInt32Value(PlayerFields.PLAYER_XP, ch.getXp());
        player._LoadIntoDataField(ch.getExploredZones().split(" "), PlayerFields.PLAYER_EXPLORED_ZONES_1.getValue(), PLAYER_EXPLORED_ZONES_SIZE);
        player._LoadIntoDataField(ch.getKnownTitles().split(" "), PlayerFields.PLAYER__FIELD_KNOWN_TITLES.getValue(), KNOWN_TITLES_SIZE * 2);

        player.SetFloatValue(UnitField.UNIT_FIELD_BOUNDINGRADIUS, 0.388999998569489f);
        player.SetFloatValue(UnitField.UNIT_FIELD_COMBATREACH, 1.5f);
        player.SetFloatValue(UnitField.UNIT_FIELD_HOVERHEIGHT, 1.0f);
        player.setMoney(ch.getMoney());

        player.SetUInt32Value(PlayerFields.PLAYER_BYTES, ch.getPlayerBytes());
        player.SetUInt32Value(PlayerFields.PLAYER_BYTES_2, ch.getPlayerBytes2());
        player.SetUInt32Value(PlayerFields.PLAYER_BYTES_3, (ch.getDrunk() & 0xFFFE) | ch.getGender());
        player.SetUInt32Value(PlayerFields.PLAYER_FLAGS, ch.getPlayerFlags());
        player.SetUInt32Value(PlayerFields.PLAYER_FIELD_WATCHED_FACTION_INDEX, (int) ch.getWatchedFaction());

        player.SetUInt64Value(PlayerFields.PLAYER_FIELD_KNOWN_CURRENCIES, ch.getKnownCurrencies());

        player.SetUInt32Value(PlayerFields.PLAYER_AMMO_ID, ch.getAmmoId());
        player.SetByteValue(PlayerFields.PLAYER_FIELD_BYTES, 2, (byte) ch.getActionBars());

        CharacterRaces raceEntry = playerDAO.getRaceEntry(player.getCharacterData().getRace());

        int modelId;
        if (player.getCharacterData().getGender() == 0)
            modelId = raceEntry.getMaleDisplayId();
        else
            modelId = raceEntry.getFemaleDisplayId();

        player.SetUInt32Value(UnitField.UNIT_FIELD_DISPLAYID, modelId);
        player.SetUInt32Value(UnitField.UNIT_FIELD_NATIVEDISPLAYID, modelId);

        LoadSkills(player);

        InitStatsForLevel(player);
        LoadInventory(player);
        return false;
    }

    public void LoadSkills(Player player) {
        int skillCount = 0;
        for (CharacterSkills skill : player.getCharacterData().getSkills()) {
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, skill.getSkill());
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, skill.getMax() << 16 | skill.getValue());
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, 0x00); // TODO: bonus research
        }

        // Clear other skills
        for (; skillCount < 127; skillCount++) {
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, 0x00);
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, 0x00);
            player.SetUInt32Value(PlayerFields.PLAYER_SKILL_INFO_1_1.getValue() + skillCount++, 0x00);
        }
    }

    /**
     * Load inventory.
     *
     * @param player the player
     */
    public void LoadInventory(Player player) {
        List<InventoryItem> inventoryItem = playerDAO
                .loadInventory(player.getObjectId());
        for (InventoryItem iT : inventoryItem) {
            ItemPrototype proto = itemStorages.get(iT.getItem_id());
            if (proto != null) {
                logger.debug(proto.getName() + " - " + iT.getSlot());
                /*
                Item item = itemStorages.loadFromDB(iT,proto);
				if( Item.IsInventoryPos( Item.INVENTORY_SLOT_BAG_0, iT.getSlot() )){
					logger.debug(proto.getName() + " - " +"in INVENTORY_SLOT_BAG_0");
				}
				if( Item.IsEquipmentPos( Item.INVENTORY_SLOT_BAG_0, iT.getSlot() )){
					logger.debug(proto.getName() + " - " +"in INVENTORY_SLOT_BAG_0 and can equip");
					player.equipItem(iT.getSlot(), item);
				}*/
            }
        }


    }

    /**
     * Inits the stats for level.
     *
     * @param player the player
     */
    public void InitStatsForLevel(Player player) {
        PlayerClassLevelInfo classInfo = playerClassLevelInfoStorages.get(
                player.getCharacterData().getClazz(), player.getCharacterData()
                .getLevel());
        PlayerLevelInfo info = playerLevelStorages.get(player
                .getCharacterData().getRace(), player.getCharacterData()
                .getClazz(), player.getCharacterData().getLevel());

        player.SetUInt32Value(PlayerFields.PLAYER_FIELD_MAX_LEVEL,
                CONFIG_UINT32_MAX_PLAYER_LEVEL);
        player.SetUInt32Value(PlayerFields.PLAYER_NEXT_LEVEL_XP, simpleStorages
                .GetXPForLevel((byte) player.getCharacterData().getLevel()));

        // UpdateSkillsForLevel();

        player.SetFloatValue(UnitField.UNIT_MOD_CAST_SPEED, 1.0f);

        for (Stats stat : Stats.values()) {
            try {
                int val = info.getStats(stat);
                player.setCreateStat(stat, val);
                player.SetUInt32Value(UnitField.UNIT_FIELD_STAT0.getValue() + stat.ordinal(), val);
            } catch (Exception e) {
                logger.info("Error while loading stats...");
            }
        }

        try {
            player.SetUInt32Value(UnitField.UNIT_FIELD_BASE_HEALTH, classInfo
                    .getBasehealth());
            player.SetUInt32Value(UnitField.UNIT_FIELD_BASE_MANA, classInfo
                    .getBasemana());
            player.SetArmor(info.getStats(Stats.AGILITY) * 2);
        } catch (Exception e) {
            logger.info("Error while loading base units (hp, mana, armor )");
        }

        // InitStatBuffMods();

        for (SpellSchools spellscool : SpellSchools.values())
            player.SetFloatValue(PlayerFields.PLAYER_FIELD_MOD_DAMAGE_DONE_PCT.getValue() + spellscool.ordinal(), 1.00f);

        player.SetFloatValue(UnitField.UNIT_FIELD_BASEATTACKTIME, 2000.0f);
        player.SetFloatValue(UnitField.UNIT_FIELD_BASEATTACKTIME.getValue() + 1, 2000.0f);
        // offhand attack time
        player.SetFloatValue(UnitField.UNIT_FIELD_RANGEDATTACKTIME, 2000.0f);

        // set armor (resistance 0) to original value (create_agility*2)

        // InitDataForForm();

        // save new stats
        for (Powers power : Powers.values())
            player.SetMaxPower(power, player.GetCreatePowers(power));

        player.SetMaxHealth(classInfo.getBasehealth());

        // stamina bonus will applied later

        // player.SetFlag(PlayerFields.UNIT_FIELD_FLAGS,
        // UnitFlags.UNIT_FLAG_PVP_ATTACKABLE.getValue() );
        // must be set
        /*
         * SetFlag(UNIT_FIELD_FLAGS_2,UNIT_FLAG2_REGENERATE_POWER); // must be
		 * set
		 * 
		 * // cleanup player flags (will be re-applied if need at aura load), to
		 * avoid have ghost flag without ghost aura, for example.
		 * RemoveFlag(PLAYER_FLAGS, PLAYER_FLAGS_AFK | PLAYER_FLAGS_DND |
		 * PLAYER_FLAGS_GM | PLAYER_FLAGS_GHOST);
		 * 
		 * RemoveStandFlags(UNIT_STAND_FLAGS_ALL); // one form stealth modified
		 * bytes RemoveByteFlag(UNIT_FIELD_BYTES_2, 1, UNIT_BYTE2_FLAG_FFA_PVP |
		 * UNIT_BYTE2_FLAG_SANCTUARY);
		 * 
		 * // restore if need some important flags
		 * SetUInt32Value(PLAYER_FIELD_BYTES2, 0 ); // flags empty by default
		 * 
		 * if(reapplyMods) //reapply stats values only on .reset stats (level)
		 * command _ApplyAllStatBonuses();
		 * 
		 * // set current level health and mana/energy to maximum after applying
		 * all mods.
		 */
        player.SetHealth(player.GetMaxHealth());
        player.SetPower(Powers.POWER_MANA, player.GetMaxPower(Powers.POWER_MANA));
        player.SetPower(Powers.POWER_ENERGY, player.GetMaxPower(Powers.POWER_ENERGY));
        if (player.GetPower(Powers.POWER_RAGE) > player.GetMaxPower(Powers.POWER_RAGE))
            player.SetPower(Powers.POWER_RAGE, player.GetMaxPower(Powers.POWER_RAGE));

    }

    /**
     * Gets the playerlist.
     *
     * @return the playerlist
     */
    public static TLongObjectHashMap<Player> getPlayerlist() {
        return playerlist;
    }

    /**
     * Sets the playerlist.
     *
     * @param playerlist the new playerlist
     */
    public static void setPlayerlist(TLongObjectHashMap<Player> playerlist) {
        PlayerService.playerlist = playerlist;
    }

    /**
     * Gets the player name.
     *
     * @param guid the guid
     * @return the player name
     */
    public static String getPlayerName(long guid) {
        if (PlayerService.playerlist.contains(guid)) {
            return PlayerService.playerlist.get(guid).getName();
        }
        return "";
    }

    /**
     * Gets the player.
     *
     * @param guid the guid
     * @return the player
     */
    public static Player getPlayer(long guid) {
        if (PlayerService.playerlist.contains(guid)) {
            return PlayerService.playerlist.get(guid);
        }
        return null;
    }

}
