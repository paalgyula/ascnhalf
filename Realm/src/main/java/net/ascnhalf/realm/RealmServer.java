/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm;

import groovy.ui.Console;
import net.ascnhalf.commons.log4j.LoggingService;
import net.ascnhalf.commons.network.jmx.JmxNetworkService;
import net.ascnhalf.commons.network.netty.service.NetworkService;
import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.commons.threadpool.ThreadPoolManager;
import net.ascnhalf.realm.config.Config;
import net.ascnhalf.realm.service.*;
import net.ascnhalf.realm.utils.gui.ManagerFrame;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.*;

/**
 * The Class RealmServer.
 */
@SuppressWarnings("unused")
public class RealmServer {

    public static Console console;

    public static ApplicationContext appContext;
    private static ManagerFrame managerFrame;

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        if (!GraphicsEnvironment.isHeadless()) {
            RealmServer.managerFrame = new ManagerFrame();
            RealmServer.managerFrame.show();
        }

        appContext = new ClassPathXmlApplicationContext("/applicationConfig.xml");

        if (RealmServer.managerFrame != null)
            RealmServer.managerFrame.setApplicationContext(appContext);

        ServiceContent.setContext(appContext);

        appContext.getBean(Config.class);
        appContext.getBean(LoggingService.class).start();
        appContext.getBean(ThreadPoolManager.class).start();
        appContext.getBean(MapService.class).start();
        appContext.getBean(PlayerClassLevelInfoStorages.class).start();
        appContext.getBean(SimpleStorages.class).start();
        appContext.getBean(ItemStorages.class).start();
        appContext.getBean(UpdateService.class).start();
        appContext.getBean(ChatService.class).start();
        appContext.getBean(JmxNetworkService.class).start();

        System.gc();

        appContext.getBean(NetworkService.class).start();
    }
}
