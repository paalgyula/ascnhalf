package net.ascnhalf.realm.model.player.chat;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 13:29
 */
public enum ChatColor {
    MSG_COLOR_NORMAL("|r"),
    MSG_COLOR_LIGHTRED("|cffff6060"),
    MSG_COLOR_LIGHTBLUE("|cff00ccff"),
    MSG_COLOR_TORQUISEBLUE("|cff00C78C"),
    MSG_COLOR_SPRINGGREEN("|cff00FF7F"),
    MSG_COLOR_GREENYELLOW("|cffADFF2F"),
    MSG_COLOR_BLUE("|cff0000ff"),
    MSG_COLOR_PURPLE("|cffDA70D6"),
    MSG_COLOR_GREEN("|cff00ff00"),
    MSG_COLOR_RED("|cffff0000"),
    MSG_COLOR_GOLD("|cffffcc00"),
    MSG_COLOR_GOLD2("|cffFFC125"),
    MSG_COLOR_GREY("|cff888888"),
    MSG_COLOR_WHITE("|cffffffff"),
    MSG_COLOR_SUBWHITE("|cffbbbbbb"),
    MSG_COLOR_MAGENTA("|cffff00ff"),
    MSG_COLOR_YELLOW("|cffffff00"),
    MSG_COLOR_ORANGEY("|cffFF4500"),
    MSG_COLOR_CHOCOLATE("|cffCD661D"),
    MSG_COLOR_CYAN("|cff00ffff"),
    MSG_COLOR_IVORY("|cff8B8B83"),
    MSG_COLOR_LIGHTYELLOW("|cffFFFFE0"),
    MSG_COLOR_SEXGREEN("|cff71C671"),
    MSG_COLOR_SEXTEAL("|cff388E8E"),
    MSG_COLOR_SEXPINK("|cffC67171"),
    MSG_COLOR_SEXBLUE("|cff00E5EE"),
    MSG_COLOR_SEXHOTPINK("|cffFF6EB4");

    String value;

    private ChatColor(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
