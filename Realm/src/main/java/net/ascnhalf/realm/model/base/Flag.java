package net.ascnhalf.realm.model.base;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 23:26
 */
public interface Flag {
    public int getValue();
}
