/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.guid;

// TODO: Auto-generated Javadoc

/**
 * The Enum TypeId.
 */
public enum TypeId {

    /**
     * The TYPEI d_ object.
     */
    TYPEID_OBJECT(0),
    /**
     * The TYPEI d_ item.
     */
    TYPEID_ITEM(1),
    /**
     * The TYPEI d_ container.
     */
    TYPEID_CONTAINER(2),
    /**
     * The TYPEI d_ unit.
     */
    TYPEID_UNIT(3),
    /**
     * The TYPEI d_ player.
     */
    TYPEID_PLAYER(4),
    /**
     * The TYPEI d_ gameobject.
     */
    TYPEID_GAMEOBJECT(5),
    /**
     * The TYPEI d_ dynamicobject.
     */
    TYPEID_DYNAMICOBJECT(6),
    /**
     * The TYPEI d_ corpse.
     */
    TYPEID_CORPSE(7);

    private int value;

    TypeId(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
