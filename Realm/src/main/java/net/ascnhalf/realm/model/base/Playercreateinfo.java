package net.ascnhalf.realm.model.base;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.08.11.
 * Time: 21:10
 */
@Entity
public class Playercreateinfo implements Serializable {
    @Id
    @Column(name = "race", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    private int race;

    @Id
    @Column(name = "class", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    private int clazz;

    @Basic
    @Column(name = "map", nullable = false, insertable = true, updatable = true, length = 5, precision = 0)
    private short map;


    @Basic
    @Column(name = "zone", nullable = false, insertable = true, updatable = true, length = 8, precision = 0)
    private int zone;


    @Basic
    @Column(name = "position_x", nullable = false, insertable = true, updatable = true, length = 12, precision = 0)
    private float positionX;


    @Basic
    @Column(name = "position_y", nullable = false, insertable = true, updatable = true, length = 12, precision = 0)
    private float positionY;


    @Basic
    @Column(name = "position_z", nullable = false, insertable = true, updatable = true, length = 12, precision = 0)
    private float positionZ;


    @Basic
    @Column(name = "orientation", nullable = false, insertable = true, updatable = true, length = 12, precision = 0)
    private float orientation;

    public Playercreateinfo() {
    }

    public int getRace() {
        return race;
    }

    public void setRace(int race) {
        this.race = race;
    }

    public int getClazz() {
        return clazz;
    }

    public void setClazz(int clazz) {
        this.clazz = clazz;
    }

    public short getMap() {
        return map;
    }

    public void setMap(short map) {
        this.map = map;
    }

    public int getZone() {
        return zone;
    }

    public void setZone(int zone) {
        this.zone = zone;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public float getPositionZ() {
        return positionZ;
    }

    public void setPositionZ(float positionZ) {
        this.positionZ = positionZ;
    }

    public float getOrientation() {
        return orientation;
    }

    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }
}
