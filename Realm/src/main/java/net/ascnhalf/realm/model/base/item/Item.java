/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.item;

import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.realm.model.base.WorldObject;
import net.ascnhalf.realm.model.base.guid.HighGuid;
import net.ascnhalf.realm.model.base.guid.TypeId;
import net.ascnhalf.realm.model.base.guid.TypeMask;
import net.ascnhalf.realm.model.base.update.ItemFields;
import net.ascnhalf.realm.model.base.update.ObjectFields;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.service.ItemStorages;

// TODO: Auto-generated Javadoc

/**
 * The Class Item.
 */
public class Item extends WorldObject {

    /**
     * The Constant INVENTORY_SLOT_BAG_0.
     */
    public static final int INVENTORY_SLOT_BAG_0 = 255;

    /**
     * The Constant NULL_SLOT.
     */
    private static final int NULL_SLOT = 255;

    /**
     * The slot.
     */
    private int slot = 0;

    /**
     * @param lowguid
     * @param itemId
     * @param owner
     */
    public Item(int lowguid, int itemId, Player owner) {
        super(lowguid, 0, HighGuid.HIGHGUID_ITEM);

        valuesCount = ItemFields.ITEM_END;
        m_uint32Values = new int[valuesCount];
        objectType.add(TypeMask.TYPEMASK_ITEM);
        objectTypeId = TypeId.TYPEID_ITEM;

        SetUInt32Value(ObjectFields.OBJECT_FIELD_GUID, lowguid);
        SetUInt32Value(ObjectFields.OBJECT_FIELD_GUID.getValue() + 1, 0x47000000);

        setEntry(itemId);
        setGuidValue(ItemFields.ITEM_FIELD_OWNER, owner.getObjectGuid());
        setGuidValue(ItemFields.ITEM_FIELD_CONTAINED, getObjectGuid());

        SetFloatValue(ObjectFields.OBJECT_FIELD_SCALE_X, 1.0f);

        SetUInt32Value(ItemFields.ITEM_FIELD_STACK_COUNT, 1);

        ItemPrototype proto = ServiceContent.getContext().getBean(ItemStorages.class).get(itemId);
        if (proto != null) {
            SetUInt32Value(ItemFields.ITEM_FIELD_MAXDURABILITY, proto.getMaxDurability());
            SetUInt32Value(ItemFields.ITEM_FIELD_DURABILITY, proto.getMaxDurability());

            /*
            for (int i = 0; i < MAX_ITEM_PROTO_SPELLS; ++i)
                SetSpellCharges(i, itemProto->Spells[i].SpellCharges);
            */
            SetUInt32Value(ItemFields.ITEM_FIELD_DURATION, proto.getDuration());
        }
    }

    /**
     * Sets the slot.
     *
     * @param slot the new slot
     */
    public void setSlot(int slot) {
        this.slot = slot;
    }

    /**
     * Gets the slot.
     *
     * @return the slot
     */
    public int getSlot() {
        return slot;
    }

    /**
     * Checks if is inventory pos.
     *
     * @param bag  the bag
     * @param slot the slot
     * @return true, if successful
     */
    public static boolean IsInventoryPos(int bag, int slot) {
        if (bag == INVENTORY_SLOT_BAG_0 && slot == NULL_SLOT)
            return true;
        if (bag == INVENTORY_SLOT_BAG_0 && (slot >= InventoryPackSlots.INVENTORY_SLOT_ITEM_START && slot < InventoryPackSlots.INVENTORY_SLOT_ITEM_END))
            return true;
        if (bag >= InventorySlots.INVENTORY_SLOT_BAG_START.getValue() && bag < InventorySlots.INVENTORY_SLOT_BAG_END.getValue())
            return true;
        if (bag == INVENTORY_SLOT_BAG_0 && (slot >= KeyRingSlots.KEYRING_SLOT_START && slot < CurrencyTokenSlots.CURRENCYTOKEN_SLOT_END))
            return true;
        return false;
    }

    /**
     * Checks if is equipment pos.
     *
     * @param bag  the bag
     * @param slot the slot
     * @return true, if successful
     */
    public static boolean IsEquipmentPos(int bag, int slot) {
        if (bag == INVENTORY_SLOT_BAG_0 && (slot < EquipmentSlots.END.getValue()))
            return true;
        if (bag == INVENTORY_SLOT_BAG_0 && (slot >= InventorySlots.INVENTORY_SLOT_BAG_START.getValue() && slot < InventorySlots.INVENTORY_SLOT_BAG_END.getValue()))
            return true;
        return false;
    }

    public static EquipmentSlots findEquipSlot(InventoryType inventoryType) {

        switch (inventoryType) {
            case HEAD:
                return EquipmentSlots.HEAD;
            case SHOULDERS:
                return EquipmentSlots.SHOULDERS;
            case BODY:
                return EquipmentSlots.BODY;
            case CHEST:
                return EquipmentSlots.CHEST;
            case ROBE:
                return EquipmentSlots.CHEST;
            case WAIST:
                return EquipmentSlots.WAIST;
            case LEGS:
                return EquipmentSlots.LEGS;
            case FEET:
                return EquipmentSlots.FEET;
            case WRISTS:
                return EquipmentSlots.WRISTS;
            case HANDS:
                return EquipmentSlots.HANDS;
            case CLOAK:
                return EquipmentSlots.BACK;
            case WEAPON:
                return EquipmentSlots.MAINHAND;
            case SHIELD:
                return EquipmentSlots.OFFHAND;
            case RANGED:
                return EquipmentSlots.RANGED;
            case TWO_HAND_WEAPON:
                return EquipmentSlots.MAINHAND;
            case TABARD:
                return EquipmentSlots.TABARD;
            case WEAPONMAINHAND:
                return EquipmentSlots.MAINHAND;
            case WEAPONOFFHAND:
                return EquipmentSlots.OFFHAND;
            case HOLDABLE:
                return EquipmentSlots.OFFHAND;
            case THROWN:
                return EquipmentSlots.RANGED;
            case RANGEDRIGHT:
                return EquipmentSlots.RANGED;
            default:
                return null;
        }
    }
}
