/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.item;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

/**
 * The Class _Socket.
 */
@Embeddable
@Access(AccessType.PROPERTY)
public final class _Socket {

    /**
     * The Color.
     */
    private int color;

    /**
     * The Content.
     */
    private int content;

    /**
     * Default constructor
     */
    public _Socket() {

    }

    /**
     * Instantiates a new _ socket.
     *
     * @param color   the color
     * @param content the content
     */
    public _Socket(int color, int content) {
        color = color;
        content = content;
    }

    /**
     * Gets the color.
     *
     * @return the color
     */
    public int getColor() {
        return color;
    }

    /**
     * Sets the color.
     *
     * @param color the color to set
     */
    public void setColor(int color) {
        color = color;
    }

    /**
     * Gets the content.
     *
     * @return the content
     */
    public int getContent() {
        return content;
    }

    /**
     * Sets the content.
     *
     * @param content the content to set
     */
    public void setContent(int content) {
        content = content;
    }

}

