package net.ascnhalf.realm.model.player;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.08.12.
 * Time: 15:47
 */
@Entity(name = "CharacterStartOutfit")
@Table(name = "charstartoutfit")
public class CharacterStartOutfit {

    @Id
    public int id;

    @Basic
    @Column(name = "clazz")
    public int clazz;

    @Basic
    @Column(name = "race")
    public int race;

    @Basic
    @Column(name = "gender")
    public int gender;

    @Basic
    @Column(name = "proto_id")
    public int protoId;

    @Basic
    @Column(name = "slot")
    public int slot;

    public CharacterStartOutfit() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClazz() {
        return clazz;
    }

    public void setClazz(int clazz) {
        this.clazz = clazz;
    }

    public int getRace() {
        return race;
    }

    public void setRace(int race) {
        this.race = race;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getProtoId() {
        return protoId;
    }

    public void setProtoId(int protoId) {
        this.protoId = protoId;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
