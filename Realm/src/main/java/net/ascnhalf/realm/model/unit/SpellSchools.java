/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.unit;

// TODO: Auto-generated Javadoc

/**
 * The Enum SpellSchools.
 */
public enum SpellSchools {

    /**
     * The SPEL l_ schoo l_ normal.
     */
    SPELL_SCHOOL_NORMAL,
    /**
     * The SPEL l_ schoo l_ holy.
     */
    SPELL_SCHOOL_HOLY,
    /**
     * The SPEL l_ schoo l_ fire.
     */
    SPELL_SCHOOL_FIRE,
    /**
     * The SPEL l_ schoo l_ nature.
     */
    SPELL_SCHOOL_NATURE,
    /**
     * The SPEL l_ schoo l_ frost.
     */
    SPELL_SCHOOL_FROST,
    /**
     * The SPEL l_ schoo l_ shadow.
     */
    SPELL_SCHOOL_SHADOW,
    /**
     * The SPEL l_ schoo l_ arcane.
     */
    SPELL_SCHOOL_ARCANE;

    /**
     * The MA x_ spel l_ school.
     */
    public static int MAX_SPELL_SCHOOL = 7;
}
