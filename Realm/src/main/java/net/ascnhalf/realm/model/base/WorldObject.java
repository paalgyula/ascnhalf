/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base;

import net.ascnhalf.commons.model.NamedObject;
import net.ascnhalf.commons.network.model.UpdateField;
import net.ascnhalf.realm.model.UpdateType;
import net.ascnhalf.realm.model.base.guid.HighGuid;
import net.ascnhalf.realm.model.base.guid.ObjectGuid;
import net.ascnhalf.realm.model.base.guid.TypeId;
import net.ascnhalf.realm.model.base.guid.TypeMask;
import net.ascnhalf.realm.model.base.update.*;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.model.unit.Units;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_UPDATE_OBJECT;
import org.jboss.netty.buffer.ChannelBuffer;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.EnumSet;
import java.util.List;


/**
 * The Class WorldObject.
 */
public class WorldObject extends NamedObject {

    /**
     * The guid.
     */
    ObjectGuid guid = null;

    /**
     * The object type.
     */
    protected EnumSet<TypeMask> objectType = EnumSet.noneOf(TypeMask.class);

    /**
     * The object type id.
     */
    protected TypeId objectTypeId;

    /**
     * The m_uint32 values.
     */
    protected int[] m_uint32Values;

    /**
     * The fload values
     */
    protected float[] m_floatValues;

    /**
     * The m_uint32 values_mirror.
     */
    protected ChannelBuffer m_uint32Values_mirror;

    /**
     * The values count.
     */
    protected int valuesCount = 0;

    /**
     * BitSet for update packet
     */
    protected BitSet bitSet = new BitSet();

    protected List<ChannelBuffer> updateBlocks = new ArrayList<ChannelBuffer>();

    /**
     * Instantiates a new world object.
     *
     * @param objectId the object id
     */
    public WorldObject(long objectId) {
        super((int) objectId);
        guid = new ObjectGuid(objectId);
        objectType.add(TypeMask.TYPEMASK_OBJECT);
    }

    public WorldObject(int lowguid, int entry, HighGuid guidHigh) {
        super(lowguid);
        guid = new ObjectGuid(guidHigh, entry, lowguid);
        objectType.add(TypeMask.TYPEMASK_OBJECT);
        switch (guidHigh) {
            case HIGHGUID_ITEM:
                m_uint32Values = new int[ItemFields.ITEM_END];
                break;
            default:
                m_uint32Values = new int[ObjectFields.OBJECT_END];
        }

        SetUInt32Value(ObjectFields.OBJECT_FIELD_GUID, lowguid);
        SetUInt32Value(ObjectFields.OBJECT_FIELD_GUID.getValue() + 1, 0x47000000);
    }

    public void setGuidValue(UpdateField updateField, ObjectGuid guid) {
        SetUInt64Value(updateField, guid.getRawValue());
    }

    /**
     * Sets object entry
     *
     * @param entry
     */
    public void setEntry(int entry) {
        SetUInt32Value(ObjectFields.OBJECT_FIELD_ENTRY, entry);
    }

    /**
     * Gets the object type.
     *
     * @return the objectType
     */
    public final EnumSet<TypeMask> getObjectType() {
        return objectType;
    }

    /**
     * Sets the object type.
     *
     * @param objectType the objectType to set
     */
    public final void setObjectType(EnumSet<TypeMask> objectType) {
        this.objectType = objectType;
    }

    /**
     * Gets the object guid.
     *
     * @return the guid
     */
    public final ObjectGuid getObjectGuid() {
        return guid;
    }

    /**
     * Gets the object type id.
     *
     * @return the m_objectTypeId
     */
    public TypeId getObjectTypeId() {
        return objectTypeId;
    }

    /**
     * Sets the object type id.
     *
     * @param ObjectTypeId the objectTypeId to set
     */
    public void setObjectTypeId(TypeId ObjectTypeId) {
        objectTypeId = ObjectTypeId;
    }

    /**
     * Update.
     */
    public SMSG_UPDATE_OBJECT update() {
        System.out.println(m_uint32Values.length);
        return null;
    }

    /**
     * Initfields.
     */
    public void initfields() {
        m_uint32Values = new int[valuesCount];
        m_floatValues = new float[valuesCount];
    }

    /**
     * Sets the u int32 value.
     *
     * @param index the index
     * @param value the value
     */
    public void SetUInt32Value(int index, int value) {
        m_uint32Values[index] = value;
        bitSet.set(index);
    }

    /**
     * Sets the u int32 value.
     *
     * @param updateField the PlayerFields enum
     * @param value       the value
     */
    public void SetUInt32Value(UpdateField updateField, int value) {
        SetUInt32Value(updateField.getValue(), value);
    }

    /**
     * Gets the u int32 value.
     *
     * @param index the index
     * @return the int
     */
    public int GetUInt32Value(int index) {
        return m_uint32Values[index];
    }

    /**
     * Gets the u int32 value.
     *
     * @param updateField the PlayerField
     * @return the int
     */
    public int GetUInt32Value(UpdateField updateField) {
        return GetUInt32Value(updateField.getValue());
    }

    /**
     * Sets the u int64 value.
     *
     * @param index the index
     * @param value the value
     */
    public void SetUInt64Value(int index, long value) {
        m_uint32Values[index + 1] = (int) (value >> 32);
        bitSet.set(index + 1);
        m_uint32Values[index] = (int) value;
        bitSet.set(index);

        /*
            SetUInt32Value( index, ((int)value >> 32) );
            SetUInt32Value( index + 1, (int)value );
        */
    }

    /**
     * Sets the u int64 value.
     *
     * @param updateField the i
     * @param value       the value
     */
    public void SetUInt64Value(UpdateField updateField, long value) {
        SetUInt64Value(updateField.getValue(), value);
    }

    /**
     * Gets the u int64 value.
     *
     * @param i the i
     * @return the long
     */
    public long GetUInt64Value(int i) {
        // fuck it
        //return m_uint32Values.getLong(i*4);
        return 0;
    }

    /**
     * Gets the u int64 value.
     *
     * @param updateField the i
     * @return the long
     */
    public long GetUInt64Value(UpdateField updateField) {
        return GetUInt64Value(updateField.getValue());
    }

    /**
     * _ load into data field.
     *
     * @param strings the strings
     * @param from    the from
     * @param count   the count
     */
    public void _LoadIntoDataField(String[] strings, int from, int count) {
        for (int i = 0; i < strings.length; i++) {
            try {
                SetUInt32Value(from + i, Integer.decode(strings[i]));
            } catch (NumberFormatException e) {
                // Hmmmm... Manual DB Edit? N00B!
            }
        }
    }

    /**
     * Load values.
     *
     * @param data the data
     * @return true, if successful
     */
    public boolean loadValues(String[] data) {
        if (data.length != valuesCount) {
            return false;
        }
        for (int i = 0; i < data.length; i++) {
            SetUInt32Value(i, Long.decode(data[i]).intValue());
        }
        return true;
    }

    /**
     * Sets the byte value.
     *
     * @param index  the index
     * @param offset the offset
     * @param value  the value
     */
    public void SetByteValue(int index, int offset, byte value) {
        if (offset > 4)
            return;
        int rvalue = GetUInt32Value(index);
        if ((rvalue >> (offset * 8)) != value) {
            rvalue &= ~(0xFF << (offset * 8));
            rvalue |= (value << (offset * 8));
        }
        SetUInt32Value(index, rvalue);
        bitSet.set(index);
    }

    /**
     * Sets the byte value.
     *
     * @param updateField the PlayerFields index
     * @param offset      the offset
     * @param value       the value
     */
    public void SetByteValue(UpdateField updateField, int offset, byte value) {
        SetByteValue(updateField.getValue(), offset, value);
    }

    /**
     * Sets the float value.
     *
     * @param index the i
     * @param value the value
     */
    public void SetFloatValue(int index, float value) {
        m_uint32Values[index] = Float.floatToRawIntBits(value);
        bitSet.set(index);
    }

    /**
     * Sets the float value.
     *
     * @param field the field
     * @param value the value
     */
    public void SetFloatValue(UpdateField field, float value) {
        SetFloatValue(field.getValue(), value);
    }

    /**
     * Gets the float value.
     *
     * @param index the index
     * @return the float
     */
    public float GetFloatValue(int index) {
        return m_floatValues[index];
    }

    /**
     * Gets the float value.
     *
     * @param updateField the i
     * @return the float
     */
    public float GetFloatValue(UpdateField updateField) {
        return GetFloatValue(updateField.getValue());
    }

    /**
     * Updatemask
     */
    public BitSet getBitSet() {
        return bitSet;
    }

    /**
     * Returns updatemask's set count (1)
     *
     * @return
     */
    public int getUpdateCount() {
        int count = 0;
        for (int i = 0; i < bitSet.length(); i++)
            if (bitSet.get(i))
                count++;
        return count;
    }

    // For clear any pending display changes
    public void clearBits() {
        bitSet.clear();
    }

    /**
     * Check if self is a gameobject
     *
     * @return true or false
     */
    public boolean isGameObject() {
        return getObjectTypeId() == TypeId.TYPEID_GAMEOBJECT;
    }

    /**
     * Check if it's a vehicle
     *
     * @return true or false
     */
    public boolean isVehicle() {
        // TODO: implement
        return false;
    }

    /**
     * Determines if player
     *
     * @return true or false
     */
    public boolean isPlayer() {
        return getObjectTypeId() == TypeId.TYPEID_PLAYER;
    }

    /**
     * Unit check
     *
     * @return
     */
    public boolean isCreature() {
        return getObjectTypeId() == TypeId.TYPEID_UNIT;
    }

    public void buildCreateUpdateBlockForPlayer(ChannelBuffer data, Player target) {
        int flags = 0; // uint32
        int flags2 = 0; // uint32

        UpdateType updatetype = UpdateType.CREATE_OBJECT;
        if ( /*IsCorpse()*/ false) {
            if (((Player) this).getDisplayId() == 0)
                return;

            updatetype = UpdateType.CREATE_SELF;
        }

        // any other case
        switch (getObjectTypeId()) {
            case TYPEID_ITEM:
            case TYPEID_CONTAINER:
                flags = 0x10;
                break;
            case TYPEID_UNIT:
                flags = 0x70;
                break;
            case TYPEID_PLAYER:
                flags = 0x70;
                return;
            case TYPEID_GAMEOBJECT:
                flags = 0x0350;
                break;
            case TYPEID_DYNAMICOBJECT:
                flags = 0x0150;
                break;
            case TYPEID_CORPSE:
                flags = 0x0150;
                break;
            default:
                // anyone else can get fucked and die!
        }

        if (target == this) {
            // player creating self
            flags |= 0x01;
            updatetype = UpdateType.CREATE_SELF;
        }

        // gameobject stuff
        if (isGameObject()) {
            // TODO implement it
            /*
            switch( m_uint32Values[GameObjectFields.GAMEOBJECT_BYTES_1.getValue()] ) {
                case GAMEOBJECT_TYPE_MO_TRANSPORT:
                    if(GetTypeFromGUID() != HIGHGUID_TYPE_TRANSPORTER)
                        return 0;   // bad transporter
                    else
                        flags = 0x0352;
                break;

                case GAMEOBJECT_TYPE_TRANSPORT:
                {
                    // deeprun tram, etc
                    flags = 0x252;
                }
                break;

                case GAMEOBJECT_TYPE_DUEL_ARBITER:
                {
                    // duel flags have to stay as updatetype 3, otherwise
                    // it won't animate
                    updatetype = UPDATETYPE_CREATE_YOURSELF;
                }
                break;
            }
            //The above 3 checks FAIL to identify transports, thus their flags remain 0x58, and this is BAAAAAAD! Later they don't get position x,y,z,o updates, so they appear randomly by a client-calculated path, they always face north, etc... By: VLack
            if(flags != 0x0352 && IsGameObject() && TO< GameObject* >(this)->GetInfo()->Type == GAMEOBJECT_TYPE_TRANSPORT && !(TO< GameObject* >(this)->GetOverrides() & GAMEOBJECT_OVERRIDE_PARENTROT))
                flags = 0x0352;
            */
        }

        if (isVehicle())
            flags |= UpdateFlags.VEHICLE.getValue();

        // build our actual update
        data.writeByte(updatetype.getValue());

        // we shouldn't be here, under any circumstances, unless we have a wowguid..
        // todo: ASSERT(m_wowGuid.GetNewGuidLen() > 0);

        data.writeBytes(getObjectGuid().pack());
        data.writeByte(getObjectTypeId().getValue());

        _BuildMovementUpdate(data, flags, flags2, target);

        // we have dirty data, or are creating for ourself.
        _SetCreateBits(target);

        // this will cache automatically if needed
        _BuildValuesUpdate(data, target);
    }

    public void _BuildMovementUpdate(ChannelBuffer data, int flags, int flags2, Player target) {
        //ByteBuffer* splinebuf = (m_objectTypeId == TYPEID_UNIT) ? target->GetAndRemoveSplinePacket(GetGUID()) : 0;
        ByteBuffer splinebuf = null;

        /*if (splinebuf != null) {
            flags2 |= MOVEFLAG_SPLINE_ENABLED | MOVEFLAG_MOVE_FORWARD;       //1=move forward
            if (GetTypeId() == TYPEID_UNIT) {
                if (TO_UNIT(this) - > GetAIInterface() - > HasWalkMode(WALKMODE_WALK))
                    flags2 |= MOVEFLAG_WALK;
            }
        }*/

        int moveflags2 = 0; // uint16 mostly seem to be used by vehicles to control what kind of movement is allowed
        if (isVehicle()) {
            /*
            Units u = (Units)(this);
            if ( u.getVehicleComponent() != null )
                moveflags2 |= u.getVehicleComponent().getMoveFlags2();

            if ( isCreature() ) {
                if ( u.hasAuraWithName(SPELL_AURA_ENABLE_FLIGHT ) )
                    flags2 |= (MOVEFLAG_NO_COLLISION | MOVEFLAG_AIR_SWIMMING);
            }*/
        }

        /*data.writeShort( flags ); // uint16

        Player pThis = null;
        MovementInfo moveinfo = null;

        if (isPlayer()) {
            pThis = (Player)this;
            if ( pThis.getGetSession() )
                moveinfo = pThis.GetSession().GetMovementInfo();
            if (target == this) {
                // Updating our last speeds.
                pThis.UpdateLastSpeeds();
            }
        }

        Units uThis = null;
        if ( isCreature() )
            uThis = (Units)this;


        if ( ( flags & UpdateFlags.LIVING.getValue() ) > 0 ) {  //0x20
            if (pThis && pThis.transporter_info.guid != 0)
                flags2 |= MOVEFLAG_TRANSPORT; //0x200
            else if (uThis != NULL && transporter_info.guid != 0 && uThis - > transporter_info.guid != 0)
                flags2 |= MOVEFLAG_TRANSPORT; //0x200

            if ((pThis != NULL) && pThis - > isRooted())
                flags2 |= MOVEFLAG_ROOTED;
            else if ((uThis != NULL) && uThis - > isRooted())
                flags2 |= MOVEFLAG_ROOTED;

            if (uThis != null) {
                //		 Don't know what this is, but I've only seen it applied to spirit healers.
                //		 maybe some sort of invisibility flag? :/
                switch (GetEntry()) {
                    case 6491:  // Spirit Healer
                    case 13116: // Alliance Spirit Guide
                    case 13117: // Horde Spirit Guide
                    {
                        flags2 |= MOVEFLAG_WATER_WALK; //0x10000000
                    }
                    break;
                }

                if (uThis - > GetAIInterface() - > IsFlying())
                    flags2 |= MOVEFLAG_NO_COLLISION; //0x400 Zack : Teribus the Cursed had flag 400 instead of 800 and he is flying all the time
                if (uThis - > GetAIInterface() - > onGameobject)
                    flags2 |= MOVEFLAG_ROOTED;
                if (uThis - > GetProto() - > extra_a9_flags) {
                    //do not send shit we can't honor
                    #define UNKNOWN_FLAGS2 (0x00002000 | 0x04000000 | 0x08000000)
                    uint32 inherit = uThis - > GetProto() - > extra_a9_flags & UNKNOWN_FLAGS2;
                    flags2 |= inherit;
                }
            }

            *data << (uint32) flags2;

            *data << (uint16) moveflags2;

            *data << getMSTime(); // uint32 this appears to be time in ms but can be any thing. Maybe packet serializer ?

            // this stuff:
            //   0x01 -> Enable Swimming?
            //   0x04 -> ??
            //   0x10 -> disables movement compensation and causes players to jump around all the place
            //   0x40 -> disables movement compensation and causes players to jump around all the place

            //Send position data, every living thing has these
            *data << (float) m_position.x;
            *data << (float) m_position.y;
            *data << (float) m_position.z;
            *data << (float) m_position.o;

            if (flags2 & MOVEFLAG_TRANSPORT) //0x0200
            {
                *data << WoWGuid(transporter_info.guid);
                *data <<float(transporter_info.x);
                *data <<float(transporter_info.y);
                *data <<float(transporter_info.z);
                *data <<float(transporter_info.o);
                *data << uint32(transporter_info.flags);
                *data << uint8(transporter_info.seat);
            }


            if ((flags2 & (MOVEFLAG_SWIMMING | MOVEFLAG_AIR_SWIMMING)) || (moveflags2 & MOVEFLAG2_ALLOW_PITCHING))   // 0x2000000+0x0200000 flying/swimming, || sflags & SMOVE_FLAG_ENABLE_PITCH
            {
                if (pThis && moveinfo)
                *data << moveinfo - > pitch;
                else
                *data << (float) 0; //pitch
            }

            if (pThis && moveinfo)
            *data << moveinfo - > unklast;
            else
            *data << (uint32) 0; //last fall time

            if (flags2 & MOVEFLAG_REDIRECTED)   // 0x00001000
            {
                if (moveinfo != NULL) {
                    *data << moveinfo - > redirectVelocity;
                    *data << moveinfo - > redirectSin;
                    *data << moveinfo - > redirectCos;
                    *data << moveinfo - > redirect2DSpeed;
                } else {
                    *data << (float) 0;
                    *data << (float) 1.0;
                    *data << (float) 0;
                    *data << (float) 0;
                }
            }

            if (m_walkSpeed == 0)
            *data << 8.0f;
            else
            *data << m_walkSpeed;    // walk speed
            if (m_runSpeed == 0)
            *data << 8.0f;
            else
            *data << m_runSpeed;    // run speed
            *data << m_backWalkSpeed;    // backwards walk speed
            *data << m_swimSpeed;        // swim speed
            *data << m_backSwimSpeed;    // backwards swim speed
            if (m_flySpeed == 0)
            *data << 8.0f;
            else
            *data << m_flySpeed;    // fly speed
            *data << m_backFlySpeed;    // back fly speed
            *data << m_turnRate;        // turn rate
            *data <<float(7);        // pitch rate, now a constant...

            if (flags2 & MOVEFLAG_SPLINE_ENABLED)   //VLack: On Mangos this is a nice spline movement code, but we never had such... Also, at this point we haven't got this flag, that's for sure, but fail just in case...
            {
                data - > append( * splinebuf);
                delete splinebuf;
            }
        } else //----------------------------------- No UPDATEFLAG_LIVING -----------------------------------
        {
            if (flags & UPDATEFLAG_POSITION)   //0x0100
            {
                *data << uint8(0);   //some say it is like parent guid ?
                *data << (float) m_position.x;
                *data << (float) m_position.y;
                *data << (float) m_position.z;
                *data << (float) m_position.x;
                *data << (float) m_position.y;
                *data << (float) m_position.z;
                *data << (float) m_position.o;

                if (IsCorpse())
                *data << (float) m_position.o; //VLack: repeat the orientation!
                else
                *data << (float) 0;
            } else if (flags & UPDATEFLAG_HAS_POSITION)  //0x40
            {
                if (flags & UPDATEFLAG_TRANSPORT && m_uint32Values[GAMEOBJECT_BYTES_1] == GAMEOBJECT_TYPE_MO_TRANSPORT) {
                    *data << (float) 0;
                    *data << (float) 0;
                    *data << (float) 0;
                } else {
                    *data << (float) m_position.x;
                    *data << (float) m_position.y;
                    *data << (float) m_position.z;
                }
                *data << (float) m_position.o;
            }
        }


        if (flags & UPDATEFLAG_LOWGUID)   //0x08
        *data << GetLowGUID();

        if (flags & UPDATEFLAG_HIGHGUID)   //0x10
        *data << GetHighGUID();

        if (flags & UPDATEFLAG_HAS_TARGET)   //0x04
        {
            if (IsUnit())
                FastGUIDPack( * data, TO_UNIT(this) - > GetTargetGUID());    //some compressed GUID
            else
            *data << uint64(0);
        }


        if (flags & UPDATEFLAG_TRANSPORT)   //0x2
        {
            *data << getMSTime();
        }
        if (flags & UPDATEFLAG_VEHICLE) {
            uint32 vehicleid = 0;

            if (IsCreature())
                vehicleid = TO < Creature * > (this) - > GetProto() - > vehicleid;
            else if (IsPlayer())
                vehicleid = TO < Player * > (this) - > mountvehicleid;

            *data << uint32(vehicleid);
            *data <<float(GetOrientation());
        }

        if (flags & UPDATEFLAG_ROTATION)   //0x0200
        {
            if (IsGameObject())
            *data << TO < GameObject * > (this) - > m_rotation;
            else
            *data << uint64(0);   //?
        }
        */
    }

    //=======================================================================================
    //  Creates an update block with the values of this object as
    //  determined by the updateMask.
    //=======================================================================================
    public void _BuildValuesUpdate(ChannelBuffer data, Player target) {
        boolean activate_quest_object = false;
        boolean reset = false;
        int oldflags = 0;

        // Creating?
        if (getBitSet().get(ObjectFields.OBJECT_FIELD_GUID.getValue()) && (target != null)) {
            if (isCreature()) {
                Units pThis = (Units) this;

                // Todo: implement tagger
                /*if(pThis->IsTagged() && (pThis->loot.gold || pThis->loot.items.size())) {
                    // Let's see if we're the tagger or not.
                    oldflags = m_uint32Values[UNIT_DYNAMIC_FLAGS];
                    uint32 Flags = m_uint32Values[UNIT_DYNAMIC_FLAGS];
                    uint32 oldFlags = 0;

                    if(pThis->GetTaggerGUID() == target->GetGUID())
                    {
                        // Our target is our tagger.
                        oldFlags = U_DYN_FLAG_TAGGED_BY_OTHER;

                        if(Flags & U_DYN_FLAG_TAGGED_BY_OTHER)
                            Flags &= ~oldFlags;

                        if(!(Flags & U_DYN_FLAG_LOOTABLE) && pThis->HasLootForPlayer(target))
                            Flags |= U_DYN_FLAG_LOOTABLE;
                    }
                    else
                    {
                        // Target is not the tagger.
                        oldFlags = U_DYN_FLAG_LOOTABLE;

                        if(!(Flags & U_DYN_FLAG_TAGGED_BY_OTHER))
                            Flags |= U_DYN_FLAG_TAGGED_BY_OTHER;

                        if(Flags & U_DYN_FLAG_LOOTABLE)
                            Flags &= ~oldFlags;
                    }

                    m_uint32Values[UNIT_DYNAMIC_FLAGS] = Flags;

                    updateMask->SetBit(UNIT_DYNAMIC_FLAGS);

                    reset = true;
                }*/
            }

            if ((target != null) && isGameObject()) {
                // TODO: implement gameobject
                /*GameObject* go = TO_GAMEOBJECT(this);
                QuestLogEntry* qle;
                GameObjectInfo* info;
                if(go->HasQuests())
                {
                    std::list<QuestRelation*>::iterator itr;
                    for(itr = go->QuestsBegin(); itr != go->QuestsEnd(); ++itr)
                    {
                        QuestRelation* qr = (*itr);
                        if(qr != NULL)
                        {
                            Quest* qst = qr->qst;
                            if(qst != NULL)
                            {
                                if((qr->type & QUESTGIVER_QUEST_START && !target->HasQuest(qst->id))
                                        ||	(qr->type & QUESTGIVER_QUEST_END && target->HasQuest(qst->id))
                                        )
                                {
                                    activate_quest_object = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    info = go->GetInfo();
                    if(info && (info->goMap.size() || info->itemMap.size()))
                    {
                        for(GameObjectGOMap::iterator itr = go->GetInfo()->goMap.begin(); itr != go->GetInfo()->goMap.end(); ++itr)
                        {
                            qle = target->GetQuestLogForEntry(itr->first->id);
                            if(qle != NULL)
                            {
                                if(qle->GetQuest()->count_required_mob == 0)
                                    continue;
                                for(uint32 i = 0; i < 4; ++i)
                                {
                                    if(qle->GetQuest()->required_mob[i] == static_cast<int32>(go->GetEntry()) && qle->GetMobCount(i) < qle->GetQuest()->required_mobcount[i])
                                    {
                                        activate_quest_object = true;
                                        break;
                                    }
                                }
                                if(activate_quest_object)
                                    break;
                            }
                        }

                        if(!activate_quest_object)
                        {
                            for(GameObjectItemMap::iterator itr = go->GetInfo()->itemMap.begin();
                            itr != go->GetInfo()->itemMap.end();
                            ++itr)
                            {
                                for(std::map<uint32, uint32>::iterator it2 = itr->second.begin();
                                it2 != itr->second.end();
                                ++it2)
                                {
                                    if((qle = target->GetQuestLogForEntry(itr->first->id)) != 0)
                                    {
                                        if(target->GetItemInterface()->GetItemCount(it2->first) < it2->second)
                                        {
                                            activate_quest_object = true;
                                            break;
                                        }
                                    }
                                }
                                if(activate_quest_object)
                                    break;
                            }
                        }
                    }
                }
                */
            }
        }


        if (activate_quest_object) {
            oldflags = m_uint32Values[GameObjectFields.GAMEOBJECT_DYNAMIC.getValue()];
            if (!getBitSet().get(GameObjectFields.GAMEOBJECT_DYNAMIC.getValue()))
                getBitSet().set(GameObjectFields.GAMEOBJECT_DYNAMIC.getValue());
            m_uint32Values[GameObjectFields.GAMEOBJECT_DYNAMIC.getValue()] = 1 | 8; // 8 to show sparkles
            reset = true;
        }

        // todo:ASSERT(updateMask && updateMask->GetCount()  == m_valuesCount);
        int bc; // uint32
        int values_count; // uint32
        if (getUpdateCount() > (2 * 0x20)) {    //if number of blocks > 2->  unit and player+item container
            //values_count = min<uint32>(bc * 32, m_valuesCount);
            bc = updateBlocks.size();
            if (bc * 32 < getUpdateCount())
                values_count = bc * 32;
            else
                values_count = getUpdateCount();
        } else {
            bc = updateBlocks.size();
            values_count = getUpdateCount();
        }

        /*
        *data << (uint8)bc;
        data->append(updateMask->GetMask(), bc * 4);

        for(uint32 index = 0; index < values_count; index ++)
        {
            if(updateMask->GetBit(index))
            {
                *data << m_uint32Values[ index ];
            }
        }

        if(reset)
        {
            switch(GetTypeId())
            {
                case TYPEID_UNIT:
                    m_uint32Values[UNIT_DYNAMIC_FLAGS] = oldflags;
                    break;
                case TYPEID_GAMEOBJECT:
                    m_uint32Values[GAMEOBJECT_DYNAMIC] = oldflags;
                    break;
            }
        }
        */
    }

    public void _SetCreateBits(Player target) {
        for (int i = 0; i < PlayerFields.PLAYER_END; i++)
            if (target.GetUInt32Value(i) > 0)
                target.getBitSet().set(i);
    }
}
