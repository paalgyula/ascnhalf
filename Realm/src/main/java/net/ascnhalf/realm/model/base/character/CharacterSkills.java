package net.ascnhalf.realm.model.base.character;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.16.
 * Time: 1:42
 */
@Entity
@Table(name = "character_skills", uniqueConstraints = @UniqueConstraint(columnNames = {"guid", "skill"}))
public class CharacterSkills implements Serializable {
    @Basic
    @Column(name = "guid", nullable = false, insertable = false, updatable = false)
    private long guid;

    @Id
    @Column(name = "skill", nullable = false, insertable = true, updatable = true, length = 8, precision = 0)
    private int skill;

    @Basic
    @Column(name = "current_value", nullable = false, insertable = true, updatable = true, length = 8, precision = 0)
    private int value;

    @Basic
    @Column(name = "max_value", nullable = false, insertable = true, updatable = true, length = 8, precision = 0)
    private int max;

    @ManyToOne
    @JoinColumn(name = "guid", nullable = false)
    private CharacterData characterData;

    public long getGuid() {
        return guid;
    }

    public void setGuid(long guid) {
        this.guid = guid;
    }

    public int getSkill() {
        return skill;
    }

    public void setSkill(int skill) {
        this.skill = skill;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public CharacterData getCharacterData() {
        return characterData;
    }

    public void setCharacterData(CharacterData characterData) {
        this.characterData = characterData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterSkills that = (CharacterSkills) o;

        if (max != that.max) return false;
        if (skill != that.skill) return false;
        if (value != that.value) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + skill;
        result = 31 * result + value;
        result = 31 * result + max;
        return result;
    }
}
