package net.ascnhalf.realm.model.player.chat;

import net.ascnhalf.realm.model.base.Flag;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 23:05
 */
public enum MemberFlag implements Flag {
    NONE(0x00),
    OWNER(0x01),
    MODERATOR(0x02),
    VOICED(0x04),
    MUTED(0x08),
    CUSTOM(0x10),
    MIC_MUTED(0x20);

    public int value;

    MemberFlag(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
