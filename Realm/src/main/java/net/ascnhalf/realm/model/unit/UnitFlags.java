/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.unit;

// TODO: Auto-generated Javadoc

/**
 * The Enum UnitFlags.
 */
public enum UnitFlags {

    /**
     * The UNIT_FLAG_UNK_0
     */
    UNIT_FLAG_UNK_0(1 << 0),

    /**
     * The UNIT_FLAG_NON_ATTACKABLE
     * not attackable
     */
    UNIT_FLAG_NON_ATTACKABLE(1 << 1),

    /**
     * The UNIT_FLAG_DISABLE_MOVE
     */
    UNIT_FLAG_DISABLE_MOVE(1 << 2),

    /**
     * The UNIT_FLAG_PVP_ATTACKABLE
     * allow apply pvp rules to attackable state in addition to faction dependent state
     */
    UNIT_FLAG_PVP_ATTACKABLE(1 << 3),

    /**
     * The UNIT_FLAG_RENAME
     */
    UNIT_FLAG_RENAME(1 << 4),

    /**
     * The UNIT_FLAG_PREPARATION
     * don't take reagents for spells with SPELL_ATTR_EX5_NO_REAGENT_WHILE_PREP
     */
    UNIT_FLAG_PREPARATION(1 << 5),

    /**
     * UNIT_FLAG_UNK_6
     */
    UNIT_FLAG_UNK_6(1 << 6),

    /**
     * The UNIT_FLAG_NOT_ATTACKABLE_1
     * ?? (UNIT_FLAG_PVP_ATTACKABLE | UNIT_FLAG_NOT_ATTACKABLE_1) is NON_PVP_ATTACKABLE
     */
    UNIT_FLAG_NOT_ATTACKABLE_1(1 << 7),

    /**
     * The UNIT_FLAG_OOC_NOT_ATTACKABLE
     * 2.0.8 - (OOC Out Of Combat) Can not be attacked when not in combat. Removed if unit for some reason enter combat (flag probably removed for the attacked and it's party/group only)
     */
    UNIT_FLAG_OOC_NOT_ATTACKABLE(1 << 8),

    /**
     * The UNIT_FLAG_PASSIVE
     * makes you unable to attack everything. Almost identical to our "civilian"-term. Will ignore it's surroundings and not engage in combat unless "called upon" or engaged by another unit.
     */
    UNIT_FLAG_PASSIVE(1 << 9),

    /**
     * The UNIT_FLAG_LOOTING
     * loot animation
     */
    UNIT_FLAG_LOOTING(1 << 10),

    /**
     * The UNIT_FLAG_PET_IN_COMBAT
     * in combat?, 2.0.8
     */
    UNIT_FLAG_PET_IN_COMBAT(1 << 11),

    /**
     * The UNIT_FLAG_PVP
     * changed in 3.0.3
     */
    UNIT_FLAG_PVP(1 << 12),

    /**
     * The UNIT_FLAG_SILENCED
     * silenced, 2.1.1
     */
    UNIT_FLAG_SILENCED(1 << 13),

    /**
     * The UNIT_FLAG_UNK_14
     * 2.0.8
     */
    UNIT_FLAG_UNK_14(1 << 14),

    /**
     * The UNIT_FLAG_UNK_15
     */
    UNIT_FLAG_UNK_15(1 << 15),

    /**
     * The UNIT_FLAG_UNK_16
     * removes attackable icon
     */
    UNIT_FLAG_UNK_16(1 << 16),

    /**
     * The UNIT_FLAG_PACIFIED
     * 3.0.3 ok
     */
    UNIT_FLAG_PACIFIED(1 << 17),

    /**
     * The UNIT_FLAG_STUNNED
     * 3.0.3 ok
     */
    UNIT_FLAG_STUNNED(1 << 18),

    /**
     * The UNIT_FLAG_IN_COMBAT
     */
    UNIT_FLAG_IN_COMBAT(1 << 19),

    /**
     * The UNIT_FLAG_TAXI_FLIGHT
     * disable casting at client side spell not allowed by taxi flight (mounted?), probably used with 0x4 flag
     */
    UNIT_FLAG_TAXI_FLIGHT(1 << 20),

    /**
     * The UNIT_FLAG_DISARMED
     * 3.0.3, disable melee spells casting..., "Required melee weapon" added to melee spells tooltip.
     */
    UNIT_FLAG_DISARMED(1 << 21),

    /**
     * The UNIT_FLAG_CONFUSED
     */
    UNIT_FLAG_CONFUSED(1 << 22),

    /**
     * The UNIT_FLAG_FLEEING
     */
    UNIT_FLAG_FLEEING(1 << 23),

    /**
     * The UNIT_FLAG_PLAYER_CONTROLLED
     * used in spell Eyes of the Beast for pet... let attack by controlled creature
     */
    UNIT_FLAG_PLAYER_CONTROLLED(1 << 24),

    /**
     * The UNIT_FLAG_NOT_SELECTABLE
     */
    UNIT_FLAG_NOT_SELECTABLE(1 << 25),

    /**
     * The UNIT_FLAG_SKINNABLE
     */
    UNIT_FLAG_SKINNABLE(1 << 26),

    /**
     * The UNIT_FLAG_MOUNT
     */
    UNIT_FLAG_MOUNT(1 << 27),

    /**
     * The UNIT_FLAG_UNK_28
     */
    UNIT_FLAG_UNK_28(1 << 28),

    /**
     * The UNIT_FLAG_UNK_29
     * used in Feing Death spell
     */
    UNIT_FLAG_UNK_29(1 << 29),

    /**
     * The UNIT_FLAG_SHEATHE
     */
    UNIT_FLAG_SHEATHE(1 << 30),

    /**
     * The UNIT_FLAG_UNK_31
     * set skinnable icon and also changes color of portrait
     */
    UNIT_FLAG_UNK_31(1 << 31);

    /**
     * The value.
     */
    private int value;


    /**
     * Instantiates a new unit flags.
     *
     * @param flag the flag
     */
    UnitFlags(int flag) {
        this.value = flag;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }
}
