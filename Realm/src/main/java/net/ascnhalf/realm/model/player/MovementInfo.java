package net.ascnhalf.realm.model.player;

import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.guid.ObjectGuid;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.08.20.
 * Time: 22:40
 */
public class MovementInfo {

    public long time; // uint32
    float pitch;// -1.55=looking down, 0=looking forward, +1.55=looking up
    float redirectSin;//on slip 8 is zero, on jump some other number
    float redirectCos, redirect2DSpeed;//9,10 changes if you are not on foot
    int unk11, unk12; // uint32
    int unk13; // uint8
    int unklast; // uint32 something related to collision
    public int unk_230; // uint16

    public float x, y, z, orientation;
    public int flags; // uint32
    float redirectVelocity;
    ObjectGuid transGuid;
    float transX, transY, transZ, transO, transUnk;
    int transUnk_2; // uint8

    public MovementInfo() {
        time = 0;
        pitch = 0.0f;
        redirectSin = 0.0f;
        redirectCos = 0.0f;
        redirect2DSpeed = 0.0f;
        unk11 = 0;
        unk12 = 0;
        unk13 = 0;
        unklast = 0;
        unk_230 = 0;
        x = 0.0f;
        y = 0.0f;
        z = 0.0f;
        orientation = 0.0f;
        flags = 0;
        redirectVelocity = 0.0f;
        transGuid = new ObjectGuid(0);
        transX = 0.0f;
        transY = 0.0f;
        transZ = 0.0f;
        transO = 0.0f;
        transUnk = 0.0f;
        transUnk_2 = 0;
    }

    public MovementInfo(CharacterData cd) {
        x = cd.getPositionX();
        y = cd.getPositionY();
        z = cd.getPositionZ();
        orientation = cd.getOrientation();

        if (cd.getTransguid() > 0) {
            transGuid = new ObjectGuid(cd.getTransguid());
            transX = cd.getTransX();
            transY = cd.getTransY();
            transZ = cd.getTransZ();
            transO = cd.getTransO();
        }
    }

    public void write(AbstractWoWServerPacket packet) {
        /*data << flags << unk_230 << getMSTime();

        data << x << y << z << orientation;

        if(flags & MOVEFLAG_TRANSPORT)
        {
            data << transGuid << transX << transY << transZ << transO << transUnk << transUnk_2;
        }
        if(flags & MOVEFLAG_SWIMMING)
        {
            data << pitch;
        }
        if(flags & MOVEFLAG_FALLING)
        {
            data << redirectVelocity << redirectSin << redirectCos << redirect2DSpeed;
        }
        if(flags & MOVEFLAG_SPLINE_MOVER)
        {
            data << unk12;
        }
        data << unklast;
        if(unk13)
            data << unk13;

            */
    }


    public void updateTime() {
        RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();
        this.time = rb.getUptime();
    }
}
