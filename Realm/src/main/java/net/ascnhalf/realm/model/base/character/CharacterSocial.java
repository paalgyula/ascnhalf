package net.ascnhalf.realm.model.base.character;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.16.
 * Time: 1:42
 */
@javax.persistence.Table(name = "character_social")
@Entity
public class CharacterSocial implements Serializable {
    private int guid;

    @javax.persistence.Column(name = "guid", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    private int friend;

    @javax.persistence.Column(name = "friend", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getFriend() {
        return friend;
    }

    public void setFriend(int friend) {
        this.friend = friend;
    }

    private boolean flags;

    @javax.persistence.Column(name = "flags", nullable = false, insertable = true, updatable = true, length = 0, precision = 0)
    @Id
    public boolean isFlags() {
        return flags;
    }

    public void setFlags(boolean flags) {
        this.flags = flags;
    }

    private String note;

    @javax.persistence.Column(name = "note", nullable = false, insertable = true, updatable = true, length = 48, precision = 0)
    @Basic
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterSocial that = (CharacterSocial) o;

        if (flags != that.flags) return false;
        if (friend != that.friend) return false;
        if (guid != that.guid) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = guid;
        result = 31 * result + friend;
        result = 31 * result + (flags ? 1 : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
