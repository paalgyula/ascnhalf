/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.item;

/**
 * The Enum InventoryType.
 */
public enum InventoryType {

    /**
     * The INVTYP e_ no n_ equip.
     */
    NON_EQUIP(-1),
    /**
     * The INVTYP e_ head.
     */
    HEAD(1),
    /**
     * The INVTYP e_ neck.
     */
    NECK(2),
    /**
     * The INVTYP e_ shoulders.
     */
    SHOULDERS(3),
    /**
     * The INVTYP e_ body.
     */
    BODY(4),
    /**
     * The INVTYP e_ chest.
     */
    CHEST(5),
    /**
     * The INVTYP e_ waist.
     */
    WAIST(6),
    /**
     * The INVTYP e_ legs.
     */
    LEGS(7),
    /**
     * The INVTYP e_ feet.
     */
    FEET(8),
    /**
     * The INVTYP e_ wrists.
     */
    WRISTS(9),
    /**
     * The INVTYP e_ hands.
     */
    HANDS(10),
    /**
     * The INVTYP e_ finger.
     */
    FINGER(11),
    /**
     * The INVTYP e_ trinket.
     */
    TRINKET(12),
    /**
     * The INVTYP e_ weapon.
     */
    WEAPON(13),

    TWO_HAND_WEAPON(21),
    /**
     * The INVTYP e_ shield.
     */
    SHIELD(14),
    /**
     * The INVTYP e_ ranged.
     */
    RANGED(15),
    /**
     * The INVTYP e_ cloak.
     */
    CLOAK(16),

    /**
     * The INVTYP e_ bag.
     */
    BAG(18),
    /**
     * The INVTYP e_ tabard.
     */
    TABARD(19),
    /**
     * The INVTYP e_ robe.
     */
    ROBE(20),
    /**
     * The INVTYP e_ weaponmainhand.
     */
    WEAPONMAINHAND(21),
    /**
     * The INVTYP e_ weaponoffhand.
     */
    WEAPONOFFHAND(22),
    /**
     * The INVTYP e_ holdable.
     */
    HOLDABLE(23),
    /**
     * The INVTYP e_ ammo.
     */
    AMMO(24),
    /**
     * The INVTYP e_ thrown.
     */
    THROWN(25),
    /**
     * The INVTYP e_ rangedright.
     */
    RANGEDRIGHT(26),
    /**
     * The INVTYP e_ quiver.
     */
    QUIVER(27),
    /**
     * The INVTYP e_ relic.
     */
    RELIC(0);

    private int value;

    InventoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
