package net.ascnhalf.realm.model.base.account;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.09.02.
 * Time: 20:43
 */
@Entity
@Table(name = "account_data")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class AccountData implements Serializable {
    @Id
    @Column(name = "guid", length = 11, nullable = false)
    private int guid;

    @Id
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type", length = 3, nullable = false)
    private AccountDataType type;

    @Basic
    @Column(name = "time", length = 11, nullable = false)
    private int time;

    @Lob
    @Column(name = "data", nullable = false)
    private String data;

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public AccountDataType getType() {
        return type;
    }

    public void setType(AccountDataType type) {
        this.type = type;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
