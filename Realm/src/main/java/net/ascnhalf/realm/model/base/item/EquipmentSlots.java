package net.ascnhalf.realm.model.base.item;

/**
 * Created with IntelliJ IDEA.
 * User: Adrian
 * Date: 2012.08.11.
 * Time: 23:09
 * To change this template use File | Settings | File Templates.
 */
public enum EquipmentSlots {
    START(0),
    HEAD(0),
    NECK(1),
    SHOULDERS(2),
    BODY(3),
    CHEST(4),
    WAIST(5),
    LEGS(6),
    FEET(7),
    WRISTS(8),
    HANDS(9),
    FINGER1(10),
    FINGER2(11),
    TRINKET1(12),
    TRINKET2(13),
    BACK(14),
    MAINHAND(15),
    OFFHAND(16),
    RANGED(17),
    TABARD(18),
    END(19);

    private int value;

    private EquipmentSlots(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
