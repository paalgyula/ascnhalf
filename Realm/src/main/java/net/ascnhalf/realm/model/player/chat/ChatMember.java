package net.ascnhalf.realm.model.player.chat;

import net.ascnhalf.realm.model.player.Player;

import java.util.Date;
import java.util.EnumSet;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 23:04
 */
public class ChatMember {
    private Player player;
    private EnumSet<MemberFlag> memberFlags = EnumSet.noneOf(MemberFlag.class);
    private Date joined = new Date();

    public ChatMember(Player player, EnumSet<MemberFlag> memberFlags) {
        this.player = player;
        this.memberFlags = memberFlags;
    }

    public ChatMember(Player player) {
        this.player = player;
        this.memberFlags.add(MemberFlag.NONE);
    }

    public void addFlag(MemberFlag flag) {
        memberFlags.add(flag);
    }

    public void removeFlag(MemberFlag flag) {
        memberFlags.remove(flag);
    }

    public int flagToInt() {
        int flags = 0;
        for (MemberFlag mf : memberFlags) {
            flags |= mf.getValue();
        }
        return flags;
    }

    public Player getPlayer() {
        return this.player;
    }

    public Date getJoined() {
        return joined;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Player) {
            Player that = (Player) o;
            return that.equals(this.player);
        } else if (o instanceof ChatMember) {
            ChatMember that = (ChatMember) o;
            if (that.getPlayer().equals(getPlayer()))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return player != null ? player.hashCode() : 0;
    }
}
