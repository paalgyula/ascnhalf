package net.ascnhalf.realm.model.base;

import net.ascnhalf.realm.model.Classes;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2012.08.12.
 * To change this template use File | Settings | File Templates.
 */
@Embeddable
public class PlayerClassLevelInfoPK implements Serializable {

    @Column(name = "class", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    private int clazz;

    @Column(name = "level", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    private int level;

    public PlayerClassLevelInfoPK() {
    }

    public PlayerClassLevelInfoPK(Classes clazz, int level) {
        this.clazz = clazz.getValue();
        this.level = level;
    }

    public PlayerClassLevelInfoPK(int clazz, int level) {
        this.clazz = clazz;
        this.level = level;
    }

    public int getClazz() {
        return clazz;
    }

    public void setClazz(int clazz) {
        this.clazz = clazz;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PlayerClassLevelInfoPK) {
            PlayerClassLevelInfoPK pcli = (PlayerClassLevelInfoPK) obj;
            if ((pcli.getClazz() == getClazz()) && (pcli.getLevel() == getLevel()))
                return true;
            else
                return false;
        } else
            return false;
    }

    @Override
    public int hashCode() {
        return 10000 * level + clazz;
    }
}
