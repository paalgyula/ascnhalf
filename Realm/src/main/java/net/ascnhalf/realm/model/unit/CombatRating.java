/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.unit;

// TODO: Auto-generated Javadoc

/**
 * The Enum CombatRating.
 */
public enum CombatRating {

    /**
     * The C r_ weapo n_ skill.
     */
    CR_WEAPON_SKILL,

    /**
     * The C r_ defens e_ skill.
     */
    CR_DEFENSE_SKILL,

    /**
     * The C r_ dodge.
     */
    CR_DODGE,
    /**
     * The C r_ parry.
     */
    CR_PARRY,

    /**
     * The C r_ block.
     */
    CR_BLOCK,
    /**
     * The C r_ hi t_ melee.
     */
    CR_HIT_MELEE,

    /**
     * The C r_ hi t_ ranged.
     */
    CR_HIT_RANGED,

    /**
     * The C r_ hi t_ spell.
     */
    CR_HIT_SPELL,

    /**
     * The C r_ cri t_ melee.
     */
    CR_CRIT_MELEE,

    /**
     * The C r_ cri t_ ranged.
     */
    CR_CRIT_RANGED,

    /**
     * The C r_ cri t_ spell.
     */
    CR_CRIT_SPELL,

    /**
     * The C r_ hi t_ take n_ melee.
     */
    CR_HIT_TAKEN_MELEE,

    /**
     * The C r_ hi t_ take n_ ranged.
     */
    CR_HIT_TAKEN_RANGED,

    /**
     * The C r_ hi t_ take n_ spell.
     */
    CR_HIT_TAKEN_SPELL,

    /**
     * The C r_ cri t_ take n_ melee.
     */
    CR_CRIT_TAKEN_MELEE,

    /**
     * The C r_ cri t_ take n_ ranged.
     */
    CR_CRIT_TAKEN_RANGED,

    /**
     * The C r_ cri t_ take n_ spell.
     */
    CR_CRIT_TAKEN_SPELL,

    /**
     * The C r_ hast e_ melee.
     */
    CR_HASTE_MELEE,

    /**
     * The C r_ hast e_ ranged.
     */
    CR_HASTE_RANGED,

    /**
     * The C r_ hast e_ spell.
     */
    CR_HASTE_SPELL,

    /**
     * The C r_ weapo n_ skil l_ mainhand.
     */
    CR_WEAPON_SKILL_MAINHAND,

    /**
     * The C r_ weapo n_ skil l_ offhand.
     */
    CR_WEAPON_SKILL_OFFHAND,

    /**
     * The C r_ weapo n_ skil l_ ranged.
     */
    CR_WEAPON_SKILL_RANGED,

    /**
     * The C r_ expertise.
     */
    CR_EXPERTISE,

    /**
     * The C r_ armo r_ penetration.
     */
    CR_ARMOR_PENETRATION,

    /**
     * The MA x_ comba t_ rating.
     */
    MAX_COMBAT_RATING;
}
