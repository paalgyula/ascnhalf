/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base;

import gnu.trove.map.hash.TLongObjectHashMap;
import net.ascnhalf.commons.network.model.SendablePacket;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.model.player.Player;

/**
 * The Class Map.
 */
public class Map {

    AbstractPacketSender sender;

    /**
     * The id.
     */
    int id = 0;

    /**
     * The player list.
     */
    TLongObjectHashMap<Player> playerList = new TLongObjectHashMap<Player>();

    /**
     * The units.
     */
    TLongObjectHashMap<WorldObject> units = new TLongObjectHashMap<WorldObject>();

    /**
     * Instantiates a new map.
     *
     * @param id the id
     */
    public Map(int id) {
        super();
        this.id = id;
    }

    /**
     * Adds the object.
     *
     * @param plObject the pl object
     */
    public void AddObject(WorldObject plObject) {
        switch (plObject.getObjectTypeId()) {
            case TYPEID_PLAYER:
                playerList.put(plObject.getObjectGuid().getRawValue(), (Player) plObject);
                break;
            case TYPEID_UNIT:
                units.put(plObject.getObjectGuid().getRawValue(), plObject);
                break;
            default:
                break;
        }

    }

    /**
     * Update.
     *
     * @return true, if successful
     */
    public boolean update() {
        /*
        for (Object pl : playerList.values()) {
			System.out.println("Player name: " + ((Units)pl).getName());
		};
		for (Object pl : units.values()) {
			System.out.println("Units name: " + ((Units)pl).getName());
		};
		System.out.println("Player on map " + playerList.size());
		System.out.println("units on map " + units.size());*/
        return true;
    }


    public void sendToSetExcept(AbstractPacketSender sender, SendablePacket packet, Player player) {
        for (Object pObj : playerList.values()) {
            if (!((Player) pObj).equals(player)) // Except self
                sender.send(((Player) pObj).getChannel(), packet);
        }
    }

    public void sendToSet(AbstractPacketSender sender, SendablePacket packet) {
        for (Object pObj : playerList.values()) {
            sender.send(((Player) pObj).getChannel(), packet);
        }
    }

    public void sendToAll(AbstractPacketSender sender, SendablePacket packet) {
        for (Object pObj : playerList.values()) {
            sender.send(((Player) pObj).getChannel(), packet);
        }
    }
}
