package net.ascnhalf.realm.model.base.character;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.16.
 * Time: 1:42
 */
@javax.persistence.Table(name = "character_spell")
@Entity
public class CharacterSpell implements Serializable {
    private int guid;

    @javax.persistence.Column(name = "guid", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    private int spell;

    @javax.persistence.Column(name = "spell", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    public int getSpell() {
        return spell;
    }

    public void setSpell(int spell) {
        this.spell = spell;
    }

    private byte active;

    @javax.persistence.Column(name = "active", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    @Basic
    public byte getActive() {
        return active;
    }

    public void setActive(byte active) {
        this.active = active;
    }

    private byte disabled;

    @javax.persistence.Column(name = "disabled", nullable = false, insertable = true, updatable = true, length = 3, precision = 0)
    @Basic
    public byte getDisabled() {
        return disabled;
    }

    public void setDisabled(byte disabled) {
        this.disabled = disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CharacterSpell that = (CharacterSpell) o;

        if (active != that.active) return false;
        if (disabled != that.disabled) return false;
        if (guid != that.guid) return false;
        if (spell != that.spell) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = guid;
        result = 31 * result + spell;
        result = 31 * result + (int) active;
        result = 31 * result + (int) disabled;
        return result;
    }
}
