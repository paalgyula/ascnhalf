/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.unit;

// TODO: Auto-generated Javadoc

/**
 * The Enum UnitMoveType.
 */
public enum UnitMoveType {

    /**
     * The MOV e_ walk.
     */
    MOVE_WALK,
    /**
     * The MOV e_ run.
     */
    MOVE_RUN,
    /**
     * The MOV e_ ru n_ back.
     */
    MOVE_RUN_BACK,
    /**
     * The MOV e_ swim.
     */
    MOVE_SWIM,
    /**
     * The MOV e_ swi m_ back.
     */
    MOVE_SWIM_BACK,
    /**
     * The MOV e_ tur n_ rate.
     */
    MOVE_TURN_RATE,
    /**
     * The MOV e_ flight.
     */
    MOVE_FLIGHT,
    /**
     * The MOV e_ fligh t_ back.
     */
    MOVE_FLIGHT_BACK,
    /**
     * The MOV e_ pitc h_ rate.
     */
    MOVE_PITCH_RATE;

    /**
     * The Constant MAX_MOVE_TYPE.
     */
    public final static int MAX_MOVE_TYPE = 9;
}
