package net.ascnhalf.realm.model.base.character;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.08.21.
 * Time: 16:13
 */
@Entity
@Table(name = "dbc_char_races")
public class CharacterRaces {
    @Id
    @Column(nullable = false)
    private int race;

    @Basic
    @Column(nullable = false)
    private int maleDisplayId;

    @Basic
    @Column(nullable = false)
    private int femaleDisplayId;

    @Basic
    @Column(nullable = false)
    private int exploredZones;


    public CharacterRaces() {
    }

    public CharacterRaces(int race, int maleDisplayId, int femaleDisplayId, int exploredZones) {
        this.race = race;
        this.maleDisplayId = maleDisplayId;
        this.femaleDisplayId = femaleDisplayId;
        this.exploredZones = exploredZones;
    }

    public int getRace() {
        return race;
    }

    public void setRace(int race) {
        this.race = race;
    }

    public int getMaleDisplayId() {
        return maleDisplayId;
    }

    public void setMaleDisplayId(int maleDisplayId) {
        this.maleDisplayId = maleDisplayId;
    }

    public int getFemaleDisplayId() {
        return femaleDisplayId;
    }

    public void setFemaleDisplayId(int femaleDisplayId) {
        this.femaleDisplayId = femaleDisplayId;
    }

    public int getExploredZones() {
        return exploredZones;
    }

    public void setExploredZones(int exploredZones) {
        this.exploredZones = exploredZones;
    }
}
