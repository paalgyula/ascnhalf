/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.item;

// TODO: Auto-generated Javadoc

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

/**
 * The Class _ItemStat.
 */
@Embeddable
@Access(AccessType.FIELD)
public final class _ItemStat {

    /**
     * The Item stat type.
     */
    private int itemStatType;

    /**
     * The Item stat value.
     */
    private int itemStatValue;

    /**
     * Default no-arg constructor
     */
    public _ItemStat() {
    }

    /**
     * Instantiates a new _Item stat.
     *
     * @param itemStatType  the item stat type
     * @param itemStatValue the item stat value
     */
    public _ItemStat(int itemStatType, int itemStatValue) {
        this.itemStatType = itemStatType;
        this.itemStatValue = itemStatValue;
    }

    /**
     * Gets the item stat type.
     *
     * @return the itemStatType
     */
    public int getItemStatType() {
        return itemStatType;
    }

    /**
     * Sets the item stat type.
     *
     * @param itemStatType the itemStatType to set
     */
    public void setItemStatType(int itemStatType) {
        this.itemStatType = itemStatType;
    }

    /**
     * Gets the item stat value.
     *
     * @return the itemStatValue
     */
    public int getItemStatValue() {
        return itemStatValue;
    }

    /**
     * Sets the item stat value.
     *
     * @param itemStatValue the itemStatValue to set
     */
    public void setItemStatValue(int itemStatValue) {
        this.itemStatValue = itemStatValue;
    }
}
