package net.ascnhalf.realm.model.base.update;

import net.ascnhalf.commons.network.model.UpdateField;
import net.ascnhalf.realm.model.base.guid.ObjectGuid;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.08.14.
 * Time: 11:36
 */
public class UpdateFieldUtils {
    public static UpdateField getField(int code, ObjectGuid objectGuid) {
        if (code < ObjectFields.OBJECT_END)
            return ObjectFields.get(code);

        // Check the objectGuid
        switch (objectGuid.GetTypeId()) {
            case TYPEID_ITEM:
                if (code < ItemFields.ITEM_END)
                    return ItemFields.get(code);
                else
                    return null;
            case TYPEID_PLAYER:
            case TYPEID_UNIT:
                if (code < UnitField.UNIT_END)
                    return UnitField.get(code);
                else if (code < PlayerFields.PLAYER_END)
                    return PlayerFields.getNearest(code);
                else
                    return null;
        }

        return null;
    }
}
