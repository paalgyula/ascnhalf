/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.model.base.item;

// TODO: Auto-generated Javadoc

/**
 * The Class InventoryPackSlots.
 */
public class InventoryPackSlots {

    /**
     * The INVENTOR y_ slo t_ ite m_ start.
     */
    public static int INVENTORY_SLOT_ITEM_START = 23;

    /**
     * The INVENTOR y_ slo t_ ite m_ end.
     */
    public static int INVENTORY_SLOT_ITEM_END = 39;
}
