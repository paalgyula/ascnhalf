package net.ascnhalf.realm.model.player;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.08.19.
 * Time: 17:56
 */
@Entity
@Table(name = "dbc_character_start_items")
public class CharacterStartOutfitItems {
    @Id
    @TableGenerator(allocationSize = 1, table = "sequences", name = "DBC_CHAR_START_ITEMS")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "DBC_CHAR_START_ITEMS")
    int id;

    @Basic
    @Column(name = "outfit_id", insertable = false, updatable = false)
    int outfit;

    @Basic
    @Column(name = "display_id")
    int displayId;

    @Basic
    @Column(name = "item_id")
    int itemId;

    @Basic
    @Column(name = "inventory_slot")
    int inventorySlot;

    @ManyToOne
    @JoinColumn(name = "outfit_id")
    private CharacterStartOutfit characterStartOutfit;

    public CharacterStartOutfitItems() {
    }

    public CharacterStartOutfitItems(int displayId, int itemId, int inventorySlot) {
        this.displayId = displayId;
        this.itemId = itemId;
        this.inventorySlot = inventorySlot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOutfit() {
        return outfit;
    }

    public void setOutfit(int outfit_id) {
        this.outfit = outfit_id;
    }

    public int getDisplayId() {
        return displayId;
    }

    public void setDisplayId(int displayId) {
        this.displayId = displayId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getInventorySlot() {
        return inventorySlot;
    }

    public void setInventorySlot(int inventorySlot) {
        this.inventorySlot = inventorySlot;
    }

    public CharacterStartOutfit getCharacterStartOutfit() {
        return characterStartOutfit;
    }

    public void setCharacterStartOutfit(CharacterStartOutfit characterStartOutfit) {
        this.characterStartOutfit = characterStartOutfit;
    }
}
