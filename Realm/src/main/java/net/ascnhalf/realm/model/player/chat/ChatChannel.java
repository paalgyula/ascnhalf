package net.ascnhalf.realm.model.player.chat;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHANNEL_LIST;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHANNEL_NOTIFY;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_MESSAGE_CHAT;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 23:04
 */
public class ChatChannel {
    private String name;
    private String password = "";
    private int flags;
    private boolean ownerless = false;
    private boolean isCity = false;
    private boolean guildrec = false;

    // LFG, gm_sync_channel
    private boolean joinable = true;

    private Set<ChatMember> playerList = new HashSet<ChatMember>();

    private AbstractPacketSender sender;

    public ChatChannel(AbstractPacketSender sender, String name, String password) {
        this(sender, name);
        if (!ownerless)
            this.password = password;
    }

    public ChatChannel(AbstractPacketSender sender, String name) {
        this.sender = sender;
        this.name = name;
        if (name.toLowerCase().startsWith("general - ")) {
            ownerless = true;
            flags = 0x10 | 0x08;
        }

        if (name.toLowerCase().startsWith("trade - ")) {
            isCity = true;
            ownerless = true;
            flags = 0x20 | 0x10 | 0x08 | 0x04;
        }

        if (name.toLowerCase().startsWith("localdefense - ")) {
            ownerless = true;
            flags = 0x10 | 0x08;
        }

        if (name.toLowerCase().startsWith("guildrecruitment - ")) {
            ownerless = true;
            flags = 0x20 | 0x10 | 0x08;
        }

        if (name.toLowerCase().equals("lookingforgroup")) {
            ownerless = true;
            flags = 0x40 | 0x10;
            joinable = false;
        }
    }

    public boolean isMemeber(Player player) {
        return playerList.contains(new ChatMember(player));
    }

    public void sendNotMember(Player player) {
        sender.send(player.getChannel(), new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_NOT_ON_2));
    }

    public void say(Player player, String message) {
        if (playerList.contains(new ChatMember(player)))
            for (ChatMember m : playerList) {
                SMSG_MESSAGE_CHAT msg = new SMSG_MESSAGE_CHAT(message, name);
                msg.setSpeaker(player);
                sender.send(m.getPlayer().getChannel(), msg);
            }
        else
            sendNotMember(player);
    }

    private void addMember(Player player) {

        playerList.add(new ChatMember(player));
    }

    public void forceJoin(Player player, boolean silent) {
        if (isMemeber(player))
            return;

        ChatMember member = new ChatMember(player);
        if (playerList.size() == 0 && !ownerless)
            member.addFlag(MemberFlag.OWNER);

        addMember(player);

        if (!silent)
            sender.send(player.getChannel(), new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_YOUJOINED));
    }

    public void leave(Player player) {
        playerList.remove(new ChatMember(player));

        SMSG_CHANNEL_NOTIFY notifyMsg = new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_LEFT);
        notifyMsg.setGuid(player.getObjectGuid().getRawValue());
        if (!ownerless)
            for (ChatMember m : playerList)
                sender.send(player.getChannel(), notifyMsg);
    }

    /**
     * Player requested to join the channel
     *
     * @param player   {@link Player}Player
     * @param password channel password
     */
    public void requestJoin(Player player, String password) {
        if (!joinable) return; // LFG or banned channels

        if ((this.password == "") || (password.equals(this.password))) {
            if (isMemeber(player)) {
                sender.send(player.getChannel(), new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_ALREADY_ON));
                return;
            }

            // implement LFG, Trade pattern check
            ChatMember member = new ChatMember(player);
            if (playerList.size() == 0 && !ownerless)
                member.addFlag(MemberFlag.OWNER);

            addMember(player);

            sender.send(player.getChannel(), new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_YOUJOINED));
            return;
        } else {
            sender.send(player.getChannel(), new SMSG_CHANNEL_NOTIFY(name, SMSG_CHANNEL_NOTIFY.CHANNEL_NOTIFY_FLAG_WRONGPASS));
        }
    }

    /**
     * Returns character names from the channel
     * Used for debugging
     *
     * @return String[]
     */
    public String[] getPlayerList() {
        String[] players = new String[playerList.size()];
        Iterator<ChatMember> itr = playerList.iterator();
        int i = 0;
        while (itr.hasNext()) {
            players[i++] = itr.next().getPlayer().getName();
        }

        return players;
    }

    /**
     * Returns the members of the channel
     *
     * @return Set of members
     */
    public Set<ChatMember> getMembers() {
        return playerList;
    }

    /**
     * Sends the current channel member list to the player
     *
     * @param player
     */
    public void sendMemberList(Player player) {
        if (!isMemeber(player)) {
            sendNotMember(player);
            return;
        }

        sender.send(player.getChannel(), new SMSG_CHANNEL_LIST(name, playerList));
    }
}
