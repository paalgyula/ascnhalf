package net.ascnhalf.realm.model.base.character;

import net.ascnhalf.realm.model.base.account.AccountData;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.09.02.
 * Time: 19:27
 */
@Entity
@Table(name = "character_account_data")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class CharacterAccountData extends AccountData {

    @ManyToOne
    @JoinColumn(name = "guid", insertable = false, updatable = false, nullable = false)
    private CharacterData characterData;

    public CharacterData getCharacterData() {
        return characterData;
    }

    public void setCharacterData(CharacterData characterData) {
        this.characterData = characterData;
    }
}
