package net.ascnhalf.realm.model.base.update;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.08.16.
 * Time: 19:07
 */
public enum UpdateFlags {
    NONE(0x0000),
    SELF(0x0001),
    TRANSPORT(0x0002),
    HAS_ATTACKING_TARGET(0x0004),
    LOWGUID(0x0008),
    /**
     * 0x0010
     */
    HIGHGUID(0x0010),
    /**
     * 0x0020
     */
    LIVING(0x0020),
    /**
     * 0x0040
     */
    HAS_POSITION(0x0040),
    VEHICLE(0x0080),
    POSITION(0x0100),
    ROTATION(0x0200);

    private int value;

    UpdateFlags(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
