package net.ascnhalf.realm.model;

import net.ascnhalf.realm.model.base.character.CharacterData;

import javax.persistence.*;

/**
 * The Class InventoryItem.
 */
@Entity
@Table(name = "character_inventory", uniqueConstraints =
@UniqueConstraint(columnNames = {"owner_guid", "slot", "item"}))
public class InventoryItem {

    @Column(name = "owner_guid", unique = true, nullable = false, insertable = false, updatable = false)
    @GeneratedValue
    private Long owner_guid;

    /**
     * The bag_guid.
     */
    @Basic
    @Column(name = "bag", length = 11, nullable = false)
    private int bag_guid;

    /**
     * The item_guid.
     */
    @Basic
    @Column(name = "item_template", length = 11, nullable = false)
    private int item_guid;

    /**
     * The item_id.
     */
    @Id
    @TableGenerator(table = "sequences", allocationSize = 2, name = "char_inv_item")
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "char_inv_item")
    @Column(name = "item", length = 11, nullable = false)
    private int item_id;

    /**
     * The slot.
     */
    @Basic
    @Column(name = "slot", length = 3, nullable = false)
    private int slot;

    @Basic
    @Column(name = "data", length = 512, nullable = false)
    private String data = "";

    @ManyToOne
    @JoinColumn(name = "owner_guid")
    private CharacterData characterData;

    /**
     * No argument constructor for persistence
     */
    public InventoryItem() {
    }

    public InventoryItem(int item_guid, int slot) {
        this.item_guid = item_guid;
        this.slot = slot;
    }

    /**
     * Instantiates a new inventory template.
     *
     * @param data     the data
     * @param bagGuid  the bag guid
     * @param slot     the slot
     * @param itemGuid the item guid
     * @param itemId   the item id
     */
    public InventoryItem(String data, int bagGuid, byte slot, int itemGuid, int itemId) {
        bag_guid = bagGuid;
        this.slot = slot;
        item_guid = itemGuid;
        item_id = itemId;
    }

    public Long getOwner_guid() {
        return owner_guid;
    }

    public void setOwner_guid(Long owner_guid) {
        this.owner_guid = owner_guid;
    }

    /**
     * Gets the bag_guid.
     *
     * @return the bag_guid
     */
    public final int getBag_guid() {
        return bag_guid;
    }

    /**
     * Gets the item_guid.
     *
     * @return the item_guid
     */
    public final int getItem_guid() {
        return item_guid;
    }

    /**
     * Gets the item_id.
     *
     * @return the item_id
     */
    public final int getItem_id() {
        return item_id;
    }

    /**
     * Gets the slot.
     *
     * @return the slot
     */
    public final int getSlot() {
        return slot;
    }

    /**
     * Sets the bag_guid.
     *
     * @param bagGuid the bag_guid to set
     */
    public final void setBag_guid(int bagGuid) {
        bag_guid = bagGuid;
    }

    /**
     * Sets the item_guid.
     *
     * @param itemGuid the item_guid to set
     */
    public final void setItem_guid(int itemGuid) {
        item_guid = itemGuid;
    }

    /**
     * Sets the item_id.
     *
     * @param itemId the item_id to set
     */
    public final void setItem_id(int itemId) {
        item_id = itemId;
    }

    /**
     * Sets the slot.
     *
     * @param slot the slot to set
     */
    public final void setSlot(int slot) {
        this.slot = slot;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public CharacterData getCharacterData() {
        return characterData;
    }

    public void setCharacterData(CharacterData characterData) {
        this.characterData = characterData;
    }
}
