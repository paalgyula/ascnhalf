/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/**
 *
 */
package net.ascnhalf.realm.model.player;

// TODO: Auto-generated Javadoc

/**
 * The Enum BaseModGroup.
 */
public enum BaseModGroup {

    /**
     * The CRI t_ percentage.
     */
    CRIT_PERCENTAGE,
    /**
     * The RANGE d_ cri t_ percentage.
     */
    RANGED_CRIT_PERCENTAGE,
    /**
     * The OFFHAN d_ cri t_ percentage.
     */
    OFFHAND_CRIT_PERCENTAGE,
    /**
     * The SHIEL d_ bloc k_ value.
     */
    SHIELD_BLOCK_VALUE,
    /**
     * The BASEMO d_ end.
     */
    BASEMOD_END
}
