package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.08.
 * Time: 3:58
 * To change this template use File | Settings | File Templates.
 */
public class MSG_MOVE_START_FORWARD extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(MSG_MOVE_START_FORWARD.class);

    @Override
    protected void writeImpl() {
        log.fatal("MSG_MOVE_START_FORWARD");
    }
}
