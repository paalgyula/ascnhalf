package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.08.
 * Time: 2:44
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_MOVE_FALL_LAND extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass());

    long guid;

    Integer unkLast, unk13;

    @Override
    protected void readImpl() throws RuntimeException {
        guid = readG();

        Player player = getPlayer();

        player.m_movementInfo.flags = readD();
        player.m_movementInfo.unk_230 = readH();
        player.m_movementInfo.time = readD();
        player.m_movementInfo.x = readF();
        player.m_movementInfo.y = readF();
        player.m_movementInfo.z = readF();
        player.m_movementInfo.orientation = readF();

        // Epp lepottyant, most nincs ilyen
        /*
        fallTime = readD();

        unk8 = readD();
        unk9 = readD();
        unk10 = readD(); */

        unkLast = readD();

        if (getAvaliableBytes() == 2)
            unk13 = readC();
        else
            log.warn("Hoppa, valami mas maradt itt: " + getAvaliableBytes());

        skipAll();
    }

    @Override
    protected void runImpl() {

    }
}
