package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHAR_ENUM;
import net.ascnhalf.realm.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * The Class CMSG_CHAR_ENUM.
 */
@Component
@Scope("prototype")
public class CMSG_CHAR_ENUM extends AbstractWoWClientPacket {

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /**
     * The account service.
     */
    @Autowired
    private AccountService accountService;

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws RuntimeException {
        // nothing to read
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        sender.send(getClient(), new SMSG_CHAR_ENUM(accountService.getCharacters(getAccount().getId())));
    }

}
