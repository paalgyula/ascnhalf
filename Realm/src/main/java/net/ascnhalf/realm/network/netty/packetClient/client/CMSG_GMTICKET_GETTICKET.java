package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_GMTICKET_GETTICKET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.08.
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_GMTICKET_GETTICKET extends AbstractWoWClientPacket {

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Override
    protected void readImpl() throws RuntimeException {
        // No readable bytes
    }

    @Override
    protected void runImpl() {
        // TODO: implement GMTicket manager
        sender.send(getPlayer().getChannel(), new SMSG_GMTICKET_GETTICKET());
    }
}
