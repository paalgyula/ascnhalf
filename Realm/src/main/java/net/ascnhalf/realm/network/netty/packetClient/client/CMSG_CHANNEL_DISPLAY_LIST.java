package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 21:48
 */
@Component
@Scope("prototype")
public class CMSG_CHANNEL_DISPLAY_LIST extends AbstractWoWClientPacket {
    @Autowired
    ChatService chatService;

    private String channelName;

    @Override
    protected void readImpl() throws RuntimeException {
        channelName = readS(); // Channel Name
    }

    @Override
    protected void runImpl() {
        // Sending member list if he's on channel
        chatService.getChannel(channelName).sendMemberList(getPlayer());
    }
}
