/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetAuth.client;

import net.ascnhalf.commons.model.WoWAuthResponse;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetAuth.AbstractRealmClientPacket;
import net.ascnhalf.realm.network.netty.packetAuth.server.SCMD_AUTH_ENABLE_CRYPT;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.nio.BufferUnderflowException;

/**
 * The Class <tt>CMD_AUTH_LOGON_PROOF</tt>.
 */
@Service
@Scope("prototype")
public class CMD_AUTH_LOGON_PROOF extends AbstractRealmClientPacket {

    /**
     * The logger.
     */
    private static Logger logger = Logger.getLogger(CMD_AUTH_LOGON_PROOF.class);

    @Autowired
    @Qualifier("RealmToAuth")
    private AbstractPacketSender sender;

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        if (readC() == WoWAuthResponse.WOW_SUCCESS.getMessageId()) {
            logger.debug("CMD_AUTH_LOGON_PROOF succes");
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        sender.send(getClient(), new SCMD_AUTH_ENABLE_CRYPT());

    }
}
