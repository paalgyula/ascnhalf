package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_LOGOUT_COMPLETE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.08.12.
 * Time: 17:31
 */
@Component
@Scope("prototype")
public class CMSG_LOGOUT_REQUEST extends AbstractWoWClientPacket {

    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    @Autowired
    private SMSG_LOGOUT_COMPLETE logoutPacket;

    @Override
    protected void readImpl() throws RuntimeException {
        skipAll();
    }

    @Override
    protected void runImpl() {
        // TODO: implement SMSG_LOGOUT_RESPONSE
        logoutPacket.setPlayer(getPlayer());
        sender.send(getPlayer().getChannel(), logoutPacket);
    }
}
