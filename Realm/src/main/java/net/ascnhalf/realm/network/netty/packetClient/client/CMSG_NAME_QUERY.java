package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_NAME_QUERY_RESPONSE;
import net.ascnhalf.realm.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * The Class CMSG_NAME_QUERY.
 */
@Component
@Scope("prototype")
public class CMSG_NAME_QUERY extends AbstractWoWClientPacket {

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /**
     * The guid.
     */
    private long guid;

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        guid = readQ();
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        /**
         *  FIXME send from database information too
         *  need cache
         */

        if (PlayerService.getPlayer(guid) != null) {
            sender.send(getClient(), new SMSG_NAME_QUERY_RESPONSE(PlayerService
                    .getPlayer(guid)));
        }

    }
}
