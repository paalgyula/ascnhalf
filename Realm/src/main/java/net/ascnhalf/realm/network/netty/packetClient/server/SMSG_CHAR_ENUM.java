/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.realm.model.InventoryItem;
import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.item.ItemPrototype;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import net.ascnhalf.realm.service.ItemStorages;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * The Class SMSG_CHAR_ENUM.
 */
public class SMSG_CHAR_ENUM extends AbstractWoWServerPacket {

    /**
     * The charlist.
     */
    private List<CharacterData> charlist;

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger.getLogger(SMSG_CHAR_ENUM.class);

    /**
     * Instantiates a new <tt>SMSG_CHAR_ENUM</tt> packet.
     *
     * @param charlist the charlist
     */
    public SMSG_CHAR_ENUM(List<CharacterData> charlist) {
        this.charlist = charlist;
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.SendablePacket#writeImpl()
     */
    @Override
    public void writeImpl() {
        logger.info("Character list size: " + charlist.size());
        writeC(charlist.size());
        for (CharacterData character : charlist) {
            writeQ(character.getGuid());
            writeS(character.getName());
            writeC((byte) character.getRace().getValue());
            writeC((byte) character.getClazz().getValue());
            writeC(character.getGender());
            int playerBytes = character.getPlayerBytes();
            writeC(playerBytes & 0xFF);
            writeC((playerBytes >> 8) & 0xFF);
            writeC((playerBytes >> 16) & 0xFF);
            writeC((playerBytes >> 24) & 0xFF);
            writeC(character.getPlayerBytes2() & 0xFF);
            writeC(character.getLevel());
            writeD(character.getZone());
            writeD(character.getMap());
            writeF(character.getPositionX());
            writeF(character.getPositionY());
            writeF(character.getPositionY());
            // TODO: implement guild
            writeD(-1);
            // Ban, dead, help, cloak, rename values. default: no flags
            writeD(0);
            writeD(character.getAtLoginFlags());

            writeC(0); // FIXME check at login first

            // TODO: implement Pet!
            writeD(0x00 /*character.getPetDisplayId()*/);
            writeD(0x00 /*character.getPetLevel()*/);
            writeD(0x00 /*character.getPetFamily()*/);

            ItemStorages itemStorages = ServiceContent.getContext().getBean(ItemStorages.class);
            if (itemStorages == null)
                logger.fatal("Cannot get ItemStorages instance!");

            // 19 inventory slot
            for (int i = 0; i < 19; i++) {
                InventoryItem invItem = character.getItemFromSlot(i);
                if (invItem != null) {
                    try {
                        ItemPrototype itemPrototype = itemStorages.get(invItem.getItem_guid());
                        if (itemPrototype == null)
                            throw new Exception();

                        writeD(itemPrototype.getDisplayInfoID());
                        writeC(itemPrototype.getInventoryType() - 1);
                        writeD(0x00 /* paalgyula: need some info about it character.getItems()[i].getEnchantAuraId()*/);
                    } catch (Exception e) {
                        logger.fatal("ID not found in the storage: " + invItem.getItem_guid(), e);
                        writeD(0x00);
                        writeC(0x00);
                        writeD(0x00);
                    }

                } else {
                    writeD(0x00);
                    writeC(0x00);
                    writeD(0x00);
                }
            }

            // TODO: implement bags
            for (int i = 0; i < 4; i++) {
                writeD(0); // DisplayID
                writeC(0); // InventoryType
                writeD(0); // Enchantment
            }
        }

    }

}
