/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

// TODO: Auto-generated Javadoc

/**
 * The Class SMSG_INIT_WORLD_STATES.
 */
public class SMSG_INIT_WORLD_STATES extends AbstractWoWServerPacket {

    /**
     * The player.
     */
    private Player player;

    /**
     * Instantiates a new sMS g_ ini t_ worl d_ states.
     */
    public SMSG_INIT_WORLD_STATES() {
    }

    /**
     * Instantiates a new sMS g_ ini t_ worl d_ states.
     *
     * @param player the player
     */
    public SMSG_INIT_WORLD_STATES(Player player) {
        this.player = player;
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.SendablePacket#writeImpl()
     */
    @Override
    protected void writeImpl() {
        /*writeD(player.getCharacterData().getMap() );
        writeD(player.getCharacterData().getZone() );
        writeD( 0xDD );*/ // TODO: Area research

        /*
        HM_NAMESPACE::hash_map< uint32, HM_NAMESPACE::hash_map< uint32, uint32 > >::const_iterator itr
		= worldstates.find( zone );

        data << uint16( 2 + itr->second.size() );

		for( HM_NAMESPACE::hash_map< uint32, uint32 >::const_iterator itr2 = itr->second.begin(); itr2 != itr->second.end(); ++itr2 ){
			data << uint32( itr2->first );
			data << uint32( itr2->second );
		}
         */

        /*
        writeH( 0x02 ); // END

        writeD( 3191 );
        writeD( 0x03 );
        writeD( 3901 );
        writeD( 0x01 );*/
        writeB(new byte[]{
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x0c, (byte) 0x00, (byte) 0x00, (byte) 0x00,
                (byte) 0x0c, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x10, (byte) 0x00, (byte) 0xd8, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd7, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd6, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd5, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd4, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd3, (byte) 0x08,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x77, (byte) 0x0c,
                (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x3d, (byte) 0x0f,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xd9, (byte) 0x0e,
                (byte) 0x00, (byte) 0x00, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x02, (byte) 0x11,
                (byte) 0x00, (byte) 0x00, (byte) 0xd3, (byte) 0x40, (byte) 0x52, (byte) 0x50, (byte) 0x95, (byte) 0x07,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x96, (byte) 0x07,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x97, (byte) 0x07,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x23, (byte) 0x0b,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x6f, (byte) 0x0e,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xb1, (byte) 0x10,
                (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,
        });
    }
}
