package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * The Class CMSG_VOICE_SESSION_ENABLE.
 */
@Component
@Scope("prototype")
public class CMSG_VOICE_SESSION_ENABLE extends AbstractWoWClientPacket {

    /* (non-Javadoc)
     * @see ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        // TODO this need ??
        skipAll();
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {

    }

}
