package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.model.player.chat.ChatColor;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.19.
 * Time: 0:31
 */
@Component
@Scope("prototype")
public class CMSG_AREATRIGGER extends AbstractWoWClientPacket {
    int areaTrigger;

    @Override
    protected void readImpl() throws RuntimeException {
        areaTrigger = readD();
    }

    @Override
    protected void runImpl() {
        if (getPlayer().getAccount().getAccessLevel() >= 2)
            getPlayer().sendBroadcastMessage(
                    "[" + ChatColor.MSG_COLOR_CYAN.getValue() + "AreaTrigger|r] Entered AreaTrigger: " + areaTrigger
            );
    }
}
