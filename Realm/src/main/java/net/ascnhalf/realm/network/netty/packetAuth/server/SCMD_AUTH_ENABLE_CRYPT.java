package net.ascnhalf.realm.network.netty.packetAuth.server;

import net.ascnhalf.realm.network.netty.packetAuth.AbstractRealmServerPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class <tt>CMD_AUTH_ENABLE_CRYPT</tt>.
 */
@Service
@Scope("prototype")
public class SCMD_AUTH_ENABLE_CRYPT extends AbstractRealmServerPacket {

    public SCMD_AUTH_ENABLE_CRYPT() {
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.network.model.SendablePacket#writeImpl()
     */
    @Override
    protected void writeImpl() {

    }
}

