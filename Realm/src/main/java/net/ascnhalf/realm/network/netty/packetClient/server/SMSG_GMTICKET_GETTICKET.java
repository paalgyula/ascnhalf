package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.08.
 * Time: 23:19
 */
public class SMSG_GMTICKET_GETTICKET extends AbstractWoWServerPacket {
    private String message = "";
    private int status = 0x0A; // Standard no ticket

    public SMSG_GMTICKET_GETTICKET() {
    }

    public SMSG_GMTICKET_GETTICKET(String message) {
        this.message = message;
        status = 0x06; // Text present
    }

    @Override
    protected void writeImpl() {
        // size: (4 + len + 1 + 4 + 2 + 4 + 4)
        writeD(status);
        if (status == 0x06) {
            writeD(0x12); // unknown
            writeS(message);
            writeC(0x07); // Ticket category
            writeF(0.0f); // Queue
            writeF(0.0f); // if > "tickets in queue" then "We are currently experiencing a high volume of petitions."
            writeF(0.0f); // 0 - "Your ticket will be serviced soon", 1 - "Wait time currently unavailable"
            writeC(0x00); // if == 2 and next field == 1 then "Your ticket has been escalated"
            writeC(0x00); // const
        } else {
            writeD(0x00); // unknown
            writeC(0x00);
            writeF(0.0f); // Queue
            writeF(0.0f); // if > "tickets in queue" then "We are currently experiencing a high volume of petitions."
            writeF(0.0f); // 0 - "Your ticket will be serviced soon", 1 - "Wait time currently unavailable"
            writeC(0x00); // if == 2 and next field == 1 then "Your ticket has been escalated"
            writeC(0x00); // const
        }
    }
}
