package net.ascnhalf.realm.network.netty.packetClient.client

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_QUEST_QUERY_RESPONSE
import net.ascnhalf.realm.service.QuestStorages
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

import java.nio.BufferUnderflowException

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2012.08.08.
 * Time: 14:41
 */
@Component
@Scope("prototype")
public class CMSG_QUEST_QUERY extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(CMSG_QUEST_QUERY.class);

    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    @Autowired
    private QuestStorages questStorages;

    private def questProrotype = null;

    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        Integer questId = readD();
        log.info("Quest query received (#" + questId + ")");
        skipAll();

        questProrotype = questStorages.getQuest(questId);
    }

    @Override
    protected void runImpl() {
        if (questProrotype == null)
            return;

        def questResponse = new SMSG_QUEST_QUERY_RESPONSE()
        questResponse.setQuest questProrotype

        sender.send player.channel, questResponse
    }
}
