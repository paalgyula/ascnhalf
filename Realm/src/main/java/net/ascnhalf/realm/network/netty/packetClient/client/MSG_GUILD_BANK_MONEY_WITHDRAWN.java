package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 0:50
 */
@Component
@Scope("prototype")
public class MSG_GUILD_BANK_MONEY_WITHDRAWN extends AbstractWoWClientPacket {

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        // Skip, no info in this packet
    }

    @Override
    protected void runImpl() {
        sender.send(getPlayer().getChannel(), new net.ascnhalf.realm.network.netty.packetClient.server.MSG_GUILD_BANK_MONEY_WITHDRAWN());
    }
}
