package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 0:10
 */
// TODO: implement battlefield status
@Component
@Scope("prototype")
public class CMSG_BATTLEFIELD_STATUS extends AbstractWoWClientPacket {
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
    }

    @Override
    protected void runImpl() {
    }
}
