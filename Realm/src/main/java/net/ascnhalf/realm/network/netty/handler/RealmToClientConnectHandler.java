/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.handler;

import net.ascnhalf.commons.network.model.ConnectHandler;
import net.ascnhalf.commons.network.model.NettyNetworkChannel;
import net.ascnhalf.commons.network.model.State;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_AUTH_CHALLENGE;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.ChannelHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class RealmToClientConnectHandler.
 */
@Service
@Qualifier("RealmToClient")
@Scope("singleton")
public class RealmToClientConnectHandler implements ConnectHandler {

    /**
     * The Constant log.
     */
    private static final Logger log = Logger
            .getLogger(RealmToClientConnectHandler.class);

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onConnect(net.ascnhalf.commons.network.model.NettyNetworkChannel, org.jboss.netty.channel.ChannelHandler)
     */
    @Override
    public void onConnect(NettyNetworkChannel networkChannel,
                          ChannelHandler handler) {
        networkChannel.setChannelState(State.CONNECTED);
        log.info("Accepting connection from: " + networkChannel.getAddress());
        sender.send(networkChannel, new SMSG_AUTH_CHALLENGE());
    }

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onDisconnect(net.ascnhalf.commons.network.model.NettyNetworkChannel)
     */
    @Override
    public void onDisconnect(NettyNetworkChannel networkChannel) {
        log.info("Disconnection : " + networkChannel.getAddress().getHostName());

    }

}
