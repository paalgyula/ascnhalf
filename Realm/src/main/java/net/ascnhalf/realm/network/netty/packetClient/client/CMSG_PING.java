package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_PONG;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * The Class CMSG_PING.
 */
@Component
@Scope("prototype")
public class CMSG_PING extends AbstractWoWClientPacket {

    /**
     * The Constant logger.
     */
    private static final Logger logger = Logger
            .getLogger(CMSG_PING.class);

    /**
     * The ping.
     */
    private int ping;

    /**
     * The latency.
     */
    private int latency;

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        ping = readD();
        latency = readD();

        if (GetChannelHandler().getLastPingTime() < 0) {
            GetChannelHandler().setLastPingTime(System.currentTimeMillis());
        } else {
            // FIXME flood protection need here??
        }
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        sender.send(getClient(), new SMSG_PONG(ping));

    }

}
