package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.20.
 * Time: 14:12
 */
public class SMSG_CHAR_DELETE extends AbstractWoWServerPacket {
    private boolean success = true;

    /*
     E_CHAR_DELETE_IN_PROGRESS								= 0x46,
    E_CHAR_DELETE_SUCCESS									= 0x47,
    E_CHAR_DELETE_FAILED									= 0x48,
    E_CHAR_DELETE_FAILED_LOCKED_FOR_TRANSFER				= 0x49,
    E_CHAR_DELETE_FAILED_GUILD_LEADER						= 0x4A,
    E_CHAR_DELETE_FAILED_ARENA_CAPTAIN						= 0x4B,
     */

    public SMSG_CHAR_DELETE(boolean success) {
        this.success = success;
    }

    public SMSG_CHAR_DELETE() {
    }

    @Override
    protected void writeImpl() {
        if (success)
            writeC(0x47);
        else
            writeC(0x48);
    }
}
