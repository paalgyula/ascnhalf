package net.ascnhalf.realm.network.netty.handler;

import net.ascnhalf.commons.network.model.ConnectHandler;
import net.ascnhalf.commons.network.model.NettyNetworkChannel;
import net.ascnhalf.commons.network.model.State;
import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetAuth.server.SCMD_AUTH_LOGON_CHALLENGE;
import org.apache.log4j.Logger;
import org.jboss.netty.channel.ChannelHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The Class R2LConnectHandler.
 */
public class RealmToAuthConnectHandler implements ConnectHandler {

    /**
     * The Constant log.
     */
    private static final Logger log = Logger
            .getLogger(RealmToAuthConnectHandler.class);

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("RealmToAuth")
    private AbstractPacketSender sender;

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onConnect(NettyNetworkChannel, ChannelHandler);
     */
    @Override
    public void onConnect(NettyNetworkChannel networkChannel, ChannelHandler handler) {
        networkChannel.setChannelState(State.CONNECTED);
        log.info("Connection to: " + networkChannel.getAddress());
        sender.send(networkChannel, new SCMD_AUTH_LOGON_CHALLENGE());
    }

    /**
     * (non-Javadoc)
     *
     * @see net.ascnhalf.commons.network.model.ConnectHandler#onDisconnect(NettyNetworkChannel)
     */
    @Override
    public void onDisconnect(NettyNetworkChannel networkChannel) {
        log.info("Disconnection : " + networkChannel.getAddress().getHostName());

    }

}
