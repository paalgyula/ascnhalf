package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.base.update.UnitField;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.08.30.
 * Time: 23:48
 * To change this template use File | Settings | File Templates.
 */
public class SMSG_STANDSTATE_UPDATE extends AbstractWoWServerPacket {
    int standState = 0;

    public SMSG_STANDSTATE_UPDATE(int standState) {
        this.standState = standState;
    }

    public SMSG_STANDSTATE_UPDATE() {
    }

    @Override
    protected void writeImpl() {
        getPlayer().SetByteValue(UnitField.UNIT_FIELD_BYTES_0, 0, (byte) standState);
        writeC(standState);
    }
}
