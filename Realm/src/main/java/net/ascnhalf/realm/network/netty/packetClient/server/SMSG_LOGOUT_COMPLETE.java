package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.model.player.chat.ChatChannel;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import net.ascnhalf.realm.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.08.12.
 * Time: 17:33
 */
@Component
@Scope("prototype")
public class SMSG_LOGOUT_COMPLETE extends AbstractWoWServerPacket {
    @Autowired
    private ChatService chatService;

    @Autowired
    PlayerDAO playerDAO;

    private Player player;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * default constructor!
     */
    public SMSG_LOGOUT_COMPLETE() {
    }

    /**
     * Logouts player and leaves all chat channels
     *
     * @param player
     */
    public SMSG_LOGOUT_COMPLETE(Player player) {
        this.player = player;
    }

    @Override
    protected void writeImpl() {
        chatService = ServiceContent.getContext().getBean(ChatService.class);
        for (Object cObj : chatService.getChannels().values()) {
            ((ChatChannel) cObj).leave(getPlayer());
        }

        playerDAO.saveCharacterData(getPlayer().collectCharacterData());
    }
}
