package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.player.chat.ChatMember;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.15.
 * Time: 21:52
 */
public class SMSG_CHANNEL_LIST extends AbstractWoWServerPacket {

    private String channelName;
    private Set<ChatMember> members;

    /**
     * Don't use default constructor!
     */
    @Deprecated
    public SMSG_CHANNEL_LIST() {
    }

    /**
     * Sends channel member list to the client
     *
     * @param channelName specified channel name
     * @param members     Set of {@link ChatMember}
     */
    public SMSG_CHANNEL_LIST(String channelName, Set<ChatMember> members) {
        this.channelName = channelName;
        this.members = members;
    }

    @Override
    protected void writeImpl() {
        writeC(0x01); // Channel type
        writeS(channelName);
        writeC(0x00); // Channel flags
        writeD(members.size());

        for (ChatMember member : members) {
            writeQ(member.getPlayer().getObjectGuid().getRawValue());
            writeC(member.flagToInt());
        }
    }
}
