package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.UpdateType;
import net.ascnhalf.realm.model.base.WorldObject;
import net.ascnhalf.realm.model.base.guid.TypeId;
import net.ascnhalf.realm.model.base.item.Item;
import net.ascnhalf.realm.model.base.update.UpdateFieldUtils;
import net.ascnhalf.realm.model.base.update.UpdateFlags;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.model.unit.MovementFlags;
import net.ascnhalf.realm.model.unit.Units;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

import java.util.BitSet;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.09.
 * Time: 0:07
 * To change this template use File | Settings | File Templates.
 */
public class SMSG_UPDATE_OBJECT extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(getClass());

    private WorldObject object;
    private UpdateType updateType = UpdateType.VALUES;

    public SMSG_UPDATE_OBJECT() {
    }

    ;

    public SMSG_UPDATE_OBJECT(WorldObject object, UpdateType updateType) {
        this.object = object;
        this.updateType = updateType;
    }

    @Override
    protected void writeImpl() {
        writeD(1); // Commands

        switch (object.getObjectTypeId()) {
            case TYPEID_PLAYER:
                updatePlayer(updateType);
                break;
            case TYPEID_ITEM:
                updateItem(updateType);
                break;
            default:
                log.fatal("Object type update not implemented");
        }

    }

    void buildMovementUpdate(int updateFlags) {
        writeH(updateFlags); // movement flags

        if ((updateFlags & UpdateFlags.LIVING.getValue()) > 0) {
            if (object.getObjectGuid().GetTypeId() == TypeId.TYPEID_PLAYER) {
                Player player = (Player) object;

                if (player.getTransport() != null)
                    player.m_movementInfo.flags |= MovementFlags.MOVEFLAG_ONTRANSPORT.getValue();
                //TODO:imlement remove flag
                // else
                //player.m_movementInfo.flags |= MovementFlags.MOVEFLAG_ONTRANSPORT.getValue();
            }
            Units unit = (Units) object;

            // Updating movement time
            unit.m_movementInfo.updateTime();

            //player.m_movementInfo.flags |= ( 0x00002000 | 0x04000000 | 0x08000000 | 0x01 ); // Move forward ?

            writeD(unit.m_movementInfo.flags);
            writeH(unit.m_movementInfo.unk_230);
            writeD(unit.m_movementInfo.time);

            writeF(unit.m_movementInfo.x);
            writeF(unit.m_movementInfo.y);
            writeF(unit.m_movementInfo.z);
            writeF(unit.m_movementInfo.orientation);

            // TODO: implement extended movement data
            writeD(0x00);

            // Unit speeds
            writeF(4.7f /*unit->GetSpeed(MOVE_WALK)*/);
            writeF(7.1f /*unit->GetSpeed(MOVE_RUN)*/);
            writeF(5.0f /*unit->GetSpeed(MOVE_RUN_BACK)*/);
            writeF(4.722222f /*unit->GetSpeed(MOVE_SWIM)*/);
            writeF(4.722222f /*unit->GetSpeed(MOVE_SWIM_BACK)*/);
            writeF(7.0f /*unit->GetSpeed(MOVE_FLIGHT)*/);
            writeF(6.0f /*unit->GetSpeed(MOVE_FLIGHT_BACK)*/);
            writeF(2.0f /*unit->GetSpeed(MOVE_TURN_RATE)*/);
            writeF(2.0f /*unit->GetSpeed(MOVE_PITCH_RATE)*/);

            // 0x08000000
            /*if (unit->m_movementInfo.GetMovementFlags() & MOVEFLAG_SPLINE_ENABLED)
                Movement::PacketBuilder::WriteCreate(*unit->movespline, *data); */
        } else { /* NOT LIVING */
            if ((updateFlags & UpdateFlags.HAS_POSITION.getValue()) > 0) {
                if ((updateFlags & 0x02) > 0) { // Transport
                    writeF(0.0f);
                    writeF(0.0f);
                    writeF(0.0f);
                    writeF(0.0f);
                } else {
                    writeF(getPlayer().getCharacterData().getPositionX());
                    writeF(getPlayer().getCharacterData().getPositionY());
                    writeF(getPlayer().getCharacterData().getPositionZ());
                    writeF(getPlayer().getCharacterData().getOrientation());
                }
            }
        }

        if ((updateFlags & UpdateFlags.HIGHGUID.getValue()) > 0) {
            switch (object.getObjectTypeId()) {
                case TYPEID_OBJECT:
                case TYPEID_ITEM:
                case TYPEID_CONTAINER:
                case TYPEID_GAMEOBJECT:
                case TYPEID_DYNAMICOBJECT:
                case TYPEID_CORPSE:
                    writeD(object.getObjectGuid().getHigh().getValue()); // GetGUIDHigh()
                    break;
                case TYPEID_UNIT:
                    writeD(0x0000000B);                // unk, can be 0xB or 0xC
                    break;
                case TYPEID_PLAYER:
                    if ((updateFlags & UpdateFlags.SELF.getValue()) > 0)
                        writeD(0x0000002F);            // unk, can be 0x15 or 0x22
                    else
                        writeD(0x00000008);            // unk, can be 0x7 or 0x8
                    break;
                default:
                    writeD(0x00000000);                // unk
                    break;
            }
        }

    }

    private void updateItem(UpdateType updateType) {
        Item item = (Item) object;

        if (updateType == UpdateType.CREATE_OBJECT) {
            writeB(item.getObjectGuid().pack());
            writeC(TypeId.TYPEID_ITEM.getValue());
        }

        buildMovementUpdate(0x00 | UpdateFlags.HIGHGUID.getValue());
        updateValues();
    }

    private void updatePlayer(UpdateType updateType) {
        Player player = (Player) object;

        if ((updateType == UpdateType.CREATE_SELF) || (updateType == UpdateType.CREATE_OBJECT)) {
            // Dynamic object, corpse, player
            if (player == getPlayer())
                updateType = UpdateType.CREATE_SELF;

            writeC(updateType.getValue()); // Update type
            writeB(player.getObjectGuid().pack()); // Packet wow guidW
            writeC(TypeId.TYPEID_PLAYER.getValue()); // ObjectType

            player.m_updateFlag |= UpdateFlags.SELF.getValue();
            int updateFlags = player.m_updateFlag;

            buildMovementUpdate(updateFlags);
        } else {
            writeC(updateType.getValue()); // Update type
            writePackedGuid(player.getObjectGuid().getRawValue()); // Packed wow guid (uint64)
        }

        updateValues();
        log.info("Update complete. packets cleared.");
    }

    public void updateValues() {
        BitSet bits = object.getBitSet();
        int mod = bits.toByteArray().length % 4;

        int blocks = bits.toByteArray().length / 4;

        if (mod > 0)
            blocks = (bits.toByteArray().length + (4 - mod)) / 4;

        writeC(blocks); // UpdateValues pakcet size
        writeB(bits.toByteArray()); // UpdateValues bitmask

        if (mod > 0)
            writeB(new byte[4 - mod]);

        for (int i = 0; i < bits.length(); i++) {
            if (!bits.get(i))
                continue;

            log.info("Updating object: " + UpdateFieldUtils.getField(i, object.getObjectGuid()) + String.format(" (0x%08x)", object.GetUInt32Value(i)));
            writeD(object.GetUInt32Value(i));
        }
    }
}
