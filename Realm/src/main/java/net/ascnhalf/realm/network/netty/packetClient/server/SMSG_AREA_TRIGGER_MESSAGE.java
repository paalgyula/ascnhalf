package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 12:42
 */
public class SMSG_AREA_TRIGGER_MESSAGE extends AbstractWoWServerPacket {
    private String message;

    public SMSG_AREA_TRIGGER_MESSAGE(String message) {
        this.message = message;
    }

    @Override
    protected void writeImpl() {
        writeD(0x00);
        writeS(message);
    }
}
