package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * The Class CMSG_SET_ACTIONBAR_TOGGLES.
 */
@Component
@Scope("prototype")
public class CMSG_SET_ACTIONBAR_TOGGLES extends AbstractWoWClientPacket {

    /**
     * The action bar.
     */
    byte actionBar;

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        actionBar = (byte) readC();
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        //FIXME need complete stats
        if (getPlayer() != null) {
            //	getPlayer().setByteValue()
        }

    }


}
