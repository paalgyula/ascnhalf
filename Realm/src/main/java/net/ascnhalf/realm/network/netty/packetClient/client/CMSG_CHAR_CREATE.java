package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHAR_CREATE;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.07.
 * Time: 23:26
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_CHAR_CREATE extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass());

    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    @Autowired
    PlayerDAO playerDAO;

    String charName;
    Integer charRace;
    Integer charClass;
    Integer gender, skin, face, hairStyle, hairColor, facialHair, outfitId;

    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        charName = readS();
        charRace = readC();
        charClass = readC();

        gender = readC();
        skin = readC();
        face = readC();
        hairStyle = readC();
        hairColor = readC();
        facialHair = readC();
        outfitId = readC();

        log.info(String.format("PlayerCreate: [%s] Race: %d, Class: %d", charName, charRace, charClass));
        skipAll();
    }

    @Override
    protected void runImpl() {
        sender.send(getClient(), new SMSG_CHAR_CREATE(playerDAO.createCharacter(getAccount().getId(), charName, charClass, charRace, gender, skin, face, hairStyle, hairColor, facialHair, outfitId)));
    }
}
