package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.model.base.account.AccountDataType;
import net.ascnhalf.realm.model.base.character.CharacterAccountData;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_UPDATE_ACCOUNT_DATA_COMPLETE;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.InflaterOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 7:05
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_UPDATE_ACCOUNT_DATA extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass());
    private int type;

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Autowired
    PlayerDAO playerDAO;

    @Override
    protected void readImpl() throws RuntimeException {
        type = readD();
        int timestamp = readD(), decompressedSize = readD();
        byte[] data = new byte[getAvaliableBytes()];
        data = readB(data.length);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InflaterOutputStream ios = new InflaterOutputStream(baos);
        try {
            ios.write(data);
            ios.finish();
        } catch (IOException e) {
            log.fatal("CMSG_UPDATE_ACCOUNT_DATA decompress failed!");
            skipAll();
            return;
        }

        if (getPlayer() == null) {
            // TODO: account data implement
        } else {
            CharacterAccountData accountData = new CharacterAccountData();

            accountData.setData(baos.toString());
            accountData.setTime(timestamp);
            accountData.setGuid((int) getPlayer().getCharacterData().getGuid());
            accountData.setType(AccountDataType.getByValue(type));

            playerDAO.saveAccountData(accountData);
        }
    }

    @Override
    protected void runImpl() {
        // Notify client
        sender.send(getClient(), new SMSG_UPDATE_ACCOUNT_DATA_COMPLETE(type));
    }
}
