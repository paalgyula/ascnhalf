package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 20:43
 */
public class SMSG_LEARN_SPELL extends AbstractWoWServerPacket {
    private int spellId;

    public SMSG_LEARN_SPELL() {
    }

    public SMSG_LEARN_SPELL(int spellId) {
        this.spellId = spellId;
    }

    @Override
    protected void writeImpl() {
        writeD(spellId);
        writeH(0x00);
    }
}
