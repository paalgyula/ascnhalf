package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_ITEM_QUERY_SINGLE_RESPONSE;
import net.ascnhalf.realm.service.ItemStorages;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.BufferUnderflowException;

/**
 * The Class CMSG_ITEM_QUERY_SINGLE.
 */
@Component
@Scope("prototype")
public class CMSG_ITEM_QUERY_SINGLE extends AbstractWoWClientPacket {

    /**
     * The item.
     */
    private int item;

    /**
     * The sender.
     */
    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    /**
     * The item storages.
     */
    @Autowired
    private ItemStorages itemStorages;

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#readImpl()
     */
    @Override
    protected void readImpl() throws BufferUnderflowException, RuntimeException {
        item = readD();

    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.ReceivablePacket#runImpl()
     */
    @Override
    protected void runImpl() {
        Logger.getLogger(getClass()).info("User item query for id: " + item);
        sender.send(getClient(), new SMSG_ITEM_QUERY_SINGLE_RESPONSE(itemStorages.get(item)));
    }

}
