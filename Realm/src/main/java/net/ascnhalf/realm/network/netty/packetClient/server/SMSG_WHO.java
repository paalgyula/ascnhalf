package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import net.ascnhalf.realm.service.PlayerService;

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.09.22.
 * Time: 18:22
 * To change this template use File | Settings | File Templates.
 */
public class SMSG_WHO extends AbstractWoWServerPacket {
    @Override
    protected void writeImpl() {
        writeD(PlayerService.getPlayerlist().size());
        writeD(PlayerService.getPlayerlist().size());

        for (Object obj : PlayerService.getPlayerlist().values()) {
            Player player = (Player) obj;
            writeS(player.getName());
            writeS("");
            writeD(player.getCharacterData().getLevel());
            writeD(player.getCharacterData().getClazz().getValue());
            writeD(player.getCharacterData().getRace().getValue());
            writeC(player.getCharacterData().getGender());
            writeD(player.getCharacterData().getZone()); // zone TODO: research
        }
    }
}
