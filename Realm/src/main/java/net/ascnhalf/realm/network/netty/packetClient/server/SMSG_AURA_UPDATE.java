package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2012.09.02.
 * Time: 5:36
 */
// TODO implement it
public class SMSG_AURA_UPDATE extends AbstractWoWServerPacket {
    private int auraSlot;
    private int spellId;
    private int auraFlags;
    private int auraLevel;
    private int stackCount;

    private boolean remove = false;

    /**
     * Default no arg constructor. DO NOT USE!
     */
    @Deprecated
    public SMSG_AURA_UPDATE() {
    }

    /**
     * Sends Aura Update packet
     *
     * @param auraSlot
     * @param spellId
     * @param auraFlags
     * @param auraLevel
     * @param stackCount
     */
    public SMSG_AURA_UPDATE(int auraSlot, int spellId, int auraFlags, int auraLevel, int stackCount) {
        this.stackCount = stackCount;
        this.auraSlot = auraSlot;
        this.spellId = spellId;
        this.auraFlags = auraFlags;
        this.auraLevel = auraLevel;
    }

    /**
     * Removes aura by Slot ID
     *
     * @param auraSlot
     */
    public SMSG_AURA_UPDATE(int auraSlot) {
        this.remove = true;
        this.auraSlot = auraSlot;
    }

    @Override
    protected void writeImpl() {
        if (remove) {
            writeB(getPlayer().getObjectGuid().pack());

            writeC(auraSlot);
            writeD(spellId);

            writeC(auraFlags);
            writeC(auraLevel);
            writeC(stackCount);
        } else {
            // Remove aura
            writeB(getPlayer().getObjectGuid().pack());
            writeC(auraSlot);
            writeD(0x00);
        }
    }
}
