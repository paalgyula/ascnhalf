package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHAR_DELETE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Receive wowguid, and tries to delete a character
 * <p/>
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.20.
 * Time: 14:02
 */
@Component
@Scope("singleton")
public class CMSG_CHAR_DELETE extends AbstractWoWClientPacket {
    @Autowired
    PlayerDAO playerDAO;

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    private boolean success = false;

    @Override
    protected void readImpl() throws RuntimeException {
        long wowguid = readQ();

        try {
            playerDAO.deleteCharacter(wowguid);
            success = true;
        } catch (Exception e) {
            success = false;
        }
    }

    @Override
    protected void runImpl() {
        sender.send(getClient(), new SMSG_CHAR_DELETE(success));
    }
}
