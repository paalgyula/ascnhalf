package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 20:04
 */
public class SMSG_UPDATE_ACCOUNT_DATA_COMPLETE extends AbstractWoWServerPacket {
    private int type;

    /**
     * Update complete notify
     *
     * @param type - accountDataType
     */
    public SMSG_UPDATE_ACCOUNT_DATA_COMPLETE(int type) {
        this.type = type;
    }

    @Override
    protected void writeImpl() {
        writeD(type);
        writeD(0x00);
    }
}
