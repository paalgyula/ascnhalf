package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.MSG_MOVE_TIME_SKIPPED;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.08.
 * Time: 4:16
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_MOVE_TIME_SKIPPED extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass().getSimpleName());

    private long guid;
    private int time_dif = 0;

    @Autowired
    @Qualifier("client")
    private AbstractPacketSender sender;

    @Override
    protected void readImpl() throws RuntimeException {
        try {
            this.guid = readQ();
            this.time_dif = readD();
            log.info("MOVE_TIME_SKIPPED guid: " + this.guid + " " + this.time_dif);
        } catch (Exception e) {
            log.fatal("CMSG_MOVE_TIME_SKIPPED - Wrong Packet length");
        }
    }

    @Override
    protected void runImpl() {
        sender.send(getPlayer().getChannel(), new MSG_MOVE_TIME_SKIPPED(time_dif));
    }
}
