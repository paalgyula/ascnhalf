/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.unit.Powers;
import net.ascnhalf.realm.model.unit.Units;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

// TODO: Auto-generated Javadoc

/**
 * The Class SMSG_POWER_UPDATE.
 */
public class SMSG_POWER_UPDATE extends AbstractWoWServerPacket {
    Units target;
    Powers powerType;
    int value;

    /**
     * Default constructor
     */
    public SMSG_POWER_UPDATE() {
    }

    /**
     * Sets the specified unit power value
     *
     * @param target    - Units target
     * @param powerType - The power type example: Powers.MANA
     * @param value     - the power value
     */
    public SMSG_POWER_UPDATE(Units target, Powers powerType, int value) {
        this.target = target;
        this.powerType = powerType;
        this.value = value;
    }

    /* (non-Javadoc)
     * @see org.wowemu.common.network.model.SendablePacket#writeImpl()
     */
    @Override
    public void writeImpl() {
        writeB(target.getObjectGuid().pack());
        writeC(this.powerType.getValue());
        writeD(value);
    }

}
