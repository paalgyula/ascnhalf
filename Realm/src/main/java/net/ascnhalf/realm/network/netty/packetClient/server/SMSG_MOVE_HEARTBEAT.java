package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.player.MovementInfo;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.10.16.
 * Time: 23:09
 */
public class SMSG_MOVE_HEARTBEAT extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(getClass());

    MovementInfo movementInfo;
    long wowguid;
    byte[] reaming;

    public SMSG_MOVE_HEARTBEAT(MovementInfo movementInfo, long wowguid, byte[] reaming) {
        this.movementInfo = movementInfo;
        this.wowguid = wowguid;
        this.reaming = reaming;
    }

    @Override
    protected void writeImpl() {
        writePackedGuid(wowguid);

        writeD(movementInfo.flags);
        writeH(movementInfo.unk_230);
        writeD(movementInfo.time);
        writeF(movementInfo.x);
        writeF(movementInfo.y);
        writeF(movementInfo.z);
        writeF(movementInfo.orientation);

        writeB(reaming);

        // Tadaaaam
    }
}
