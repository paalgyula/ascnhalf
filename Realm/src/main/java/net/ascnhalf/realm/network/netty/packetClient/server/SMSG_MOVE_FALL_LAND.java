package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.08.
 * Time: 2:44
 * To change this template use File | Settings | File Templates.
 */
public class SMSG_MOVE_FALL_LAND extends AbstractWoWServerPacket {
    Logger log = Logger.getLogger(getClass());

    @Override
    protected void writeImpl() throws RuntimeException {
        Player player = getPlayer();

        writeB(player.getObjectGuid().pack());

        writeD(player.m_movementInfo.flags);
        writeH(player.m_movementInfo.unk_230);
        writeF(player.m_movementInfo.time);
        writeF(player.m_movementInfo.x);
        writeF(player.m_movementInfo.y);
        writeF(player.m_movementInfo.z);
        writeF(player.m_movementInfo.orientation);
    }
}
