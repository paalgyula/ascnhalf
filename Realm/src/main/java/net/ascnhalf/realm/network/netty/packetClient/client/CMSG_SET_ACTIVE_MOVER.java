package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.08.
 * Time: 23:10
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("prototype")
public class CMSG_SET_ACTIVE_MOVER extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass());

    @Override
    protected void readImpl() throws RuntimeException {
        long guid = readQ(); // Get mover guid

        /*if(_player->GetMover()->GetObjectGuid() != guid) {
            sLog.outError("HandleSetActiveMoverOpcode: incorrect mover guid: mover is %s and should be %s",
                    _player->GetMover()->GetGuidStr().c_str(), guid.GetString().c_str());
            return;
        }*/
        log.info("Mover guid: " + guid);
    }

    @Override
    protected void runImpl() {

    }
}
