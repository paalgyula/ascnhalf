/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.commons.service.ServiceContent;
import net.ascnhalf.realm.dao.AccountDAO;
import net.ascnhalf.realm.model.base.account.AccountData;
import net.ascnhalf.realm.model.base.account.AccountDataType;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.List;

/**
 * The Class SMSG_ACCOUNT_DATA_TIMES.
 */
public class SMSG_ACCOUNT_DATA_TIMES extends AbstractWoWServerPacket {
    AccountDAO accountDAO = ServiceContent.getContext().getBean(AccountDAO.class);

    Logger log = Logger.getLogger(getClass());

    /**
     * The Constant GLOBAL_CACHE_MASK.
     */
    public static final int GLOBAL_CACHE_MASK = 0x15;

    /**
     * The Constant PER_CHARACTER_CACHE_MASK.
     */
    public static final int PER_CHARACTER_CACHE_MASK = 0xEA;

    /**
     * The curmask.
     */
    private int curmask;

    /**
     * The account data.
     */
    private List<? extends AccountData> accountDataList;

    /**
     * Instantiates a new SMSG_ACCOUNT_DATA_TIMES.
     */
    public SMSG_ACCOUNT_DATA_TIMES() {
        this.curmask = GLOBAL_CACHE_MASK;
    }

    /**
     * Instantiates a new SMSG_ACCOUNT_DATA_TIMES.
     *
     * @param mask        the mask
     * @param accountGuid the account guid
     */
    public SMSG_ACCOUNT_DATA_TIMES(int mask, int accountGuid) {
        this.curmask = mask;
        this.accountDataList = accountDAO.getAccountData(accountGuid);
    }

    /**
     * Instantiates a new SMSG_ACCOUNT_DATA_TIMES.
     *
     * @param mask            the mask
     * @param accountDataList the account data
     */
    public SMSG_ACCOUNT_DATA_TIMES(int mask, List<? extends AccountData> accountDataList) {
        this.curmask = mask;
        this.accountDataList = accountDataList;
    }

    /*
      * (non-Javadoc)
      *
      * @see org.wowemu.common.network.model.SendablePacket#writeImpl()
      */
    @Override
    public void writeImpl() {
        writeD(System.currentTimeMillis() / 1000L);
        writeC(1);
        writeD(curmask);
        for (int i = 0; i < AccountDataType.NUM_ACCOUNT_DATA_TYPES.getValue(); ++i)
            if ((curmask & (1 << i)) > 0)
                writeD(getTime(i));
    }

    public int getTime(int accountDataType) {
        Iterator<? extends AccountData> itr = accountDataList.iterator();
        while (itr.hasNext()) {
            AccountData ad = itr.next();
            if (ad.getType().getValue() == accountDataType)
                return ad.getTime();
        }

        return 0x00;
    }
}
