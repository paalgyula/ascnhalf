package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.model.player.chat.ChatChannel;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.service.ChatService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 20:21
 */
@Component
@Scope("prototype")
public class CMSG_JOIN_CHANNEL extends AbstractWoWClientPacket {
    int channelId;
    String channelName, channelPass;
    boolean hasVoice = false, zoneUpdate = false;

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Autowired
    ChatService chatService;

    public final Logger log = Logger.getLogger(getClass());

    @Override
    protected void readImpl() throws RuntimeException {
        channelId = readD();

        readH(); // Voice, channel

        channelName = readS(); // ChannelName
        channelPass = readS();
    }

    @Override
    protected void runImpl() {
        ChatChannel chatChannel = chatService.getChannel(channelName);
        chatChannel.requestJoin(getPlayer(), channelPass);
    }
}
