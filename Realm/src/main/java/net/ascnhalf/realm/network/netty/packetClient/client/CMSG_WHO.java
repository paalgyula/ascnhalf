package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_WHO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: ITEXPERT
 * Date: 2012.09.22.
 * Time: 18:08
 * To change this template use File | Settings | File Templates.
 */
@Component
@Scope("singleton")
public class CMSG_WHO extends AbstractWoWClientPacket {

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Override
    protected void readImpl() throws RuntimeException {

    }

    @Override
    protected void runImpl() {
        sender.send(getClient(), new SMSG_WHO());
    }
}
