package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.09.
 * Time: 6:38
 */
public class SMSG_AURA_UPDATE_ALL extends AbstractWoWServerPacket {
    @Override
    protected void writeImpl() {
        writeB(getPlayer().getObjectGuid().pack());
        writeB(new byte[]{
                (byte) 0x00, (byte) 0x99, (byte) 0x09, (byte) 0x00, (byte) 0x00, (byte) 0x1b,
                (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0xb5, (byte) 0x14, (byte) 0x00, (byte) 0x00, (byte) 0x19,
                (byte) 0x01, (byte) 0x01, (byte) 0x02, (byte) 0xa2, (byte) 0x19, (byte) 0x00, (byte) 0x00, (byte) 0x1b,
                (byte) 0x01, (byte) 0x01, (byte) 0x03, (byte) 0xd7, (byte) 0x7d, (byte) 0x00, (byte) 0x00, (byte) 0x19,
                (byte) 0x01, (byte) 0x01
        });
    }
}
