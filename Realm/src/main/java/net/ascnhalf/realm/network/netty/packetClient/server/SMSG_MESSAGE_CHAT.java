package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.ChatLanguage;
import net.ascnhalf.realm.model.ChatType;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import net.ascnhalf.realm.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.08.
 * Time: 4:33
 * To change this template use File | Settings | File Templates.
 */
public class SMSG_MESSAGE_CHAT extends AbstractWoWServerPacket {

    @Autowired
    ChatService chatService;

    private String message = "";
    private ChatType chatType = ChatType.CHAT_MSG_SYSTEM;
    private ChatLanguage chatLanguage = ChatLanguage.LANG_UNIVERSAL;
    private Player speaker = null;
    private String chatChannel;

    public SMSG_MESSAGE_CHAT() {
    }

    public SMSG_MESSAGE_CHAT(String message) {
        this.message = message;
    }

    public SMSG_MESSAGE_CHAT(String message, ChatType chatType) {
        this.message = message;
        this.chatType = chatType;
    }

    public SMSG_MESSAGE_CHAT(String message, String channel) {
        this.chatChannel = channel;
        this.message = message;
    }

    public SMSG_MESSAGE_CHAT(String message, ChatType chatType, ChatLanguage chatLanguage) {
        this.message = message;
        this.chatType = chatType;
        this.chatLanguage = chatLanguage;
    }

    public void setSpeaker(Player player) {
        this.speaker = player;
    }

    @Override
    protected void writeImpl() {
        if (chatChannel != null) {
            writeC(ChatType.CHAT_MSG_CHANNEL.getValue());
            writeD(0x00); // Lang
            writeQ(speaker != null ? speaker.getObjectGuid().getRawValue() : getPlayer().getObjectGuid().getRawValue());
            writeD(0x00); // ?
            writeS(chatChannel);
            writeQ(speaker != null ? speaker.getObjectGuid().getRawValue() : getPlayer().getObjectGuid().getRawValue());
            writeD(message.length() + 1);
            writeS(message);
            writeC(0x08); // GM flag TODO: implement gm flag
        } else {
            switch (chatType) {
                case CHAT_MSG_MONSTER_SAY:
                case CHAT_MSG_MONSTER_PARTY:
                case CHAT_MSG_MONSTER_YELL:
                case CHAT_MSG_MONSTER_WHISPER:
                case CHAT_MSG_MONSTER_EMOTE:
                case CHAT_MSG_RAID_BOSS_WHISPER:
                case CHAT_MSG_RAID_BOSS_EMOTE:
                case CHAT_MSG_BATTLENET:
                    writeC(chatType.getValue());
                    writeD(0x00); // Lang
                    // Speaker must be set!
                    writeQ(speaker.getObjectGuid().getRawValue());
                    writeD(0x00); // 2.1.0
                    writeD(speaker.getName().length() + 1);
                    writeS(speaker.getName());
                    writeQ(getPlayer().getObjectGuid().getRawValue());
                    // Check if listener isPlayer?!
                    writeD(message.length());
                    writeS(message);
                    writeC(0x00);
                    break;
                default:
                    writeC(chatType.getValue());
                    writeD(0x00); // Lang
                    writeQ(speaker != null ? speaker.getObjectGuid().getRawValue() : getPlayer().getObjectGuid().getRawValue());
                    writeD(0x00); // ?
                    writeQ(speaker != null ? speaker.getObjectGuid().getRawValue() : getPlayer().getObjectGuid().getRawValue());
                    writeD(message.length() + 1);
                    writeS(message);
                    writeC(0x00); // GM flag TODO: implement gm flag
            }

            //Message packet completed
        }
    }
}
