package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 11:41
 */
public class SMSG_CHANNEL_NOTIFY extends AbstractWoWServerPacket {

    Logger log = Logger.getLogger(getClass());

    public static final int CHANNEL_NOTIFY_FLAG_JOINED = 0x00;
    public static final int CHANNEL_NOTIFY_FLAG_LEFT = 0x01;
    public static final int CHANNEL_NOTIFY_FLAG_YOUJOINED = 0x02;
    public static final int CHANNEL_NOTIFY_FLAG_YOULEFT = 0x03;
    public static final int CHANNEL_NOTIFY_FLAG_WRONGPASS = 0x04;
    public static final int CHANNEL_NOTIFY_FLAG_NOTON = 0x05;
    public static final int CHANNEL_NOTIFY_FLAG_NOTMOD = 0x06;
    public static final int CHANNEL_NOTIFY_FLAG_SETPASS = 0x07;
    public static final int CHANNEL_NOTIFY_FLAG_CHGOWNER = 0x08;
    public static final int CHANNEL_NOTIFY_FLAG_NOT_ON_2 = 0x09;
    public static final int CHANNEL_NOTIFY_FLAG_NOT_OWNER = 0x0A;
    public static final int CHANNEL_NOTIFY_FLAG_WHO_OWNER = 0x0B;
    public static final int CHANNEL_NOTIFY_FLAG_MODE_CHG = 0x0C;
    public static final int CHANNEL_NOTIFY_FLAG_ENABLE_ANN = 0x0D;
    public static final int CHANNEL_NOTIFY_FLAG_DISABLE_ANN = 0x0E;
    public static final int CHANNEL_NOTIFY_FLAG_MODERATED = 0x0F;
    public static final int CHANNEL_NOTIFY_FLAG_UNMODERATED = 0x10;
    public static final int CHANNEL_NOTIFY_FLAG_YOUCANTSPEAK = 0x11;
    public static final int CHANNEL_NOTIFY_FLAG_KICKED = 0x12;
    public static final int CHANNEL_NOTIFY_FLAG_YOURBANNED = 0x13;
    public static final int CHANNEL_NOTIFY_FLAG_BANNED = 0x14;
    public static final int CHANNEL_NOTIFY_FLAG_UNBANNED = 0x15;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_1 = 0x16;
    public static final int CHANNEL_NOTIFY_FLAG_ALREADY_ON = 0x17;
    public static final int CHANNEL_NOTIFY_FLAG_INVITED = 0x18;
    public static final int CHANNEL_NOTIFY_FLAG_WRONG_FACT = 0x19;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_2 = 0x1A;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_3 = 0x1B;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_4 = 0x1C;
    public static final int CHANNEL_NOTIFY_FLAG_YOU_INVITED = 0x1D;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_5 = 0x1E;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_6 = 0x1F;
    public static final int CHANNEL_NOTIFY_FLAG_UNK_7 = 0x20;
    public static final int CHANNEL_NOTIFY_FLAG_NOT_IN_LFG = 0x21;
    public static final int CHANNEL_NOTIFY_FLAG_VOICE_ON = 0x22;
    public static final int CHANNEL_NOTIFY_FLAG_VOICE_OFF = 0x23;

    private String channelName;
    private int notifyType;
    private long guid;

    public SMSG_CHANNEL_NOTIFY(String channelName, int notifyType) {
        this.channelName = channelName;
        this.notifyType = notifyType;
    }

    /**
     * Don't Use default constructor!
     */
    @Deprecated
    public SMSG_CHANNEL_NOTIFY() {
    }

    /**
     * Set player guid for join/leave notify
     *
     * @param guid
     */
    public void setGuid(long guid) {
        this.guid = guid;
    }

    @Override
    protected void writeImpl() {
        switch (notifyType) {
            case CHANNEL_NOTIFY_FLAG_YOUJOINED:
                writeC(CHANNEL_NOTIFY_FLAG_YOUJOINED);
                writeS(channelName);
                writeC(0x1A);
                writeD(0x00);
                writeD(0x00);
                break;
            case CHANNEL_NOTIFY_FLAG_NOTON:
            case CHANNEL_NOTIFY_FLAG_WRONGPASS:
            case CHANNEL_NOTIFY_FLAG_VOICE_OFF:
            case CHANNEL_NOTIFY_FLAG_ALREADY_ON:
                writeC(notifyType);
                writeS(channelName);
                break;
            case CHANNEL_NOTIFY_FLAG_SETPASS:
            case CHANNEL_NOTIFY_FLAG_YOU_INVITED:
            case CHANNEL_NOTIFY_FLAG_LEFT:
            case CHANNEL_NOTIFY_FLAG_JOINED:
                writeC(notifyType);
                writeS(channelName);
                writeQ(guid);
            default:
                writeC(CHANNEL_NOTIFY_FLAG_NOT_ON_2);
                writeS(channelName);
                log.warn("Unimplemented join flag: " + notifyType);
        }
    }
}
