package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.model.ChatLanguage;
import net.ascnhalf.realm.model.ChatType;
import net.ascnhalf.realm.model.player.Player;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_MESSAGE_CHAT;
import net.ascnhalf.realm.service.ChatService;
import net.ascnhalf.realm.service.PlayerService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.11.
 * Time: 21:17
 */
@Component
@Scope("prototype")
public class CMSG_MESSAGECHAT extends AbstractWoWClientPacket {
    Logger log = Logger.getLogger(getClass());

    ChatType messageType;
    ChatLanguage messageLanguage;
    String message;
    String channel;
    String to;

    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Autowired
    ChatService chatService;

    @Autowired
    PlayerService playerService;

    @Override
    protected void readImpl() throws RuntimeException {
        messageType = ChatType.get(readD());
        messageLanguage = ChatLanguage.get(readD());

        log.info("Message received: " + messageType + " Language: " + messageLanguage);

        switch (messageType) {
            case CHAT_MSG_SAY:
            case CHAT_MSG_EMOTE:
            case CHAT_MSG_PARTY:
            case CHAT_MSG_PARTY_LEADER:
            case CHAT_MSG_RAID:
            case CHAT_MSG_RAID_LEADER:
            case CHAT_MSG_RAID_WARNING:
            case CHAT_MSG_GUILD:
            case CHAT_MSG_OFFICER:
            case CHAT_MSG_YELL:
                message = readS();
                break;
            case CHAT_MSG_WHISPER:
                // TODO: implement factions
                to = readS();
                message = readS();

                for (Object obj : PlayerService.getPlayerlist().values()) {
                    Player dstPlayer = (Player) obj;
                    if (dstPlayer.getName().trim().equalsIgnoreCase(to.trim())) {
                        // Sending message
                        SMSG_MESSAGE_CHAT whisper = new SMSG_MESSAGE_CHAT(message, messageType, messageLanguage);
                        whisper.setSpeaker(getPlayer());
                        sender.send(dstPlayer.getChannel(), whisper);

                        // Inform self to message sent.
                        whisper = new SMSG_MESSAGE_CHAT(message, ChatType.CHAT_MSG_WHISPER_INFORM, messageLanguage);
                        whisper.setSpeaker(dstPlayer);
                        sender.send(getPlayer().getChannel(), whisper);

                        message = null;
                        return;
                    }
                }

                getPlayer().sendBroadcastMessage("Player not found: " + to);
                message = null;

                break;
            case CHAT_MSG_CHANNEL:
                channel = readS();
                message = readS();
                chatService.getChannel(channel).say(getPlayer(), message);
                break;
            case CHAT_MSG_AFK:
            case CHAT_MSG_DND:
                break;
            default:
                log.warn("Unimplemented message type: " + messageType);
                skipAll();
        }
    }

    @Override
    protected void runImpl() {
        if (message != null)
            sender.send(getPlayer().getChannel(), new SMSG_MESSAGE_CHAT(message, messageType, messageLanguage));
    }
}
