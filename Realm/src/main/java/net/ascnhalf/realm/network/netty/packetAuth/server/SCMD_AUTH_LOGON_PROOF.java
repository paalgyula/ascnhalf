/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.network.netty.packetAuth.server;


import net.ascnhalf.realm.network.netty.packetAuth.AbstractRealmServerPacket;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * The Class <tt>CMD_AUTH_LOGON_PROOF</tt>.
 */
@Service
@Scope("prototype")
public class SCMD_AUTH_LOGON_PROOF extends AbstractRealmServerPacket {

    /**
     * The m1.
     */
    private byte[] m1;

    /**
     * The a.
     */
    private byte[] a;

    /**
     * Instantiates a new jmangos.
     */
    public SCMD_AUTH_LOGON_PROOF() {
    }

    /**
     * Instantiates a new jmangos.
     *
     * @param a  the a
     * @param m1 the m1
     */
    public SCMD_AUTH_LOGON_PROOF(byte[] a, byte[] m1) {
        this.a = a;
        this.m1 = m1;
    }

    /* (non-Javadoc)
     * @see net.ascnhalf.commons.network.model.SendablePacket#writeImpl()
     */
    @Override
    protected void writeImpl() {
        byte[] crc = new byte[20];
        writeB(a);
        writeB(m1);
        writeB(crc);
        writeH(0);
    }
}

