package net.ascnhalf.realm.network.netty.packetClient.client;

import net.ascnhalf.commons.network.netty.sender.AbstractPacketSender;
import net.ascnhalf.realm.model.player.MovementInfo;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWClientPacket;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_MOVE_HEARTBEAT;
import net.ascnhalf.realm.service.MapService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.16.
 * Time: 6:07
 */
@Component
@Scope("prototype")
public class CMSG_MOVE_HEARTBEAT extends AbstractWoWClientPacket {
    @Autowired
    @Qualifier("client")
    AbstractPacketSender sender;

    @Autowired
    MapService mapService;

    Logger log = Logger.getLogger(getClass());

    byte[] reaming;
    MovementInfo movementInfo;
    long wowguid;

    @Override
    protected void readImpl() {
        wowguid = readG();

        movementInfo = new MovementInfo();
        movementInfo.flags = readD();
        movementInfo.unk_230 = readH();
        movementInfo.time = readD();
        movementInfo.x = readF();
        movementInfo.y = readF();
        movementInfo.z = readF();
        movementInfo.orientation = readF();

        if (wowguid == getPlayer().getObjectGuid().getRawValue())
            getPlayer().m_movementInfo = movementInfo;

        log.warn("Heartbeat packets left: " + getAvaliableBytes());
        reaming = readB(getAvaliableBytes());
    }

    @Override
    protected void runImpl() {
        if (wowguid != getPlayer().getObjectGuid().getRawValue())
            mapService.getMap(getPlayer().GetMapId()).sendToSetExcept(sender, new SMSG_MOVE_HEARTBEAT(movementInfo, wowguid, reaming), getPlayer());
    }
}
