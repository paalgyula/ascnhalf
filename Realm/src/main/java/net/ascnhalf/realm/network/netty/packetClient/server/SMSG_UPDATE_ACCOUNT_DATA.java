package net.ascnhalf.realm.network.netty.packetClient.server;

import net.ascnhalf.realm.model.base.account.AccountData;
import net.ascnhalf.realm.network.netty.packetClient.AbstractWoWServerPacket;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * email: paalgyula@gmail.com
 * Date: 2012.09.05.
 * Time: 1:21
 */
public class SMSG_UPDATE_ACCOUNT_DATA extends AbstractWoWServerPacket {
    private int type;

    public SMSG_UPDATE_ACCOUNT_DATA() {
    }

    public SMSG_UPDATE_ACCOUNT_DATA(int type) {
        this.type = type;
    }

    @Override
    protected void writeImpl() {
        List<? extends AccountData> accountDataList = getPlayer().getCharacterData().getCharacterAccountDataList();
        for (AccountData adata : accountDataList)
            if (adata.getType().getValue() == type) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                DeflaterOutputStream compressor = new DeflaterOutputStream(bos);
                int size = adata.getData().length();
                byte[] dataToCompress = adata.getData().getBytes();

                try {
                    compressor.write(dataToCompress);
                    compressor.finish();
                } catch (IOException e) {
                    Logger.getLogger(getClass().getSimpleName()).fatal("Failed to ZIP packet!");
                    break;
                }

                writeQ(getPlayer().getObjectGuid().getRawValue());
                writeD(adata.getTime());
                writeD(size);
                writeB(bos.toByteArray());
                return;
            }

        // :O
        Logger.getLogger(getClass().getSimpleName()).fatal("No account data found?!");
        writeQ(getPlayer().getObjectGuid().getRawValue());
        writeD(0x00);
        writeD(0x00);
    }
}
