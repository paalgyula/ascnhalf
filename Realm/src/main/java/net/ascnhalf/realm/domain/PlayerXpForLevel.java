package net.ascnhalf.realm.domain;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2012.08.12.
 * Time: 14:31
 */
@Entity
@Table(name = "player_xp_for_level")
public class PlayerXpForLevel {
    @Id
    @Column(name = "lvl", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    private int lvl;

    @Basic
    @Column(name = "xp_for_next_level", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    private int xpForNextLevel;

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getXpForNextLevel() {
        return xpForNextLevel;
    }

    public void setXpForNextLevel(int xpForNextLevel) {
        this.xpForNextLevel = xpForNextLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerXpForLevel that = (PlayerXpForLevel) o;

        if (lvl != that.lvl) return false;
        if (xpForNextLevel != that.xpForNextLevel) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = lvl;
        result = 31 * result + xpForNextLevel;
        return result;
    }
}
