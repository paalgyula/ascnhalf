package net.ascnhalf.realm.utils.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Felhasználó
 * Date: 2013.06.05.
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
public class MemoryUsagePanel extends JPanel {

    private JProgressBar progressBar;

    private synchronized void refreshUsage() {
        //                                                 KB     MB
        int max = (int) Runtime.getRuntime().maxMemory() / 1024 / 1024;

        //                                                    KB     MB
        int used = (int) Runtime.getRuntime().totalMemory() / 1024 / 1024;

        MemoryUsagePanel.this.progressBar.setMaximum(max);
        MemoryUsagePanel.this.progressBar.setValue(used);

        MemoryUsagePanel.this.progressBar.setString("Used memory: " + used + "MB");
    }

    public MemoryUsagePanel() {
        setLayout(new BorderLayout());
        this.progressBar = new JProgressBar();
        this.progressBar.setStringPainted(true);
        this.progressBar.setMinimum(0);
        this.progressBar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        this.progressBar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.gc();
                MemoryUsagePanel.this.refreshUsage();
            }
        });

        this.add(progressBar, BorderLayout.CENTER);

        Thread updateThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (; ; ) {
                    MemoryUsagePanel.this.refreshUsage();

                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {
                        // ??
                    }
                }
            }
        });

        updateThread.setName("Memory usage updater");
        updateThread.start();
    }
}
