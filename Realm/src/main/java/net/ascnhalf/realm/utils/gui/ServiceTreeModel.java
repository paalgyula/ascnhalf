package net.ascnhalf.realm.utils.gui;

import net.ascnhalf.commons.dataholder.DataLoadService;
import net.ascnhalf.commons.service.Service;
import net.ascnhalf.realm.service.ItemStorages;
import net.ascnhalf.realm.service.PlayerClassLevelInfoStorages;
import net.ascnhalf.realm.service.PlayerLevelStorages;
import net.ascnhalf.realm.service.QuestStorages;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.io.IOException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: Felhasználó
 * Date: 2013.06.05.
 * Time: 14:56
 * To change this template use File | Settings | File Templates.
 */
public class ServiceTreeModel extends DefaultTreeModel {
    public ServiceTreeModel() {
        super(null);
    }

    public void updateModel(Collection<Service> services) {
        IconTreeNode rootNode = new IconTreeNode(null);
        rootNode.setUserObject(services);

        ImageIcon dataServiceIcon = null,
                questStorageIcon = null,
                playerLevelStorageIcon = null,
                playerClassLevelStatsStorageIcon = null,
                itemStorageIcon = null;

        try {
            dataServiceIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/icons/storage_16.png")));
            questStorageIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/icons/quest_16.png")));
            playerLevelStorageIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/icons/skull_16.png")));
            playerClassLevelStatsStorageIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/icons/alchemy_16.png")));
            itemStorageIcon = new ImageIcon(ImageIO.read(getClass().getResourceAsStream("/icons/sword_16.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        IconTreeNode dataStoreServices = new IconTreeNode("Data Services", dataServiceIcon);
        rootNode.add(dataStoreServices);

        for (Service service : services) {
            if (service instanceof DataLoadService) {
                if (service instanceof QuestStorages) {
                    dataStoreServices.add(new IconTreeNode(service, questStorageIcon));
                } else if (service instanceof PlayerLevelStorages) {
                    dataStoreServices.add(new IconTreeNode(service, playerLevelStorageIcon));
                } else if (service instanceof PlayerClassLevelInfoStorages) {
                    dataStoreServices.add(new IconTreeNode(service, playerClassLevelStatsStorageIcon));
                } else if (service instanceof ItemStorages) {
                    dataStoreServices.add(new IconTreeNode(service, itemStorageIcon));
                } else {
                    dataStoreServices.add(new DefaultMutableTreeNode(service.getClass()));
                }
            } else
                rootNode.add(new DefaultMutableTreeNode(service.getClass()));
        }

        this.setRoot(rootNode);
    }

    public class IconTreeNode extends DefaultMutableTreeNode {
        private ImageIcon icon;

        public IconTreeNode(Object userObject, ImageIcon icon) {
            super(userObject);
            this.icon = icon;
        }

        public IconTreeNode(Object userObject) {
            super(userObject);
        }

        public void setIcon(ImageIcon icon) {
            this.icon = icon;
        }

        public ImageIcon getIcon() {
            return this.icon;
        }
    }
}
