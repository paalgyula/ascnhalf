package net.ascnhalf.realm.utils.gui;

import bsh.util.JConsole;
import net.ascnhalf.commons.service.Service;
import org.springframework.context.ApplicationContext;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Felhasználó
 * Date: 2013.06.05.
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
public class ManagerFrame {
    //private JTextArea consoleOutput;
    private JPanel panel1;
    private JConsole consoleOutput;
    private JTree serviceTree;
    private JTextArea textArea1;
    private MemoryUsagePanel memoryUsagePanel;

    private JFrame mainFrame;

    private JMenuBar menubar;

    private ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;

        Map<String, Service> dataServices = applicationContext.getBeansOfType(Service.class);

        ((ServiceTreeModel) (serviceTree.getModel())).updateModel(dataServices.values());

        for (String name : dataServices.keySet()) {
            textArea1.append("Service [" + name + "]\n");
        }
    }

    private void createMenuBar() {
        this.menubar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        {
            JMenuItem serviceReloadMenuItem = new JMenuItem("Reload service stats");
            {
                serviceReloadMenuItem.setMnemonic(KeyEvent.VK_R);
                serviceReloadMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.ALT_MASK));
                serviceReloadMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Map<String, Service> dataServices = applicationContext.getBeansOfType(Service.class);
                        ((ServiceTreeModel) (serviceTree.getModel())).updateModel(dataServices.values());
                    }
                });
                fileMenu.add(serviceReloadMenuItem);
            }

            JMenuItem exitMenuItem = new JMenuItem("Exit");
            {
                exitMenuItem.setMnemonic(KeyEvent.VK_X);
                exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_MASK));
                exitMenuItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.exit(0);
                    }
                });

                fileMenu.addSeparator();
                fileMenu.add(exitMenuItem);
            }
        }

        // File menu
        this.menubar.add(fileMenu);
    }

    public void show() {
        mainFrame = new JFrame("AscNHalf Manager Console");
        mainFrame.setContentPane(this.panel1);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setPreferredSize(new Dimension(500, 400));
        mainFrame.pack();
        mainFrame.setVisible(true);

        this.createMenuBar();

        mainFrame.setJMenuBar(this.menubar);

        ServiceTreeModel serviceTreeModel = new ServiceTreeModel();
        serviceTree.setRootVisible(false);
        serviceTree.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
                if (value instanceof ServiceTreeModel.IconTreeNode) {
                    ServiceTreeModel.IconTreeNode node = ((ServiceTreeModel.IconTreeNode) value);
                    node.getUserObject().getClass().getCanonicalName();
                    setIcon(node.getIcon());
                }

                return this;
            }
        });
        serviceTree.setModel(serviceTreeModel);

        System.setOut(consoleOutput.getOut());
        System.setIn(consoleOutput.getInputStream());

        try {
            java.util.List<BufferedImage> icons =
                    Arrays.asList(new BufferedImage[]{
                            ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_16.png")),
                            ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_32.png")),
                            ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_48.png")),
                            ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_64.png")),
                            ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_256.png"))
                    });
            TrayIcon trayIcon = new TrayIcon(ImageIO.read(getClass().getResourceAsStream("/icons/window_icon_256.png")), "AscNHalf Realm");
            trayIcon.setImageAutoSize(true);
            trayIcon.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (ManagerFrame.this.mainFrame.isVisible())
                        ManagerFrame.this.mainFrame.setVisible(false);
                    else
                        ManagerFrame.this.mainFrame.setVisible(true);
                }
            });

            try {
                SystemTray.getSystemTray().add(trayIcon);
            } catch (AWTException e2) {
                // System tray not supported
            }

            mainFrame.setIconImages(icons);
        } catch (IOException e) {
            // Cannot set icons...
        }
    }

    private void createUIComponents() {
        serviceTree = new JTree() {
            @Override
            protected void setExpandedState(TreePath path, boolean state) {
                super.setExpandedState(path, true);
            }
        };
    }
}
