package net.ascnhalf.realm.dao.impl;

import net.ascnhalf.realm.dao.PlayerDAO;
import net.ascnhalf.realm.model.Classes;
import net.ascnhalf.realm.model.InventoryItem;
import net.ascnhalf.realm.model.Races;
import net.ascnhalf.realm.model.base.Playercreateinfo;
import net.ascnhalf.realm.model.base.character.CharacterAccountData;
import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.character.CharacterRaces;
import net.ascnhalf.realm.model.player.CharacterStartOutfit;
import net.ascnhalf.realm.model.player.PlayerHomeBindData;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHAR_CREATE;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class PlayerDAO.
 */
@Repository("playerDAO")
@Transactional("charsTM")
public class PlayerDAOImpl implements PlayerDAO {

    @Autowired
    @Qualifier("worldSessionFactory")
    SessionFactory worldSessionFactory;

    @Autowired
    @Qualifier("characterSessionFactory")
    SessionFactory characterSessionFactory;

    private final Logger log = Logger.getLogger(getClass());

    public CharacterData getCharacter(long characterId) {
        Session session = characterSessionFactory.getCurrentSession();
        CharacterData characterData = (CharacterData) session.get(CharacterData.class, characterId);
        return characterData;
    }

    /**
     * Load home bind.
     *
     * @param objectId the object id
     * @return the player home bind data
     */
    @Transactional
    public PlayerHomeBindData loadHomeBind(int objectId) {
        Session session = characterSessionFactory.getCurrentSession();
        CharacterData characterData = (CharacterData) session.get(CharacterData.class, objectId);
        return characterData.getHomeBindData();
    }

    /**
     * Load inventory.
     *
     * @param objectId the object id
     * @return the list
     */
    @Transactional
    public List<InventoryItem> loadInventory(int objectId) {
        // TODO: implement
        return new ArrayList<InventoryItem>();
    }


    /**
     * Gets DBC race entry
     *
     * @param race
     * @return
     */
    @Transactional
    public CharacterRaces getRaceEntry(Races race) {
        Query query = worldSessionFactory.getCurrentSession().createQuery("select cr from CharacterRaces cr where cr.race = :race").setInteger("race", race.getValue());
        return (CharacterRaces) query.uniqueResult();
    }

    /**
     * Creating character
     *
     * @param accountId
     * @param name
     * @param charClass
     * @param charRace
     * @param gender
     * @param skin
     * @param face
     * @param hairStyle
     * @param hairColor
     * @param facial
     * @param outfitId
     * @return
     */
    @Override
    @Transactional("charsTM")
    public SMSG_CHAR_CREATE.CharCreateCodes createCharacter(int accountId, String name, int charClass, int charRace, int gender, int skin, int face, int hairStyle, int hairColor, int facial, int outfitId) {
        Session session = characterSessionFactory.getCurrentSession();

        Query query = session.createQuery("from CharacterData where name = :name").setString("name", name);
        if (query.list().size() != 0) {
            log.warn("Username already exists: " + name);
            return SMSG_CHAR_CREATE.CharCreateCodes.NAME_IN_USE;
        }

        Session worldSession = worldSessionFactory.openSession();
        worldSession.getTransaction().begin();

        Query pciQuery = worldSession.createQuery("select pci from Playercreateinfo pci where pci.race = :race and pci.clazz = :clazz").setParameter("race", charRace).setParameter("clazz", charClass);
        Playercreateinfo info = (Playercreateinfo) pciQuery.uniqueResult();

        if (info == null) {
            log.fatal("Player create template not found for: " + Classes.get(charClass) + " " + Races.get(charRace));
            return SMSG_CHAR_CREATE.CharCreateCodes.ERROR;
        }

        CharacterData charData = new CharacterData();
        // Set account id
        charData.setAccount(accountId);

        // Set name
        charData.setName(name);

        // Set Class/Race/Gender
        charData.setClazz(Classes.get(charClass));
        charData.setRace(Races.get(charRace));
        charData.setGender(gender);

        // Set spawn coord
        charData.setMap(info.getMap());
        charData.setZone(info.getZone());
        charData.setPositionX(info.getPositionX());
        charData.setPositionY(info.getPositionY());
        charData.setPositionZ(info.getPositionZ());
        charData.setOrientation(info.getOrientation());

        // Skin, Face, Hair(style,color)
        charData.setPlayerBytes((skin | (face << 8) | (hairStyle << 16) | (hairColor << 24)));

        // Hair(facial), Bankslot
        charData.setPlayerBytes2((facial /*| (0xEE << 8)*/ | (0x02 << 24)));

        charData.setExploredZones(Integer.toString(info.getZone()));
        charData.setKnownTitles("0");

        // TODO: make a config to create some inital value
        charData.setMoney(0);
        charData.setLevel(1);
        charData.setXp(0);

        // First login
        charData.setAtLoginFlags(0x32);

        // Set home (hs) location
        PlayerHomeBindData phbd = new PlayerHomeBindData();
        phbd.setHomeBindAreaId(charData.getZone());
        phbd.setHomeBindMapId(charData.getMap());
        phbd.setHomeBindPositionX(charData.getPositionX());
        phbd.setHomeBindPositionY(charData.getPositionY());
        phbd.setHomeBindPositionZ(charData.getPositionZ());
        phbd.setCharacterData(charData);
        charData.setHomeBindData(phbd);

        session.save(charData);

        // Inventory items
        Query outfitQuery = worldSession.createQuery("select c from CharacterStartOutfit c where c.clazz = :clazz and c.race = :race and c.gender = :gender")
                .setInteger("clazz", charClass)
                .setInteger("race", charRace)
                .setInteger("gender", gender);

        List<CharacterStartOutfit> outfit = outfitQuery.list();
        // TODO implement fatal error - outfit not found

        for (CharacterStartOutfit item : outfit) {
            InventoryItem inventoryItem = new InventoryItem(item.getProtoId(), item.getSlot());
            inventoryItem.setCharacterData(charData);
            session.save(inventoryItem);
            //log.info( "Adding item for character: " + item.getItemId() + " SlotId: " + item.getInventorySlot() );
        }

        worldSession.getTransaction().rollback();
        worldSession.close();

        return SMSG_CHAR_CREATE.CharCreateCodes.SUCCESS;
    }

    /**
     * Saves account data
     *
     * @param cad
     */
    @Override
    @Transactional
    public void saveAccountData(CharacterAccountData cad) {
        characterSessionFactory.getCurrentSession().saveOrUpdate(cad);
    }

    /**
     * Save/Update characterData
     *
     * @param characterData
     */
    @Override
    @Transactional
    public void saveCharacterData(CharacterData characterData) {
        Session session = characterSessionFactory.getCurrentSession();
        session.saveOrUpdate(characterData);
        //log.fatal( String.format( "Cannot save/update character [name: %s, guid: %d]", characterData.getName(), characterData.getGuid() ), e );
    }

    /**
     * @see PlayerDAO
     */
    @Override
    @Transactional
    public void deleteCharacter(long guid) {
        Session session = characterSessionFactory.getCurrentSession();
        CharacterData cd = (CharacterData) session.get(CharacterData.class, guid);
        session.delete(cd);
    }
}
