package net.ascnhalf.realm.dao.impl;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.realm.dao.QuestDAO;
import net.ascnhalf.realm.model.base.QuestPrototype;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The Class QuestDAO.
 */
@Repository("questDAO")
public class QuestDAOImpl implements QuestDAO {

    @Autowired
    @Qualifier("worldSessionFactory")
    SessionFactory worldSessionFactory;

    /**
     * Load quest prototypes.
     *
     * @return the t int object hash map
     */
    public TIntObjectHashMap<QuestPrototype> loadQuestPrototypes() {
        Session session = worldSessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(QuestPrototype.class);
        List<QuestPrototype> questPrototypeList = criteria.list();

        // Quest map by id
        TIntObjectHashMap<QuestPrototype> map = new TIntObjectHashMap<QuestPrototype>();

        for (QuestPrototype quest : questPrototypeList) {
            map.put(quest.getEntry(), quest);
        }

        return map;
    }

}
