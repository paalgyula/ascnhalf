/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.dao;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.realm.model.base.PlayerClassLevelInfo;
import net.ascnhalf.realm.model.base.PlayerClassLevelInfoPK;
import net.ascnhalf.realm.model.base.PlayerLevelInfo;
import net.ascnhalf.realm.model.base.PlayerLevelInfoPK;

import java.util.HashMap;

/**
 * The interface SimpleDataDAO.
 */
public interface SimpleDataDAO {

    public HashMap<PlayerClassLevelInfoPK, PlayerClassLevelInfo> getClassLevelInfos();

    /**
     * Gets the race class level infos.
     *
     * @return the race class level infos
     */
    public HashMap<PlayerLevelInfoPK, PlayerLevelInfo> getRaceClassLevelInfos();

    /**
     * Loads the Player Level-XP map
     *
     * @return
     */
    public TIntObjectHashMap<Integer> getPlayerXPForLevel();
}
