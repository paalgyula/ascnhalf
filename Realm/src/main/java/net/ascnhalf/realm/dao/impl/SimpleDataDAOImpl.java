package net.ascnhalf.realm.dao.impl;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.realm.dao.SimpleDataDAO;
import net.ascnhalf.realm.domain.PlayerXpForLevel;
import net.ascnhalf.realm.model.base.PlayerClassLevelInfo;
import net.ascnhalf.realm.model.base.PlayerClassLevelInfoPK;
import net.ascnhalf.realm.model.base.PlayerLevelInfo;
import net.ascnhalf.realm.model.base.PlayerLevelInfoPK;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * E-Mail: paalgyula@gmail.com
 * Date: 2012.09.16.
 * Time: 22:24
 */
@Repository("simpleDataDAO")
public class SimpleDataDAOImpl implements SimpleDataDAO {

    @Autowired
    @Qualifier("worldSessionFactory")
    SessionFactory worldSessionFactory;

    /**
     * Gets the class level infos.
     *
     * @return the class level infos
     */
    @Transactional
    public HashMap<PlayerClassLevelInfoPK, PlayerClassLevelInfo> getClassLevelInfos() {
        HashMap<PlayerClassLevelInfoPK, PlayerClassLevelInfo> map = new HashMap<PlayerClassLevelInfoPK, PlayerClassLevelInfo>();
        List<PlayerClassLevelInfo> infoList = (List<PlayerClassLevelInfo>) worldSessionFactory.getCurrentSession().createQuery("select pcli from PlayerClassLevelInfo pcli").list();
        for (PlayerClassLevelInfo levelInfo : infoList) {
            map.put(levelInfo.getPlayerClassLevelInfoPK(), levelInfo);
        }
        return map;
    }

    /**
     * Gets the race class level infos.
     *
     * @return the race class level infos
     */
    @Transactional
    public HashMap<PlayerLevelInfoPK, PlayerLevelInfo> getRaceClassLevelInfos() {
        HashMap<PlayerLevelInfoPK, PlayerLevelInfo> map = new HashMap<PlayerLevelInfoPK, PlayerLevelInfo>();
        Session session = worldSessionFactory.openSession();
        List<PlayerLevelInfo> infoList = (List<PlayerLevelInfo>) session.createQuery("select pli from PlayerLevelInfo pli").list();
        for (PlayerLevelInfo levelInfo : infoList) {
            map.put(levelInfo.getPlayerLevelInfoPK(), levelInfo);
        }

        return map;
    }

    @Transactional
    public TIntObjectHashMap<Integer> getPlayerXPForLevel() {
        Iterator<PlayerXpForLevel> pXpList = worldSessionFactory.getCurrentSession().createCriteria(PlayerXpForLevel.class).list().iterator();

        TIntObjectHashMap<Integer> playerXpForLevelMap = new TIntObjectHashMap<Integer>();
        while (pXpList.hasNext()) {
            PlayerXpForLevel playerXpForLevel = pXpList.next();
            playerXpForLevelMap.put(playerXpForLevel.getLvl(), playerXpForLevel.getXpForNextLevel());
        }

        return playerXpForLevelMap;
    }

}
