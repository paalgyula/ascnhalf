/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.dao.impl;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.realm.dao.ItemDAO;
import net.ascnhalf.realm.model.base.item.ItemPrototype;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The implementation of ItemDAO.
 */
@Repository("itemDAO")
public class ItemDAOImpl implements ItemDAO {
    Logger log = Logger.getLogger(getClass());

    @Autowired
    @Qualifier("worldSessionFactory")
    SessionFactory worldSessionFactory;

    /**
     * @see ItemDAO#loadItemPrototypes()
     */
    @Transactional
    public TIntObjectHashMap<ItemPrototype> loadItemPrototypes() {
        Long eTime = System.currentTimeMillis();

        Session session = worldSessionFactory.getCurrentSession();
        Query query = session.createQuery("from ItemPrototype order by id");

        TIntObjectHashMap<ItemPrototype> map = new TIntObjectHashMap<ItemPrototype>();
        @SuppressWarnings("unchecked")
        List<ItemPrototype> itemPrototypeList = (List<ItemPrototype>) query.list();

        // Fill map
        for (ItemPrototype item : itemPrototypeList) {
            map.put(item.getEntry(), item);
        }

        eTime = System.currentTimeMillis() - eTime;
        log.info(String.format("Loaded [%d] ItemPrototypes under %d ms", map.size(), eTime));

        return map;
    }

    /**
     * @see ItemDAO#loadItemPrototype(int)
     */
    @Transactional
    public ItemPrototype loadItemPrototype(int guid) {
        log.info("Loading single item from database with id: " + guid);
        return (ItemPrototype) worldSessionFactory.getCurrentSession().get(ItemPrototype.class, guid);
    }
}
