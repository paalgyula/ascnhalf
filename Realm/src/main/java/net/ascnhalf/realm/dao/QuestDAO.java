package net.ascnhalf.realm.dao;

import gnu.trove.map.hash.TIntObjectHashMap;
import net.ascnhalf.realm.model.base.QuestPrototype;

/**
 * The Class QuestDAO.
 */
public interface QuestDAO {

    /**
     * Load quest prototypes.
     *
     * @return the t int object hash map
     */
    public TIntObjectHashMap<QuestPrototype> loadQuestPrototypes();

}
