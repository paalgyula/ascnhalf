package net.ascnhalf.realm.dao.impl;

import net.ascnhalf.commons.model.Account;
import net.ascnhalf.realm.dao.AccountDAO;
import net.ascnhalf.realm.model.base.account.AccountData;
import net.ascnhalf.realm.model.base.character.CharacterData;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DAO that manages accounts.
 */
@Repository("accountDAO")
@Transactional("accountTM")
public class AccountDAOImpl implements AccountDAO {
    Logger logger = Logger.getLogger(AccountDAOImpl.class);

    @Autowired
    @Qualifier("accountSessionFactory")
    SessionFactory accountSessionFactory;

    @Autowired
    @Qualifier("characterSessionFactory")
    SessionFactory characterSessionFactory;

    /**
     * Returns account by name or null.
     *
     * @param name account name
     * @return account object or null
     */
    public Account getAccount(String name) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :name").setString("name", name);
        return (Account) query.uniqueResult();
    }

    /**
     * Retuns account id or -1 in case of error.
     *
     * @param name name of account
     * @return id or -1 in case of error
     */
    public int getAccountId(String name) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :name").setString("name", name);
        Account account = (Account) query.uniqueResult();
        if (account == null)
            return -1;
        ;
        return account.getId();
    }

    /**
     * Reruns account count If error occured - returns -1.
     *
     * @return account count
     */
    public int getAccountCount() {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a");
        int accCount = query.list().size();
        return accCount;
    }

    /**
     * Update security key.
     *
     * @param account the account
     * @return true, if successful
     */
    public boolean updateSecurityKey(Account account) {
        Session session = accountSessionFactory.getCurrentSession();
        session.getTransaction().begin();
        boolean success = false;
        try {
            session.saveOrUpdate(account);
            session.getTransaction().commit();
            success = true;
        } finally {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            session.close();
        }

        return success;
    }

    /**
     * Updates lastServer field of account.
     *
     * @param accountId  account id
     * @param lastServer last accessed server
     * @return was updated successful or not
     */
    public boolean updateLastServer(int accountId, byte lastServer) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :id").setInteger("id", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            session.close();
            logger.warn("Account with id (" + accountId + ") not found!");
            return false;
        }

        account.setLastServer(lastServer);
        session.getTransaction().begin();
        boolean success = false;
        try {
            session.saveOrUpdate(account);
            session.getTransaction().commit();
            success = true;
        } finally {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            session.close();
        }

        return success;
    }

    /**
     * Updates last ip that was used to access an account.
     *
     * @param accountId account id
     * @param ip        ip address
     * @return was update successful or not
     */
    public boolean updateLastIp(int accountId, String ip) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :id").setInteger("id", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            session.close();
            logger.warn("Account with id (" + accountId + ") not found!");
            return false;
        }

        account.setLastIp(ip);
        session.getTransaction().begin();
        boolean success = false;
        try {
            session.saveOrUpdate(account);
            session.getTransaction().commit();
            success = true;
            logger.info(String.format("Account (#%d) updated. New IP: %s.", accountId, ip));
        } finally {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            session.close();
        }

        return success;
    }

    /**
     * Get last ip that was used to access an account.
     *
     * @param accountId account id
     * @return ip address
     */
    public String getLastIp(int accountId) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.id = :accountid").setInteger("accountid", accountId);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            session.close();
            logger.warn("Account with id (" + accountId + ") not found!");
            return "";
        }

        String lastIp = account.getLastIp();
        session.close();
        return lastIp;
    }

    /**
     * Update session key.
     *
     * @param username the username
     * @param key      the key
     * @return true, if successful
     */
    public boolean updateSessionKey(String username, String key) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :username").setString("username", username);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            session.close();
            logger.warn("Account with id (" + username + ") not found!");
            return false;
        }

        account.setSessionKey(key);
        session.getTransaction().begin();
        boolean success = false;
        try {
            session.saveOrUpdate(account);
            session.getTransaction().commit();
            success = true;
            logger.info(String.format("Account (%s) updated. New session key: %s.", username, key));
        } finally {
            if (session.getTransaction().isActive())
                session.getTransaction().rollback();
            session.close();
        }

        return success;
    }

    /**
     * Gets the session key.
     *
     * @param username the username
     * @return the session key
     */
    public String getSessionKey(String username) {
        Session session = accountSessionFactory.getCurrentSession();
        Query query = session.createQuery("select a from Account a where a.username = :username").setString("username", username);
        Account account = (Account) query.uniqueResult();

        if (account == null) {
            session.close();
            logger.warn("Account with id (" + username + ") not found!");
            return "";
        }

        return account.getMsessionKey();
    }

    /**
     * Returns character list
     *
     * @param accountId account to query
     * @return List&lt;CharacterData&gt; caracters
     */
    @Transactional("charsTM")
    public List<CharacterData> getCharacters(int accountId) {
        Session session = characterSessionFactory.getCurrentSession();
        Query query = session.createQuery("from CharacterData where account = :accountid").setInteger("accountid", accountId);
        return (List<CharacterData>) query.list();
    }

    public void saveSessionKey(String username, String key) {
    }

    public int[] getTutorialsData(int guid) {
        return new int[]{0, 0, 0, 0, 0, 0, 0, 0};
    }

    @Transactional("charsTM")
    public List<AccountData> getAccountData(int guid) {
        Session session = characterSessionFactory.getCurrentSession();
        Query query = session.createQuery("from AccountData where guid = :guid").setInteger("guid", guid);
        List<AccountData> accData = (List<AccountData>) query.list();
        return accData;
    }
}
