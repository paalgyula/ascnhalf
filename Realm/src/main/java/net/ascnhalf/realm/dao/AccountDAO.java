package net.ascnhalf.realm.dao;

import net.ascnhalf.commons.model.Account;
import net.ascnhalf.realm.model.base.account.AccountData;
import net.ascnhalf.realm.model.base.character.CharacterData;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * DAO that manages accounts.
 */
public interface AccountDAO {
    Logger logger = Logger.getLogger(AccountDAO.class);

    /**
     * Returns account by name or null.
     *
     * @param name account name
     * @return account object or null
     */
    public Account getAccount(String name);

    /**
     * Retuns account id or -1 in case of error.
     *
     * @param name name of account
     * @return id or -1 in case of error
     */
    public int getAccountId(String name);

    /**
     * Reruns account count If error occured - returns -1.
     *
     * @return account count
     */
    public int getAccountCount();

    /**
     * Update security key.
     *
     * @param account the account
     * @return true, if successful
     */
    public boolean updateSecurityKey(Account account);

    /**
     * Updates lastServer field of account.
     *
     * @param accountId  account id
     * @param lastServer last accessed server
     * @return was updated successful or not
     */
    public boolean updateLastServer(int accountId, byte lastServer);

    /**
     * Updates last ip that was used to access an account.
     *
     * @param accountId account id
     * @param ip        ip address
     * @return was update successful or not
     */
    public boolean updateLastIp(int accountId, String ip);

    /**
     * Get last ip that was used to access an account.
     *
     * @param accountId account id
     * @return ip address
     */
    public String getLastIp(int accountId);

    /**
     * Update session key.
     *
     * @param username the username
     * @param key      the key
     * @return true, if successful
     */
    public boolean updateSessionKey(String username, String key);

    /**
     * Gets the session key.
     *
     * @param username the username
     * @return the session key
     */
    public String getSessionKey(String username);

    /**
     * Returns character list
     *
     * @param accountId account to query
     * @return List&lt;CharacterData&gt; caracters
     */
    public List<CharacterData> getCharacters(int accountId);

    public void saveSessionKey(String username, String key);

    public int[] getTutorialsData(int guid);

    public List<AccountData> getAccountData(int guid);
}
