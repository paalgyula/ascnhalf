/*******************************************************************************
 * Copyright (C) 2012 JMaNGOS <http://jmangos.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
package net.ascnhalf.realm.dao;

import net.ascnhalf.realm.model.InventoryItem;
import net.ascnhalf.realm.model.Races;
import net.ascnhalf.realm.model.base.character.CharacterAccountData;
import net.ascnhalf.realm.model.base.character.CharacterData;
import net.ascnhalf.realm.model.base.character.CharacterRaces;
import net.ascnhalf.realm.model.player.PlayerHomeBindData;
import net.ascnhalf.realm.network.netty.packetClient.server.SMSG_CHAR_CREATE;

import java.util.List;

/**
 * The interface PlayerDAO.
 */
public interface PlayerDAO {

    public CharacterData getCharacter(long characterId);

    /**
     * Load home bind.
     *
     * @param objectId the object id
     * @return the player home bind data
     */
    public PlayerHomeBindData loadHomeBind(int objectId);

    /**
     * Load inventory.
     *
     * @param objectId the object id
     * @return the list
     */
    public List<InventoryItem> loadInventory(int objectId);

    public CharacterRaces getRaceEntry(Races race);

    public SMSG_CHAR_CREATE.CharCreateCodes createCharacter(int accountId, String name, int charClass, int charRace, int gender, int skin, int face, int hairStyle, int hairColor, int facial, int outfitId);

    /**
     * Saves character_account_data
     *
     * @param cad
     */
    public void saveAccountData(CharacterAccountData cad);

    /**
     * Saves/updates characters
     *
     * @param characterData
     */
    public void saveCharacterData(CharacterData characterData);

    /**
     * Delete the specified character
     *
     * @param guid character guid
     */
    public void deleteCharacter(long guid);
}
