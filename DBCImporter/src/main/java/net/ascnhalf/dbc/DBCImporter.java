package net.ascnhalf.dbc;

import net.ascnhalf.dbc.dataholder.DBCDataLoader;
import net.ascnhalf.dbc.struct.CharStartOutfitEntry;
import net.ascnhalf.dbc.struct.ChatChannelsEntry;
import net.ascnhalf.dbc.struct.ChrRacesEntry;
import net.ascnhalf.realm.model.Classes;
import net.ascnhalf.realm.model.Races;
import net.ascnhalf.realm.model.base.character.CharacterRaces;
import net.ascnhalf.realm.model.player.CharacterStartOutfit;
import net.ascnhalf.realm.model.player.CharacterStartOutfitItems;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created with IntelliJ IDEA.
 * User: Goofy
 * Date: 2012.08.13.
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public class DBCImporter {
    public static void main(String[] args) {
        Logger log = Logger.getLogger(DBCImporter.class);

        boolean charOutfit = true;
        boolean charRaces = false;
        boolean chatChannels = false;

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/applicationConfig.xml");
        SessionFactory sessionFactory = (SessionFactory) applicationContext.getBean("worldSessionFactory");
        Session session = sessionFactory.openSession();

        if (charOutfit) {
            CharStartOutfitEntry entry = DBCDataLoader.loadStaticData(CharStartOutfitEntry.class, "dbc/Charstartoutfit.dbc");
            do {
                session.getTransaction().begin();
                CharacterStartOutfit outfit = new CharacterStartOutfit();
                outfit.setId(Integer.parseInt(entry.id.toString()));
                outfit.setClazz(Integer.parseInt(entry.clazz.toString()));
                outfit.setGender(Integer.parseInt(entry.gender.toString()));
                outfit.setRace(Integer.parseInt(entry.race.toString()));

                log.info("Updating " + Classes.get(outfit.getClazz()) + " " + Races.get(outfit.getRace()) + " Gender:" + outfit.getGender());

                session.saveOrUpdate(outfit); // Creating first

                try {
                    for (int i = 0; i < entry.ItemId.length; i++) {
                        try {
                            if (Integer.parseInt(entry.ItemId[i].toString()) == -1)
                                continue;
                            if (Integer.parseInt(entry.ItemId[i].toString()) == 0)
                                continue;

                            CharacterStartOutfitItems item = new CharacterStartOutfitItems(
                                    Integer.parseInt(entry.ItemDisplayId[i].toString()),
                                    Integer.parseInt(entry.ItemId[i].toString()),
                                    Integer.parseInt(entry.ItemInventorySlot[i].toString())
                            );

                            item.setCharacterStartOutfit(outfit);
                            session.saveOrUpdate(item);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IndexOutOfBoundsException e) {/* that's all :) */}

                try {
                    session.getTransaction().commit();
                } catch (ConstraintViolationException e) {
                    log.fatal(e.getMessage());
                    session.getTransaction().rollback();
                }
            } while ((entry = entry.next()) != null);
        }

        if (charRaces) {
            ChrRacesEntry entry = DBCDataLoader.loadStaticData(ChrRacesEntry.class, "dbc/ChrRaces.dbc");
            do {
                session.getTransaction().begin();
                session.save(new CharacterRaces(
                        Integer.parseInt(entry.RaceID.toString()),
                        Integer.parseInt(entry.modelM.toString()),
                        Integer.parseInt(entry.modelF.toString()),
                        Integer.parseInt(entry.exploredZones.toString())
                ));
                session.getTransaction().commit();
            } while ((entry = entry.next()) != null);
        }

        if (chatChannels) {
            ChatChannelsEntry entry = DBCDataLoader.loadStaticData(ChatChannelsEntry.class, "dbc/ChatChannels.dbc");
            do {
                System.out.println(entry.id.toString());
                System.out.println(entry.pattern.toString() + " - " + entry.flags.toString());
                System.out.println(entry.name.toString());
            } while ((entry = entry.next()) != null);
        }

        //session.close();
        System.out.println("Import completed!");
    }
}
